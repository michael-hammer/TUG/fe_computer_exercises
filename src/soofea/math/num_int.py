""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np
import math
import scipy.special.orthogonal

import unittest
import soofea.model
import soofea.math
import scipy.integrate
import numpy.testing

#------------------------------------------------------------------
def integrate( function, integration_points, parameters = None ):
	integration_val = function(integration_points[0],parameters) * integration_points[0].math_ip.weight;
	for i in range(1,len(integration_points)):
			integration_val += integration_points[i].math_ip.weight * function(integration_points[i],parameters);
	return( integration_val )

#------------------------------------------------------------------
def methodIntegrate( function, object, integration_points, parameters = None ):
	integration_val = function(object,integration_points[0],parameters) * integration_points[0].math_ip.weight;
	for i in range(1,len(integration_points)):
			integration_val += integration_points[i].math_ip.weight * function(object,integration_points[i],parameters);
	return( integration_val )

#------------------------------------------------------------------
def f1(int_point,parameters=None):
	return( np.array([1.,1.,1.]) )

#------------------------------------------------------------------
def f2(int_point,parameters=None):
	return( math.sin(int_point.math_ip.natural_coordinates) )
def g2(int_point):
	return( math.sin(int_point) )

#------------------------------------------------------------------
def f3(int_point,parameters=None):
	return( 1. / int_point.math_ip.natural_coordinates )
def g3(int_point,parameters=None):
	return( 1. / int_point )

#------------------------------------------------------------------
def f4(int_point,parameters=None):
	r = int_point.math_ip.natural_coordinates
	return( r + 2*r**2 - 5*r**3 )
def g4(int_point):
	r = int_point
	return( r + 2*r**2 - 5*r**3 )

#------------------------------------------------------------------
class TestNumInt( unittest.TestCase ):
	def testEx_3_1(self):
		for number_of_int_points in range(1,12):
			[coords, weights] = scipy.special.orthogonal.p_roots( number_of_int_points )
			int_points_1D = []
			int_points_2D = []
			int_points_3D = []
			for i in range(len(coords)):
				math_ip = soofea.math.IntegrationPoint( weights[i], coords[i] )
				int_points_1D.append( soofea.model.IntegrationPoint( math_ip , [0] ) )
				for j in range(len(coords)):
					math_ip = soofea.math.IntegrationPoint( weights[i]*weights[j], [coords[i],coords[j]] )
					int_points_2D.append( soofea.model.IntegrationPoint( math_ip , [0] ) )
					for k in range(len(coords)):
						math_ip = soofea.math.IntegrationPoint( weights[i]*weights[j]*weights[k], [coords[i],coords[j],coords[k]] )
						int_points_3D.append( soofea.model.IntegrationPoint( math_ip , [0] ) )

			numpy.testing.assert_almost_equal( integrate( f1 , int_points_1D ), [2.,2.,2.] )
			numpy.testing.assert_almost_equal( integrate( f1 , int_points_2D ), [4.,4.,4.] )
			numpy.testing.assert_almost_equal( integrate( f1 , int_points_3D ), [8.,8.,8.] )

	def testEx_3_2(self):
		[coords, weights] = scipy.special.orthogonal.p_roots( 12 )
		int_points = []
		for i in range(len(coords)):
			math_ip = soofea.math.IntegrationPoint( weights[i], coords[i] )
			int_points.append( soofea.model.IntegrationPoint( math_ip , [0] ) )

		self.assertAlmostEqual( integrate( f2 , int_points ) , scipy.integrate.quad( g2, -1. , 1. )[0] )
		# print integrate( f3 , int_points )
		# print scipy.integrate.quad( g3, -1. , 1. )
		self.assertAlmostEqual( integrate( f4 , int_points ) , scipy.integrate.quad( g4, -1. , 1. )[0] )

if __name__ == '__main__':
	unittest.main()

