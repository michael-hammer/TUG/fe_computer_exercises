""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np
from soofea.math.num_int import methodIntegrate
from soofea.analyzer.jacobian import ElementJacobian

#------------------------------------------------------------------
class ElementImpl:
	def calcStiffness( self, element ):
		K = methodIntegrate( ElementImpl._stiffnessIntegrator, self , element.int_points , \
							 {'element':element} )
		return( K )

	def _stiffnessIntegrator( self, int_point, parameters ):
		element = parameters['element']
		number_of_nodes = element.type.shape.getNumberOfNodes()
		dimension = len( element.getNode(0).lagrangian_coordinates )

		J = ElementJacobian( element, int_point )
		J_inv = J.getInv()
		dHr = element.type.shape.getDerivativeMatrix( int_point.math_ip.natural_coordinates , 1 )
		dH = np.dot( dHr.transpose(), J_inv )
		C = element.material.getElasticityMatrix(dimension)
		volume = J.getDet()

		if dimension == 2:
			int_point.B = np.zeros( [3, dimension*number_of_nodes] )
			for node_counter in range( number_of_nodes ):
				int_point.B[0, 2*node_counter   ] = dH[node_counter,0]
				int_point.B[1, 2*node_counter+1 ] = dH[node_counter,1]
				int_point.B[2, 2*node_counter   ] = dH[node_counter,1]
				int_point.B[2, 2*node_counter+1 ] = dH[node_counter,0]
				volume *= element.type.height
		else:
			raise Exception("Stiffness integrator only for 2D implemented!")

		int_matrix = np.dot( np.dot( np.transpose(int_point.B), C ), int_point.B ) * volume
		return int_matrix
