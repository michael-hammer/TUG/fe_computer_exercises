""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np
from numpy.linalg import inv, det
import math

#------------------------------------------------------------------
class Jacobian:
	def __init__(self,node_container,int_point):
		self._node_container = node_container
		self._int_point = int_point
		self._calc()

	def _calc(self):
		x_dim = self._node_container.getDimension()
		r_dim = self._node_container.type.shape.dimension
		der_matrix = self._node_container.type.shape.getDerivativeMatrix( self._int_point.math_ip.natural_coordinates, x_dim )
		node_coordinates = self._node_container.getCoordinateArray()
		temp = np.dot( der_matrix , node_coordinates )
		self._J = np.zeros([ x_dim, r_dim ])
		for r_dim_counter in range(r_dim):
			self._J[r_dim_counter,:] = temp[ r_dim_counter*r_dim : (r_dim_counter+1)*r_dim ]

#------------------------------------------------------------------
class ElementJacobian( Jacobian ):
	def __init__(self,element,int_point):
		Jacobian.__init__(self,element,int_point)

	def get( self ):
		return( self._J )

	def getInv( self ):
		return( inv( self._J ))

	def getDet( self ):
		return( det( self._J ))
