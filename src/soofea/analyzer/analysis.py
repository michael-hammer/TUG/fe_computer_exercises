""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np
from numpy.linalg import solve, cond, det
from math import floor
from scipy.sparse import lil_matrix
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt

#------------------------------------------------------------------
class Analysis:
	def __init__(self, model):
		self._model = model

	def stiffnessAssembler(self, node_container, local_stiffness, global_stiffness):
		number_of_entries = local_stiffness.shape[0] / node_container.type.shape.getNumberOfNodes()
		for local_row_counter in range( local_stiffness.shape[0] ):
			for local_col_counter in range( local_stiffness.shape[1] ):
				row_node = node_container.getNode( int(floor(local_row_counter/number_of_entries)) )
				col_node = node_container.getNode( int(floor(local_col_counter/number_of_entries)) )
				row_coord_offset = local_row_counter % number_of_entries
				col_coord_offset = local_col_counter % number_of_entries
				global_row_index = (row_node.number-1)*number_of_entries + row_coord_offset;
				global_col_index = (col_node.number-1)*number_of_entries + col_coord_offset;
				global_stiffness[global_row_index,global_col_index] += local_stiffness[ local_row_counter, local_col_counter ]

	def assembleLoad( self, global_load ):
		print "-> Assemble Global Load"

	def assembleStiffness( self, global_stiffness ):
		print "-> Assemble Global Stiffness"
		for number,element in self._model._element_dict.iteritems():
			K_elem = element.type.implementation.calcStiffness( element )
			self.stiffnessAssembler(element, K_elem, global_stiffness)

	def integrateDirichletBC( self, global_stiffness, global_load ):
		print "-> Integrate Dirichlet Boundary Conditions"

		# Algorithm is O(n^2) - rather slow :-(
		for node in self._model.getNodeIterator():
			for coord_offset in range(self._model.dimension):
				if node.dof.constraint[coord_offset]:
					global_col_index = (node.number-1)*self._model.dimension + coord_offset
					for row_counter in range(global_stiffness.shape[0]):
						if( row_counter == global_col_index ):
							global_load[row_counter] = global_stiffness[row_counter, global_col_index] * node.dof.getValue( coord_offset )
						else:
							global_load[row_counter] -= global_stiffness[row_counter, global_col_index] * node.dof.getValue( coord_offset )

					temp = global_stiffness[global_col_index,global_col_index]
					global_stiffness[:,global_col_index] = 0.
					global_stiffness[global_col_index,:] = 0.
					global_stiffness[global_col_index,global_col_index] = temp

	def integrateNeumannBC( self, global_load ):
		print "-> Integrate Neumann Boundary Conditions"

		for node in self._model.getNodeIterator():
			force_load = node.load
			if not np.allclose( force_load.force , np.zeros( len(force_load.force) ) ).all():
				for coord_offset in range( self._model.dimension ):
					global_index = (node.number-1)*self._model.dimension + coord_offset
					global_load[global_index] += force_load.getValue(coord_offset)

	def updateDOF(self, solution_vector):
		for node_counter in range( 1,self._model.getNumberOfNodes()+1 ):
			for coord_offset in range( self._model.dimension ):
				global_index = (node_counter-1)*self._model.dimension + coord_offset
				self._model.getNode(node_counter).dof.setValue( coord_offset, solution_vector[global_index] )

#------------------------------------------------------------------
class LinearAnalysis(Analysis):
	def __init__(self, model):
		Analysis.__init__(self, model)

	def run(self):
		print "-> LinearAnalysis"

		number_of_unknowns = self._model.getNumberOfUnknowns()

		global_load = np.zeros( number_of_unknowns )
		global_stiffness = lil_matrix((number_of_unknowns,number_of_unknowns))
		self.assembleStiffness( global_stiffness )
		self.assembleLoad( global_load )
		self.integrateDirichletBC( global_stiffness, global_load )
		self.integrateNeumannBC( global_load )

		print "--> Solving System ..."
		# Convert lil matrix to a compressed sparse row format
		global_stiffness = global_stiffness.tocsr()
		displacement = spsolve( global_stiffness, global_load ) # SuperLU solver
		print "--> ... done"
		self.updateDOF(displacement)
