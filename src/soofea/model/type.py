""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

from soofea.base import NumberedObject
import soofea.math
from soofea.model.shape import QuadShape
import scipy.special.orthogonal

#------------------------------------------------------------------
class Type( NumberedObject ):
	def __init__( self, number, shape_order, shape_type, number_of_int_points ):
		NumberedObject.__init__(self,number)
		if( shape_type == 'quad' ):
			self.shape = QuadShape( shape_order )
		else:
			raise Exception( "The given shape type '"+shape_type+"' is not implemented" )

		# ip_data[][0] ... natural_coords
		# ip_data[][1] ... weights
		ip_data = []
		for coord_number in number_of_int_points:
			ip_data.append( scipy.special.orthogonal.p_roots( coord_number ) )

		self._math_ips = []
		if( len(ip_data) == 2 ):
			for i in range(len(ip_data[0][0])):
				for j in range(len(ip_data[1][0])):
					self._math_ips.append(soofea.math.IntegrationPoint( ip_data[0][1][i] * ip_data[0][1][j], [ip_data[0][0][i],ip_data[0][0][j]] ))

	def getMathIntegrationPoints(self):
		return( self._math_ips )

#------------------------------------------------------------------
class ElementType( Type ):
	def __init__( self, N, shape_order, shape_type, number_of_int_points ):
		Type.__init__(self,N, shape_order, shape_type, number_of_int_points )
