""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np

#------------------------------------------------------------------
class DOF:
	pass

#------------------------------------------------------------------
class DisplacementDOF(DOF):
	def __init__( self, dimension ):
		self.displacement = np.zeros(dimension)
		self.constraint = [False for x in range(dimension)]

	def getValue( self, coord_id ):
		return( self.displacement[coord_id] )

	def setValue( self, coord_id, value ):
		self.displacement[coord_id] = value

	def setConstraintValue( self, coord_id, value ):
		self.displacement[coord_id] = value
		self.constraint[coord_id] = True

