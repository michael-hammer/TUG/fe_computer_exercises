""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np
import math
from soofea.base import NumberedObject
from soofea.model.type import ElementType
from soofea.model.material import StVenantKirchhoffMaterial
from soofea.model.dof import DisplacementDOF
from soofea.model.load import ForceLoad

#------------------------------------------------------------------
class IntegrationPoint:
	def __init__( self, math_ip , lagrangian_coordinates ):
		self.math_ip = math_ip
		self._lagrangian_coordinates = lagrangian_coordinates

#------------------------------------------------------------------
class Node(NumberedObject):
	def __init__(self, number, lagrangian_coordinates):
		NumberedObject.__init__(self,number)
		self.lagrangian_coordinates = lagrangian_coordinates
		self.dof = DisplacementDOF( len(self.lagrangian_coordinates) )
		self.load = ForceLoad( len(self.lagrangian_coordinates) )

	def __str__(self):
		print_str = 'Node '+str(self.number)+' @ '+str(self.lagrangian_coordinates)+'\n'
		return( print_str )

	def setBCDOF(self, x=None, y=None, z=None):
		if x != None:
			self.dof.setConstraintValue( 0 , x )
		if y != None:
			self.dof.setConstraintValue( 1 , y )
		if z != None:
			self.dof.setConstraintValue( 2 , z )

	def setBCLoad(self, x=None, y=None, z=None):
		if x != None:
			self.load.setValue( 0 , x )
		if y != None:
			self.load.setValue( 1 , y )
		if z != None:
			self.load.setValue( 2 , z )

	def setBodyForce( self , f_B ):
		self.f_B = f_B

	def setSurfaceForce( self , f_S ):
		self.f_S = f_S

#------------------------------------------------------------------
class NodeContainer(NumberedObject):
	def __init__(self, number, node_number_list):
		NumberedObject.__init__(self,number)
		self._node_number_list = node_number_list
		self._node_list = []
		self.type = None
		self.int_points = []

	def __str__(self):
		print_str = str( self._node_number_list )
		return( print_str )

	def setNodes(self, node_list):
		self._node_list = node_list

	def getNode( self, local_node_number ):
		return( self._node_list[local_node_number] )

	def getNodes( self ):
		return( self._node_list )

	def getDimension( self ):
		return( len(self._node_list[0].lagrangian_coordinates) )

	def setType(self, the_type):
		self.type = the_type
		math_ips = the_type.getMathIntegrationPoints()
		for math_ip in math_ips:
			X = self.getCoordinateArray()
			H = self.type.shape.getMatrix( math_ip.natural_coordinates , self.getDimension() )
			coord = np.dot( H , X )
			self.int_points.append( IntegrationPoint( math_ip, coord ) )

	def getCoordinateArray(self):
		dimension = self.getDimension()
		X = np.zeros( dimension * len(self._node_list) )
		for node_counter in range(len(self._node_list)):
			X[ node_counter * dimension : (node_counter+1) * dimension ] =\
			   self._node_list[node_counter].lagrangian_coordinates
		return( X )

#------------------------------------------------------------------
class BoundaryComponent(NodeContainer):
	def __init__(self, number, node_number_list):
		NodeContainer.__init__( self, number, node_number_list )

	def setPressure( self, p ): # scalar Force/Area
		self.pressure = p

	def setSurfaceLoad( self, load ): # vector with [Force/Area,...]
		self.f_S = load

#------------------------------------------------------------------
class Edge(BoundaryComponent):
	def __init__(self, number, node_number_list):
		BoundaryComponent.__init__( self, number, node_number_list )

#------------------------------------------------------------------
class Face(BoundaryComponent):
	def __init__(self, number, node_number_list):
		BoundaryComponent.__init__( self, number, node_number_list )

#------------------------------------------------------------------
class Element(NodeContainer):
	def __init__(self, number, node_number_list):
		NodeContainer.__init__( self, number, node_number_list )

	def setMaterial(self, material):
		self.material = material

	def setBodyForce(self, load): # vector of [Force/Volume,...]
		self.f_B = load

#------------------------------------------------------------------
class Boundary(NodeContainer):
	def __init__(self, N):
		NodeContainer.__init__(self, N, [] ) # The node numer list is empty at this point
		self._target_list = []

	def addTarget(self, target):
		self._target_list.append(target)
		for node in target._node_list:
			if node.number not in self._node_number_list:
				self._node_number_list.append(node.number)
				self._node_list.append(node)

	def getTargetList(self):
		return self._target_list

#------------------------------------------------------------------
class Model:
	def __init__(self,dimension):
		self.dimension = dimension
		self._node_dict = {}
		self._edge_dict = {}
		self._face_dict = {}
		self._element_dict = {}
		self._type_dict = {}
		self._material_dict = {}
		self._boundary_dict = {}

	def resolveNodes( self, node_number_list ):
		node_list = []
		for node_number in node_number_list:
			node_list.append( self._node_dict[node_number] )
		return( node_list )

	def addNode(self, node):
		self._node_dict[node.number] = node

	def getNodeIterator( self ):
		return( ModelIterator( self._node_dict ) )

	def getNode(self, number):
		return( self._node_dict[number] )

	def getNumberOfNodes( self ):
		return( len(self._node_dict) )

	def addEdge(self, edge ):
		self._edge_dict[edge.number] = edge
		edge.setNodes( self.resolveNodes( edge._node_number_list ) )
		if 2 in self._type_dict:
			edge.setType( self._type_dict[2] ) # We set the edge type to be 2!

	def addFace(self, face ):
		self._face_dict[face.number] = face
		face.setNodes( self.resolveNodes( face._node_number_list ) )
		if 3 in self._type_dict:
			face.setType( self._type_dict[3] ) # We set the face type to be 3!

	def addElement(self, element ):
		self._element_dict[element.number] = element
		element.setNodes( self.resolveNodes( element._node_number_list ) )
		element.setType( self._type_dict[1] ) # We set the element type to be 1!
		element.setMaterial( self._material_dict[1] ) # We set the material type to be 1!

	def getElementIterator( self ):
		return( ModelIterator( self._element_dict ) )

	def addType(self, new_type):
		self._type_dict[new_type.number] = new_type

	def getType(self, number):
		return( self._type_dict[number] )

	def addMaterial(self, material):
		self._material_dict[material.number] = material

	def getNumberOfUnknowns(self):
		number_of_unknowns = 0
		for number,node in self._node_dict.iteritems():
			number_of_unknowns += self.dimension
		return number_of_unknowns

	def appendTargetToBoundary(self, N, target_N , target_type):
		if not N in self._boundary_dict:
			self._boundary_dict[N] = Boundary(N)
		if( target_type == 'edge' ):
			self._boundary_dict[N].addTarget( self._edge_dict[target_N] )
		elif( target_type == 'face' ):
			self._boundary_dict[N].addTarget( self._face_dict[target_N] )

	def getBoundary(self, N):
		return( self._boundary_dict[N] )

#------------------------------------------------------------------
class ModelIterator:
	def __init__( self, obj_dict ):
		self._obj_dict = obj_dict
		self._last = obj_dict.itervalues()

	def __iter__(self):
		return self

	def next( self ):
		return( self._last.next() )
