""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np
import matplotlib.pyplot as plt
import unittest

#------------------------------------------------------------------
class Lagrange:
	def __init__(self, node_positions):
		self._node_positions = node_positions

	def get(self, r, index, order = None ):
		if order == None:
			order = len(self._node_positions)-1
		L = 1.;
		for count in range(order+1):
			if( count != index ):
				L *= (r - self._node_positions[count]) / (self._node_positions[index] - self._node_positions[count])
		return L

	def der(self, r, index, order = None ):
		if order == None:
			order = len(self._node_positions)-1
		constant_part = 1.;
		for count in range(order+1):
			if( count != index ):
				constant_part = constant_part / (self._node_positions[index] - self._node_positions[count])

		dependent_part = 0.;
		for sum_counter in range(order+1): # i
			if( sum_counter != index ):
				product = 1.;
				for product_counter in range(order+1): # j
					if (product_counter != sum_counter) and (product_counter != index):
						product *= (r - self._node_positions[product_counter])
				dependent_part += product

		return( constant_part * dependent_part )

#------------------------------------------------------------------
class Shape:
	def __init__(self, order, shape_dimension):
		self.dimension = shape_dimension
		self._order = order
		self._number_of_nodes = 0
		self._lagrange = Lagrange( self._calcNodePositions() )

	def getNumberOfNodes(self):
		if self._number_of_nodes == 0:
			self._number_of_nodes = self._calcNumberOfNodes()
		return( self._number_of_nodes )

	def getMatrix(self, natural_coordinates, array_length):
		H = np.zeros([array_length,self.getNumberOfNodes()*array_length]);
		for local_node_number in range(self.getNumberOfNodes()):
			for coord_offset in range(array_length):
				H[coord_offset,(local_node_number*array_length)+coord_offset]=\
					  self.get( natural_coordinates, self.getNodeIndex(local_node_number) )
		return( H )

	def getDerivativeMatrix(self, natural_coordinates, array_length):
		dH = np.zeros( [array_length*self.dimension, self.getNumberOfNodes()*array_length ] )
		for r_dim_counter in range(self.dimension):
			for array_dim_counter in range(array_length):
				for local_node_number in range(self.getNumberOfNodes()):
					dH[ array_dim_counter*self.dimension + r_dim_counter, \
						local_node_number*array_length + array_dim_counter ] =\
						self.der( natural_coordinates, self.getNodeIndex(local_node_number), r_dim_counter )
		return( dH )

#------------------------------------------------------------------
class QuadShape(Shape):
	def __init__(self,order):
		if( order > 2 ):
			raise Exception( 'QuadShape only implemented until order 2')
		Shape.__init__(self,order,2)

	def _calcNodePositions(self):
		return( np.linspace(-1,1, self._order+1) )

	def _calcNumberOfNodes(self):
		return( (self._order+1)**2 )

	def get( self, natural_coordinates, node_index ):
		return( self._lagrange.get( natural_coordinates[0] , node_index[0] )* \
				self._lagrange.get( natural_coordinates[1] , node_index[1] ) )

	def der( self, natural_coordinates, node_index, derivative_direction ):
		if( derivative_direction == 0 ):
			return( self._lagrange.der( natural_coordinates[0] , node_index[0] )* \
					self._lagrange.get( natural_coordinates[1] , node_index[1] ) )
		elif( derivative_direction == 1 ):
			return( self._lagrange.get( natural_coordinates[0] , node_index[0] )* \
					self._lagrange.der( natural_coordinates[1] , node_index[1] ) )

	def getNodeIndex( self, local_node_number ):
		if (local_node_number > 3) and (self._order < 2):
			raise Exception( 'For linear shape the local node number can not be greater then 3 - we got `'+str(local_node_number)+'`' )
		if (local_node_number > 8):
			raise Exception( 'QuadShape only implemented til order 2 - local node number can not be greater then 8 - we got `'+str(local_node_number)+'`' )
		if( local_node_number == 0 ):
			return( [0,0] )
		elif( local_node_number == 1 ):
			return( [self._order,0] )
		elif( local_node_number == 2 ):
			return( [self._order,self._order] )
		elif( local_node_number == 3 ):
			return( [0,self._order] )
		elif( local_node_number == 4 ):
			return( [1,0] )
		elif( local_node_number == 5 ):
			return( [2,1] )
		elif( local_node_number == 6 ):
			return( [1,2] )
		elif( local_node_number == 7 ):
			return( [0,1] )
		elif( local_node_number == 8 ):
			return( [1,1] )
