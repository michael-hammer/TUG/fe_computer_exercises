""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import numpy as np
from soofea.base import NumberedObject

#------------------------------------------------------------------
class Material(NumberedObject):
	def __init__(self, number):
		NumberedObject.__init__(self,number)

#------------------------------------------------------------------
class StVenantKirchhoffMaterial(Material):
	def __init__(self, N, E, nu, twodim_type='plane_strain'):
		Material.__init__(self, N)
		self._E = E
		self._nu = nu
		self._twodim_type = twodim_type

	def getElasticityMatrix( self , dimension ):
		if( dimension == 2):
			C = np.zeros([2,2])
			if( self._twodim_type == 'plane_strain' ):
				C = np.array([[1-self._nu,  self._nu,0],
							  [  self._nu,1-self._nu,0],
							  [  0, 0,(1-2*self._nu)/2]]) * self._E / ((1+self._nu)*(1-2*self._nu))
			elif( self._twodim_type == 'plane_stress' ):
				C = np.array([[       1,  self._nu,0],
							  [self._nu,          1,0],
							  [  0,  0,(1-self._nu)/2]]) * self._E / (1-self._nu**2)
		else:
			raise Exception("Only 2D St. Venant-Kirchhoff material implemented!")

		return( C )
