% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef XMLInputHandler < io.InputHandler
  
  properties( Access = protected )
    DOM;
  end
  
  methods
    function self = XMLInputHandler( filename )
      self@io.InputHandler( filename, 'xml' );
      self.DOM = xmlread( filename );
    end
    
    function m = createModel( self )
        global GlobalData;
      self.getGlobal();
      m = model.Model( GlobalData );
      self.addDataSets( m );
      self.addNodes( m );
      self.addElementTypes( m );
      self.addMaterials( m );
      self.addElements( m );
      self.addPrescribedDOF( m );
      self.addPrescribedLoad( m );
    end
  end
  
  methods( Access = protected )
    function getGlobal( self )
      global GlobalData;
      global_list = self.DOM.getElementsByTagName('global');
      GlobalData.dimension = str2double(global_list.item(0).getAttribute('dim'));
      GlobalData.problem_id = char(global_list.item(0).getAttribute('id'));
    end
        
    function addDataSets( self, m )
      data_set_list = self.getXMLList( 'data_set', 'ds' );
      for counter=0:data_set_list.getLength-1
        data_set = struct;
        xml_data_set = data_set_list.item(counter);
        data_set.number = str2double(xml_data_set.getAttribute('N'));
        data_list = xml_data_set.getElementsByTagName('data');
        for data_counter=0:data_list.getLength-1
          xml_data = data_list.item(data_counter);
          if strcmp( char(xml_data.getAttribute('type')) , 'double' )
            data_set.(char(xml_data.getAttribute('name'))) = str2double(xml_data.getAttribute('value'));
          elseif strcmp( char(xml_data.getAttribute('type')) , 'int' )
            data_set.(char(xml_data.getAttribute('name'))) = str2double(xml_data.getAttribute('value'));  
          elseif strcmp( char(xml_data.getAttribute('type')) , 'string' )
            data_set.(char(xml_data.getAttribute('name'))) = char(xml_data.getAttribute('value'));  
          end
        end
        m.addDataSet( data_set );
      end
    end
    
    function addNodes( self, m )
      global GlobalData;
      node_list = self.getXMLList( 'node', 'n' );
      for counter=0:node_list.getLength-1
        xml_node = node_list.item(counter);
        node_number = str2double(xml_node.getAttribute('N'));
        x = str2double(xml_node.getAttribute('x'));
        y = 0;
        z = 0;
        coords = [x];
        switch( GlobalData.dimension )
          case {2,3}
            y = str2double(xml_node.getAttribute('y'));
            coords = [coords y];
          case 3
            z = str2double(xml_node.getAttribute('z'));
            coords = [coords z];
        end
        node = model.Node(node_number, coords);
        m.addNode( node );
      end      
    end
    
    function addMaterials( self , m )
      material_list = self.getXMLList( 'material' , 'm');
      for counter=0:material_list.getLength-1
        xml_material = material_list.item(counter);
        material_number = str2double(xml_material.getAttribute('N'));
        id = char(xml_material.getAttribute('id'));
        ds_number = str2double(xml_material.getAttribute('ds'));
        switch( id )
          case 'st_venant_kirchhoff'
            new_mat = model.StVenantKirchhoffMaterial( material_number , m.getDataSet(ds_number) );
          case 'hyper_elastic'
            new_mat = model.HyperElasticMaterial( material_number , m.getDataSet(ds_number) );                 
        end
        m.addMaterial( new_mat );
      end
    end
    
    function addElements( self, m )
      element_list = self.getXMLList( 'element' , 'e' );
      for counter=0:element_list.getLength-1
        xml_element = element_list.item(counter);
        global_node_nums = str2num(xml_element.getAttribute('gnn'));
        element_number = str2double(xml_element.getAttribute('N'));
        node_array = [];
        for i=1:length(global_node_nums)
          node_array = [node_array,m.getNode(global_node_nums(i))];
        end
        et_number = str2double(xml_element.getAttribute('et'));
        material_number = str2double(xml_element.getAttribute('m'));
        element = model.Element(element_number,node_array,...
            m.getElementType(et_number),m.getMaterial(material_number));
        m.addElement( element );
      end
    end
    
    function addElementTypes( self, m )
      element_list = self.getXMLList( 'element_type' , 'et' );
      for counter=0:element_list.getLength-1
        xml_element = element_list.item(counter);
        element_type_number = str2double(xml_element.getAttribute('N'));
        ds_number = str2double(xml_element.getAttribute('ds'));
        id_string = char(xml_element.getAttribute('id'));
        int_points = str2num( xml_element.getAttribute('int_points') );
        element_type = model.ElementType(element_type_number, id_string, ...
          m.getDataSet(ds_number) , int_points );
        m.addElementType( element_type );
      end      
    end
    
    function addPrescribedDOF( self, m )
      constraint_list = self.getXMLList( 'prescribed_dof' , 'p' );
      for counter=0:constraint_list.getLength-1
        xml_constraint = constraint_list.item(counter);
        % constraint_number = str2double(xml_constraint.getAttribute('N'));
        node_number = str2double(xml_constraint.getAttribute('gnn'));
        attrs = xml_constraint.getAttributes;
        value_struct = struct();
        for i=0:attrs.getLength-1
          switch( char(attrs.item(i).getName) )
            case 'x'
              value_struct.x = str2double(attrs.item(i).getValue);
            case 'y'
              value_struct.y = str2double(attrs.item(i).getValue);
            case 'z'
              value_struct.z = str2double(attrs.item(i).getValue);
          end
        end
        m.integratePrescribedDOF( node_number , value_struct )
      end
    end
    
    function addPrescribedLoad( self, m )
      load_list = self.getXMLList( 'load' , 'l' );
      for counter=0:load_list.getLength-1
        xml_load = load_list.item(counter);
        % load_number = str2double(xml_load.getAttribute('N'));
        switch( char(xml_load.getAttribute('id')) )
          case 'force'
            node_number = str2double(xml_load.getAttribute('gnn'));
            load_vector = str2num(xml_load.getAttribute('vector'));
            m.integratePrescribedLoad( node_number , load_vector );
        end
      end
    end

    function list = getXMLList( self, container_tag , xml_tag )
      container_list = self.DOM.getElementsByTagName(container_tag);
      list = container_list.item(0).getElementsByTagName(xml_tag);
    end       
  end
end
