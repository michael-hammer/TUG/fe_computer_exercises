% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef NumIntIO < handle
  properties( Access = protected )
    DOM;
  end

  methods( Access = private )   
    function int_point_list_array = getIntPointListArray(self, boundary_id, ...
        integration_type, points_for_each_dimension )
      int_point_list_array = [];
      num_int_list = self.DOM.getElementsByTagName('int_type');
      for dim_counter = 1:length(points_for_each_dimension)
        for counter=0:num_int_list.getLength-1
          if integration_type == num_int_list.item(counter).getAttribute('type')
            if boundary_id == num_int_list.item(counter).getAttribute('id')
              order_list = num_int_list.item(counter).getElementsByTagName('order');
              for order_counter=0:order_list.getLength-1
                points = str2double(order_list.item(order_counter).getAttribute('points'));
                if points == points_for_each_dimension(dim_counter)
                  int_point_list_array = [int_point_list_array, ...
                    order_list.item(order_counter).getElementsByTagName('i')];
                  break;
                end
              end
              break;
            end
          end
        end
      end
    end
    
    function int_point_vector = getRectangularIntPointVector( self, points_for_each_dimension, ...
        int_point_list_array)
      int_point_vector = [];      
      switch( length(int_point_list_array) )
        case 1
          for counter = 0:points_for_each_dimension(1)-1
            signum = 1;
            if( mod(counter,2) )
              signum = -1.;
            end
            alpha = str2double(int_point_list_array(1).item(floor(counter/2)).getAttribute('alpha'));
            coord = str2double(int_point_list_array(1).item(floor(counter/2)).getAttribute('r'));
              
            int_point_vector = [ int_point_vector, ...
              model.IntegrationPoint( alpha, signum * coord ) ];          
          end
        case 2
          for counter_1 = 0:points_for_each_dimension(1)-1
            for counter_2 = 0:points_for_each_dimension(2)-1
              signum_1 = 1;
              signum_2 = 1;
              if( mod(counter_1,2) )
                signum_1 = -1.;
              end
              if( mod(counter_2,2) )
                signum_2 = -1.;
              end
              
              alpha = str2double(int_point_list_array(1).item(floor(counter_1/2)).getAttribute('alpha')) * ...
                str2double(int_point_list_array(2).item(floor(counter_2/2)).getAttribute('alpha'));
              
              coord_1 = str2double(int_point_list_array(1).item(floor(counter_1/2)).getAttribute('r'));
              coord_2 = str2double(int_point_list_array(2).item(floor(counter_2/2)).getAttribute('r'));
              
              int_point_vector = [ int_point_vector, ...
                model.IntegrationPoint( alpha, [ signum_1 * coord_1 , signum_2 * coord_2 ] ) ];
            end          
          end
        case 3
          for counter_1 = 0:points_for_each_dimension(1)-1
            for counter_2 = 0:points_for_each_dimension(2)-1
              for counter_3 = 0:points_for_each_dimension(3)-1
                signum_1 = 1;
                signum_2 = 1;
                signum_3 = 1;
                if( mod(counter_1,2) )
                  signum_1 = -1.;
                end
                if( mod(counter_2,2) )
                  signum_2 = -1.;
                end
                if( mod(counter_3,2) )
                  signum_3 = -1.;
                end
              
                alpha = str2double(int_point_list_array(1).item(floor(counter_1/2)).getAttribute('alpha')) * ...
                  str2double(int_point_list_array(2).item(floor(counter_2/2)).getAttribute('alpha')) * ...
                  str2double(int_point_list_array(3).item(floor(counter_3/2)).getAttribute('alpha'));
              
                coord_1 = str2double(int_point_list_array(1).item(floor(counter_1/2)).getAttribute('r'));
                coord_2 = str2double(int_point_list_array(2).item(floor(counter_2/2)).getAttribute('r'));
                coord_3 = str2double(int_point_list_array(3).item(floor(counter_3/2)).getAttribute('r'));
              
                int_point_vector = [ int_point_vector, ...
                  model.IntegrationPoint( alpha, [ signum_1 * coord_1 , signum_2 * coord_2 , signum_3 * coord_3 ] ) ];
              end
            end
          end
      end
    end
        
    function int_point_vector = getTriangleIntPointVector( self, points_for_each_dimension, ...
        int_point_list_array)
      int_point_vector = [];
      for counter = 0:int_point_list_array.getLength-1        
        r = str2double(int_point_list_array.item(counter).getAttribute('r'));
        s = str2double(int_point_list_array.item(counter).getAttribute('s'));
        t = 1 - (r+s);
        mul = str2double(int_point_list_array.item(counter).getAttribute('mul'));
        % IMPORTANT: multiply by 0.5 because weights are given for an integral approximation
        % as follows: F = 0.5 * SUM ( alpha_i * f(r_i,s_i) , i , 0 , n)
        alpha = 0.5 * str2double(int_point_list_array.item(counter).getAttribute('alpha'));    
        
        switch( mul )
          case 1
            int_point = model.IntegrationPoint( alpha, [r,s] );
            int_point_vector = [int_point_vector, int_point ];
          case 3
            if( abs(r - s) < eps(10) )
              int_point = model.IntegrationPoint( alpha, [r,s] );
              int_point_vector = [int_point_vector, int_point ];
              int_point = model.IntegrationPoint( alpha, [t,r] );
              int_point_vector = [int_point_vector, int_point ];
              int_point = model.IntegrationPoint( alpha, [r,t] );
              int_point_vector = [int_point_vector, int_point ];
            end
            if( abs(t - r) < eps(10) )
              int_point = model.IntegrationPoint( alpha, [r,s] );
              int_point_vector = [int_point_vector, int_point ];
              int_point = model.IntegrationPoint( alpha, [s,r] );
              int_point_vector = [int_point_vector, int_point ];
              int_point = model.IntegrationPoint( alpha, [t,r] );
              int_point_vector = [int_point_vector, int_point ];
            end
            if( abs(t - s) < eps(10) )
              int_point = model.IntegrationPoint( alpha, [r,s] );
              int_point_vector = [int_point_vector, int_point ];
              int_point = model.IntegrationPoint( alpha, [s,r] );
              int_point_vector = [int_point_vector, int_point ];
              int_point = model.IntegrationPoint( alpha, [t,s] );
              int_point_vector = [int_point_vector, int_point ];
            end
          case 6
            int_point = model.IntegrationPoint( alpha, [r,s] );
            int_point_vector = [int_point_vector, int_point ];
            int_point = model.IntegrationPoint( alpha, [s,r] );
            int_point_vector = [int_point_vector, int_point ];
            int_point = model.IntegrationPoint( alpha, [t,r] );
            int_point_vector = [int_point_vector, int_point ];
            int_point = model.IntegrationPoint( alpha, [r,t] );
            int_point_vector = [int_point_vector, int_point ];
            int_point = model.IntegrationPoint( alpha, [t,s] );
            int_point_vector = [int_point_vector, int_point ];
            int_point = model.IntegrationPoint( alpha, [s,t] );
        end
      end
    end
  end
  
  methods  
    function int_point_vector = getIntegrationPoints( self, ...
        boundary_id, ...
        integration_type, ...
        points_for_each_dimension )
      int_point_vector = [];
      
      int_point_list_array = self.getIntPointListArray(boundary_id, ...
        integration_type, points_for_each_dimension);

      switch( boundary_id )
        case 'rectangular'
          int_point_vector = self.getRectangularIntPointVector( points_for_each_dimension, ...
            int_point_list_array);
        case 'triangle'
          int_point_vector = self.getTriangleIntPointVector( points_for_each_dimension, ...
            int_point_list_array);          
      end
    end      

    function self = NumIntIO( num_int_file )
        self.DOM = xmlread( num_int_file );
    end        
  end
end
