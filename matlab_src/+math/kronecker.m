function delta = kronecker( dimension )
delta = tenzeros( dimension );
for i=1:dimension(1)
  for j=1:dimension(2)
    if( i == j )
      delta(i,j)=1;
    end
  end
end
end