% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef NumInt < handle
  methods( Static )   
    function ival = integrate( ...
        function_handle, integration_points, parameter_cell )
      
      ival = function_handle(integration_points(1),parameter_cell) * ...
          integration_points(1).weight;
      for i=2:length(integration_points)
        ival = ival + integration_points(i).weight * function_handle(integration_points(i),parameter_cell);
      end
    end
  end
end
