% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

function model = soofeam( filename )

disp '========================================================='
disp ' SOOFEAM - '
disp '    Software for Object Oriented Finite Element Analysis '
disp '                                       written in MATLAB '
disp ' Copyright 2008-2011 Hammer Michael                      '
disp '========================================================='

disp(['Loading input file <',filename,'> ...']);
input_handler = io.XMLInputHandler( filename );

num_int_file = '../etc/soofea/soofea_num_int.xml';
disp(['Lodaing numerical integration file <',num_int_file,'> ...']);
global num_int_io;
num_int_io = math.NumIntIO( num_int_file );

disp('Creating Model ...');
model = input_handler.createModel();

disp('Start Analysis: ');
analysis = analyze.Analysis( model );
analysis.run();

end
