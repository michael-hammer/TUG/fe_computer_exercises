% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

function shape(type,order,draw)
close;

switch(type)
  case 't'
    % Triangle
    switch( order )
      case 1
        nodes = [-0.3,0.2,0.1;
          2.2,0,1.2;
          0,-1,1.3]';
        unit_nodes = [0,0,1;
          1,0,0;
          0,1,0]';
      case 2
        nodes = [0,0,0;
          1,0.2,0.1;
          0.2,1,0.3;
          0.5,0.25,0.2;
          0.5,0.5,0.25;
          0.15,0.45,-0.1]';
        unit_nodes = [0,0,1;
          1,0,0;
          0,1,0;
          0.5,0,0;
          0.25,0.25,0;
          0,0.5,0]';
      otherwise
        disp('only shape=1 or 2 supported')
        return
    end
    s = model.shape.TriangleShape(order);
  case 'q'
    switch( order )
      case 1
        nodes = [-0.3,0.2,0.1;
          2.2,0,0.8;
          1.8,1.9,1.3;
          0.2,1.0,-.2]';
        unit_nodes = [-1,-1,1;
          1,-1,0;
          1,1,0;
          -1,1,0]';
      case 2
        nodes = [-0.3,0.2,0.1;
           2.2, 0.0, 0.8;
           1.8, 1.9, 1.3;
           0.0, 1.0, 0.1;
           1.0,-0.1, 0.0;
           2.3, 0.9, 1.3;
           1.0, 1.8, 0.8;
           0.0, 0.6, 0.1;
           1.0, 0.8,-0.1]';
        unit_nodes = [-1,-1,1;
          1,-1,0;
          1,1,0;
          -1,1,0;
          0,-1,0;
          1,0,0;
          0,1,0;
          -1,0,0;
          0,0,0]';
      otherwise
        disp('only shape=1 or 2 supported')
        return
    end
    s = model.shape.QuadShape(order);
  otherwise
    disp(['your type "',type,'" is not defined!'])
    return
end

figure(1); hold on;
switch( type )
  case 't'
    [R,S] = meshgrid(0:0.05:1);
  case 'q'
    [R,S] = meshgrid(-1:0.1:1);
end
X = zeros(size(R));
Y = zeros(size(R));
Z = zeros(size(R));
switch( draw )
  case 's'
    node_tensor = tensor(nodes);
  case 'h'
    node_tensor = tensor(unit_nodes);   
end

for i=1:size(R,1)
  for j=1:size(R,2)
    if( type == 't' && R(i,j)+S(i,j) >= 1 )
      X(i,j) = NaN; Y(i,j) = NaN; Z(i,j) = NaN;
      continue
    end
    N = s.getTensor([R(i,j),S(i,j)]);
    point = double(tenmat(ttt(node_tensor,N,2,1),1));
    X(i,j) = point(1);
    Y(i,j) = point(2);
    Z(i,j) = point(3);
  end
end
surf(X,Y,Z);

for i=1:size(node_tensor,2)
  text(node_tensor(1,i),node_tensor(2,i),node_tensor(3,i),int2str(i));
end
scatter3(node_tensor(1,:),node_tensor(2,:),node_tensor(3,:));

axis equal; grid on;

end

