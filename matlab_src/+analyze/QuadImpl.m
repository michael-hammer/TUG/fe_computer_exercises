% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef QuadImpl < analyze.ElementImpl
  methods
    function k_element = calcElementStiffness( self, element )
      int_points = element.integration_points;
      parameter_cell = {element};
      k_element = ...
        math.NumInt.integrate( ...
        @(int_point,parameter_cell)integrator(self, int_point, parameter_cell), ...
        int_points, parameter_cell ); %, [dim*n_amount,dim*n_amount]);
    end
    
    function int_matrix = integrator( self, int_point, parameter_cell )
      element = parameter_cell{1};
      n_amount = element.element_type.shape.node_amount;
      
      J = analyze.Jacobian(element);
      lagrangian_j_inv = J.getInv( int_point );
      der_H_reference = element.element_type.shape.getDerivativeMatrix(int_point.natural_coordinates);
      der_H = lagrangian_j_inv * der_H_reference;
      C = element.material.getCMatrix();

      B = zeros(3,2*n_amount);
      for node_counter = 1:n_amount
          B( 1 , 2*node_counter - 1 ) = der_H(1,node_counter);
          B( 2 , 2*node_counter     ) = der_H(2,node_counter);
          B( 3 , 2*node_counter - 1 ) = der_H(2,node_counter);
          B( 3 , 2*node_counter     ) = der_H(1,node_counter);
      end
      
      int_matrix = B' * C * B * element.element_type.data_set.height * J.getDet( int_point );
    end
  end
end