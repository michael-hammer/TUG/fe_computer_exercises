% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef Analysis < handle
  
  properties( Access = protected )
    model;
    model_progression;
    bc_handler;
  end
  
  methods
    function self = Analysis( model )
      self.model = model;
    end
  end
  
  methods
      function run(self)
          disp('> Analysis');
          
          load_vector = [];
          global_stiffness = [];
          [global_stiffness,load_vector] = self.assembleK(global_stiffness,load_vector);
          full(global_stiffness)
          det(global_stiffness)
          % global_bevore_BC = global_stiffness;
          [global_stiffness,load_vector] = self.integrateDirichletBC(global_stiffness,load_vector);
          [global_stiffness,load_vector] = self.integrateNeumannBC(global_stiffness,load_vector);
          
          disp('>  Solve System...');
          solution = global_stiffness\load_vector;
          disp('>  ...finished');
          self.updateDOF(solution);
          
      end
      
      function [global_stiffness,load_vector] = ...
              assembleK(self,global_stiffness,load_vector)
          disp('>  Assemble global stiffness');
          
          global GlobalData;
          dim = GlobalData.dimension;
          
          load_vector = zeros(self.model.nodeAmount()*dim,1);
          global_stiffness = sparse(...
              self.model.nodeAmount()*dim,...
              self.model.nodeAmount()*dim);
          
          for element_counter = 1:1:self.model.elementAmount
              element = self.model.getElement(element_counter);
              element_impl = element.element_type.implementation();
              
              %############################################################
              k_element = element_impl.calcElementStiffness( element );
              %############################################################
              
              for e_row_counter = 1:length(k_element)
                  for e_col_counter = 1:length(k_element)
                      
                      row_node = element.node_array(ceil(e_row_counter/dim));
                      col_node = element.node_array(ceil(e_col_counter/dim));
                      
                      row_coord_offset = mod(e_row_counter-1,dim)+1;
                      col_coord_offset = mod(e_col_counter-1,dim)+1;
                      
                      row_index = (row_node.number-1)*dim + row_coord_offset;
                      col_index = (col_node.number-1)*dim + col_coord_offset;
                      
                      global_stiffness(row_index,col_index ) = ...
                          global_stiffness(row_index,col_index ) + k_element(e_row_counter,e_col_counter);
                  end
              end
          end
      end
      
      function [global_stiffness,load_vector] = ...
              integrateDirichletBC( self, global_stiffness,load_vector )
          
          global GlobalData;
          dim = GlobalData.dimension;
          
          for row_node_counter = 1:self.model.nodeAmount()
              for col_node_counter = 1:self.model.nodeAmount()
                  row_node = self.model.getNode(row_node_counter);
                  col_node = self.model.getNode(col_node_counter);
                  
                  for row_coord_offset = 1:dim
                      for col_coord_offset = 1:dim
                          row_index = (row_node.number-1)*dim + row_coord_offset;
                          col_index = (col_node.number-1)*dim + col_coord_offset;
                          
                          row_dof = row_node.getDisplacementDOF();
                          col_dof = col_node.getDisplacementDOF();
                          
                          if( col_dof.constraint(col_coord_offset) )
                              if( ~row_dof.constraint(row_coord_offset) ) % Step 1.
                                  load_vector( row_index ) = load_vector( row_index ) - ...
                                      global_stiffness(row_index,col_index) * col_dof.getValue(col_coord_offset);
                              end
                              
                              if( col_index == row_index ) % Step 3. (Which means implicit that (row_dof_value.constraint == True))
                                  load_vector( row_index ) = global_stiffness(row_index,col_index) * col_dof.getValue(col_coord_offset);
                              else
                                  global_stiffness(row_index,col_index) = 0; % Step 2.
                              end
                          else
                              if( row_dof.constraint(row_coord_offset) )
                                  if( col_index ~= row_index )
                                      global_stiffness(row_index,col_index) = 0; % Step 2.
                                  end
                              end
                          end
                      end
                  end
              end
          end
      end
      
      function [global_stiffness,load_vector] = ...
              integrateNeumannBC(self,global_stiffness,load_vector)
          disp('>  Assemble load vector');
          global GlobalData;
          dim = GlobalData.dimension;
          
          for node_counter = 1:1:self.model.nodeAmount
              force_load = self.model.getNode(node_counter).getForceLoad();
              if( ~ isempty(force_load) )
                  node_number = self.model.getNode(node_counter).number;
                  for coord_offset = 1:length(force_load.getForceVector())
                      index = (node_number-1)*dim + coord_offset;
                      
                      load_vector(index) = ...
                          load_vector(index) + force_load.getValue(coord_offset);
                  end
              end
          end
      end
      
      function updateDOF(self, solution_vector)
          global GlobalData;
          dim = GlobalData.dimension;
          
          for node_counter = 1:1:self.model.nodeAmount
              for dim_counter=1:dim
                  index = (node_counter-1)*dim + dim_counter;
                  self.model.getNode(node_counter).updatePoint( solution_vector(index) , dim_counter );
              end
          end
      end
  end
end
