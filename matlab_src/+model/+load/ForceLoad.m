% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef ForceLoad < model.load.LoadType
  properties( SetAccess = protected )
    force = [];
  end
  
  methods
    function self = ForceLoad( force )
      self@model.load.LoadType('force');
      self.force = force;
    end
    
    function value = getValue( self, coord_id )
      value = self.force(coord_id);
    end
    
    function setValue(self, coord_id, value)
      self.force(coord_id) = value;
    end
    
    function force_vector = getForceVector( self )
      force_vector = self.force;
    end
  end
end
