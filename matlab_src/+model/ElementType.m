% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef ElementType < model.NumberedObject
  
  properties( SetAccess = protected )
    id = '';
    int_points_amount = [];
    shape;
    implementation;
    data_set;
  end
  
  methods
    function self = ElementType( number , id , data_set ,...
        int_points_amount )
      self@model.NumberedObject(number);
      self.id = id;
      self.data_set = data_set;
      self.int_points_amount = int_points_amount ;
      switch( id )
        case 'triangle'
          self.shape = model.shape.TriangleShape( data_set.shape_order );
          self.implementation = analyze.triangle.TriangleImpl();
        case 'quad'
          self.shape = model.shape.QuadShape( data_set.shape_order );
          self.implementation = analyze.QuadImpl();
      end
    end
    
    function disp(self)
      disp( ['ElementType "', self.id , '": ', int2str(self.number)] );
    end
  end
end
