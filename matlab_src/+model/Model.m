% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef Model < handle
  
  properties( SetAccess = protected )
    global_data;
    node_cell = {};
    element_cell = {};
    element_type_cell = {};
    data_set_cell = {};
    material_cell = {};
  end
  
  methods
    function self = Model( global_data )
      self.global_data = global_data;
    end
    
    function addNode( self, node )
      self.node_cell{node.number} = node;
    end
    
    function addElementType( self, element_type )
      self.element_type_cell{element_type.number} = element_type;
    end
    
    function addElement( self, element )
      self.element_cell{element.number} = element;
    end
    
    function addDataSet( self, data_set )
      self.data_set_cell{data_set.number} = data_set;
    end

    function addMaterial( self, material )
      self.material_cell{material.number} = material;
    end
    
    function node = getNode( self, number )
      node = self.node_cell{number};
    end

    function node_amount = nodeAmount( self )
      % what if node numbers are not used?
      node_amount = length(self.node_cell);
    end
       
    function element = getElement( self, element_number )
      element = self.element_cell{element_number};
    end

    function element_amount = elementAmount( self )
      % what if element numbers are not used?
      element_amount = length(self.element_cell);
    end
    
    function element_type = getElementType( self, element_type_number )
      element_type = self.element_type_cell{element_type_number};
    end
    
    function material = getMaterial( self, number )
      material = self.material_cell{number};
    end
    
    function integratePrescribedDOF( self, node_number, pdof_values )
        if( size(self.getNode(node_number).dof_cell,2) < 1 )
            disp('Node does not have DisplacementDOF yet!');
            return
        end
        coord_ids = fieldnames(pdof_values);
        for i=1:length(coord_ids)
            switch( coord_ids{i} )
                case 'x'
                    self.getNode(node_number).getDisplacementDOF().setConstraintValue(1,pdof_values.x);
                case 'y'
                    self.getNode(node_number).getDisplacementDOF().setConstraintValue(2,pdof_values.y);
                case 'z'
                    self.getNode(node_number).getDisplacementDOF().setConstraintValue(3,pdof_values.z);
            end
        end
    end
    
    function integratePrescribedLoad( self , node_number , load_vector )
        self.getNode(node_number).setForceLoad(load_vector);
    end
    
    function disp( self )
      disp(self.global_data);
      for i=1:length(self.element_cell)
        disp(self.element_cell{i});
      end
    end

    %----------------------------------------------------------------------

    function data_set = getDataSet( self, number )
      data_set = self.data_set_cell{number};
    end
  end
end
