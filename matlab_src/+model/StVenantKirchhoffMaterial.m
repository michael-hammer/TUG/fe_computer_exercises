% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef StVenantKirchhoffMaterial < model.Material
  
  methods
    function self = StVenantKirchhoffMaterial( number , data_set )
      self@model.Material( number , data_set );
    end
    
    function c_tensor = getCTensor( self )
      if ~ strcmp(self.data_set.type, 'plane_strain' )
       error('With index notation only plane_strain as material type is supported!');
      end       
        
      global global_data;
      dimension = global_data.dimension;
      lambda = self.data_set.youngs_modulus * self.data_set.poisson_ratio / ...
        ((1+self.data_set.poisson_ratio)*(1-2*self.data_set.poisson_ratio));
      mu = self.data_set.youngs_modulus / ...
        (2*(1+self.data_set.poisson_ratio));

      delta = math.kronecker([dimension dimension]);
      d_ijkl = ttt(delta,delta);
      % lambda * delta_ijkl + 2*mu* 1/2 * ( delta_ikjl + delta_ilkj )
      c_tensor = lambda * d_ijkl + mu * ( permute(d_ijkl,[1 3 2 4]) + permute(d_ijkl,[1 4 3 2]));
    end
    
    function c_matrix = getCMatrix( self )
        c_matrix = zeros(3,3);
        nu = self.data_set.poisson_ratio;
        E = self.data_set.youngs_modulus;       
        if strcmp(self.data_set.type,'plane_strain')
            c_matrix = [ 1-nu ,  nu , 0 ; nu , 1-nu , 0 ; 0 ,0 , (1-2*nu)/2 ] * E/((1+nu)*(1-2*nu));
        elseif strcmp(self.data_set.type,'plane_stress')
            c_matrix = [ 1 ,  nu , 0 ; nu , 1 , 0 ; 0 ,0 , (1-nu)/2 ] * E/(1-nu^2);            
        end
    end
  end
end