% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef Element < model.NumberedObject
  
  properties( SetAccess = protected )
    node_array = [];
    element_type;
    material;
    integration_points;
  end 
  
  methods
    function self = Element( number , nodes , element_type , material )
      global num_int_io;
      self@model.NumberedObject(number);
      self.node_array = nodes;
      self.element_type = element_type;
      self.material = material;
      switch( element_type.id )
        case 'triangle'
          self.integration_points = num_int_io.getIntegrationPoints('triangle', ...
            'gauss_legendre', element_type.int_points_amount );
        case 'quad'
          self.integration_points = num_int_io.getIntegrationPoints('rectangular', ...
            'gauss_legendre', element_type.int_points_amount );
      end
    end
       
    function node_matrix = getNodeMatrix( self )
      global GlobalData;
      node_matrix = zeros([length(self.node_array) GlobalData.dimension]);
      for node_counter = 1:1:length(self.node_array)
        node_matrix(node_counter,:) = self.node_array(node_counter).lagrangian_coords;
      end
    end
       
    function vis( self, axes_handle , config )
      global GlobalData;
      node_nr_array = self.element_type.shape.getBoundary();
      switch( GlobalData.dimension )
        case 2
          coord_1 = self.node_array(node_nr_array(end)).lagrangian_coords;
          for node_counter = 1:1:(length(node_nr_array))
            coord_2 = self.node_array(node_nr_array(node_counter)).lagrangian_coords;
            plot( axes_handle , ...
              [ coord_1(1) , coord_2(1) ], ...
              [ coord_1(2) , coord_2(2) ]);
            coord_1 = coord_2;
          end
          if( config.show.deformed )
            euler_1 = self.node_array(node_nr_array(end)).lagrangian_coords + ...
              (self.node_array(node_nr_array(end)).getDisplacementDOFVector() * config.scaling)';
            for node_counter = 1:1:(length(node_nr_array))
              euler_2 = self.node_array(node_nr_array(node_counter)).lagrangian_coords + ...
                (self.node_array(node_nr_array(node_counter)).getDisplacementDOFVector() * config.scaling)';
              plot( axes_handle , ...
                [ euler_1(1) , euler_2(1) ], ...
                [ euler_1(2) , euler_2(2) ],'r-');
              euler_1 = euler_2;
            end
          end
      end
    end
    
    function disp( self )
        disp('------------------------------------------------------------');
        disp(['Element ',num2str(self.number)]);
        disp( self.element_type );
        for i=1:length(self.node_array)
            disp(self.node_array(i));
        end
    end
  end
end
