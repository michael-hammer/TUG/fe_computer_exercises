% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef DisplacementDOF < model.dof.DOFType
  properties( SetAccess = protected )
      displacement = [];
      constraint = [];
  end
  
  methods
    function self = DisplacementDOF( dimension )
      self@model.dof.DOFType('displacement');
      self.displacement = zeros(1,dimension);
      self.constraint = zeros(1,dimension);
    end
    
    function value = getValue( self , coord_id )
      value = self.displacement(coord_id);
    end
    
    function setValue(self, coord_id, value)
      self.displacement(coord_id) = value;
    end
    
    function setConstraintValue(self, coord_id, value)
      self.displacement(coord_id) = value;
      self.constraint(coord_id) = true;
    end
    
    function displacement_vector = getDisplacementVector( self )
        displacement_vector = self.displacement;
    end
  end
end
