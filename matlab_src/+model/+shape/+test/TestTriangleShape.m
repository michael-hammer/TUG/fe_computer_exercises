% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef TestTriangleShape < mlunit.test_case
  properties(SetAccess = protected)
    linear_shape_values = [ 0 0 1 1 1
      1 0 1 1 0
      0 1 1 1 0
      0 0 2 1 0
      1 0 2 1 1
      0 1 2 1 0
      0 0 1 2 0
      1 0 1 2 0
      0 1 1 2 1];
  end

  methods
    function self = TestTriangleShape(varargin)
      self = self@mlunit.test_case(varargin{:});
    end
    
    function self = testNodeAmount(self)
      shape = model.shape.TriangleShape(1);
      mlunit.assert_equals(shape.node_amount,3)
      shape = model.shape.TriangleShape(2);
      mlunit.assert_equals(shape.node_amount,6)
    end
    
    function self = testShape(self)
      % linear
      shape = model.shape.TriangleShape(1);
      for i=1:length(self.linear_shape_values)
        mlunit.assert_equals( ...
          shape.get(self.linear_shape_values(i,1:2), ...
          [self.linear_shape_values(i,3),self.linear_shape_values(i,4)] ), ...
          self.linear_shape_values(i,5) )
      end
    end
    
    function self = testNodeIndex(self)
      % linear
      shape = model.shape.TriangleShape(1);
      for count = 1:shape.node_amount
        node_index = shape.getNodeIndex(count);
        switch count
          case 1
            mlunit.assert_equals( node_index , [1,1] )
          case 2
            mlunit.assert_equals( node_index , [2,1] )
          case 3
            mlunit.assert_equals( node_index , [1,2] )
        end
      end
      % quadratic
      shape = model.shape.TriangleShape(2);
      for count = 1:shape.node_amount
        node_index = shape.getNodeIndex(count);
        switch count
          case 1
            mlunit.assert_equals( node_index , [1,1] )
          case 2
            mlunit.assert_equals( node_index , [3,1] )
          case 3
            mlunit.assert_equals( node_index , [1,3] )
          case 4
            mlunit.assert_equals( node_index , [2,1] )
          case 5
            mlunit.assert_equals( node_index , [2,2] )
          case 6
            mlunit.assert_equals( node_index , [1,2] )
        end
      end      
    end
        
    function self = testLinearDerivativeShape(self)
      mlunit.assert_equals(1, 1);
    end
  end
end