% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef TestQuadShape < mlunit.test_case
  properties(SetAccess = protected)
    linear_shape_values = [ -1 -1 1 1 1
      1 -1 1 1 0
      -1 1 1 1 0 
      1 1 1 1 0
      -1 -1 2 1 0
      1 -1 2 1 1
      -1 1 2 1 0
      1 1 2 1 0
      -1 -1 1 2 0
      1 -1 1 2 0
      -1 1 1 2 1
      1 1 1 2 0
      -1 -1 2 2 0
      1 -1 2 2 0
      -1 1 2 2 0
      1 1 2 2 1];
  end

  methods
    function self = TestQuadShape(varargin)
      self = self@mlunit.test_case(varargin{:});
    end
    
    function self = testNodeAmount(self)
      shape = model.shape.QuadShape(1);
      mlunit.assert_equals(shape.node_amount,4)
      shape = model.shape.QuadShape(2);
      mlunit.assert_equals(shape.node_amount,9)
    end
    
    function self = testQuadShape(self)
      % linear
      shape = model.shape.QuadShape(1);
      for i=1:length(self.linear_shape_values)
        mlunit.assert_equals( ...
          shape.get(self.linear_shape_values(i,1:2), ...
          [self.linear_shape_values(i,3),self.linear_shape_values(i,4)] ), ...
          self.linear_shape_values(i,5) )
      end
    end

    function self = testNodeIndex(self)
      % linear
      shape = model.shape.QuadShape(1);
      for count = 1:shape.node_amount
        node_index = shape.getNodeIndex(count);
        switch count
          case 1
            mlunit.assert_equals( node_index , [1,1] )
          case 2
            mlunit.assert_equals( node_index , [2,1] )
          case 3
            mlunit.assert_equals( node_index , [2,2] )
          case 4
            mlunit.assert_equals( node_index , [1,2] )
        end
      end
      % quadratic
      shape = model.shape.QuadShape(2);
      for count = 1:shape.node_amount
        node_index = shape.getNodeIndex(count);
        switch count
          case 1
            mlunit.assert_equals( node_index , [1,1] )
          case 2
            mlunit.assert_equals( node_index , [3,1] )
          case 3
            mlunit.assert_equals( node_index , [3,3] )
          case 4
            mlunit.assert_equals( node_index , [1,3] )
          case 5
            mlunit.assert_equals( node_index , [2,1] )
          case 6
            mlunit.assert_equals( node_index , [3,2] )
          case 7
            mlunit.assert_equals( node_index , [2,3] )
          case 8
            mlunit.assert_equals( node_index , [1,2] )
          case 9
            mlunit.assert_equals( node_index , [2,2] )
        end
      end      
    end
    
    function self = testLinearDerivativeShape(self)
      mlunit.assert_equals(1, 1);
    end
  end
end