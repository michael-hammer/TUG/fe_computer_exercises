% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef SubdivShape < model.shape.Shape
  methods (Access=public)
    %------------------------------------------------------------------
    function n_val = getShape(obj,u,v,w,node)
      % getShape(u,v,w,node) Return value of derived shape functions
      % u           ... u coordinate
      % v           ... v coordinate
      % w           ... w coordinate
      % node        ... local node number [1..12]

      N=zeros(12,1);
      N(1) = 1/12 * ((u^4) + 2*(u^3)*v);
      N(2) = 1/12 * ((u^4) + 2*(u^3)*w);
      N(3) = 1/12 * ((u^4) + 2*(u^3)*w + 6*(u^3)*v + 6*(u^2)*v*w + 12*(u^2)*v^2 + 6*u*(v^2)*w + 6*u*v^3 + 2*(v^3)*w + v^4);
      N(4) = 1/12 * (6*(u^4) + 24*(u^3)*w + 24*(u^2)*w^2 + 8*u*w^3 + w^4 + 24*(u^3)*v + 60*(u^2)*v*w + 36*u*v*w^2 + 6*v*w^3 + 24*(u^2)*v^2 + 36*u*(v^2)*w + 12*(v^2)*w^2 + 8*u*v^3 + 6*(v^3)*w + v^4);
      N(5) = 1/12 * ((u^4) + 6*(u^3)*w + 12*(u^2)*w^2 + 6*u*w^3 + w^4 + 2*(u^3)*v + 6*(u^2)*v*w + 6*u*v*w^2 + 2*v*w^3);
      N(6) = 1/12 * (2*u*v^3 + v^4);
      N(7) = 1/12 * ((u^4) + 6*(u^3)*w + 12*(u^2)*w^2 + 6*u*w^3 + w^4 + 8*(u^3)*v + 36*(u^2)*v*w + 36*u*v*w^2 + 8*v*w^3 + 24*(u^2)*v^2 + 60*u*(v^2)*w + 24*(v^2)*w^2 + 24*u*v^3 + 24*(v^3)*w + 6*v^4);
      N(8) = 1/12 * ((u^4) + 8*(u^3)*w + 24*(u^2)*w^2 + 24*u*w^3 + 6*w^4 + 6*(u^3)*v + 36*(u^2)*v*w + 60*u*v*w^2 + 24*v*w^3 + 12*(u^2)*v^2 + 36*u*(v^2)*w + 24*(v^2)*w^2 + 6*u*v^3 + 8*(v^3)*w + v^4);
      N(9) = 1/12 * (2*u*w^3 + w^4);
      N(10) = 1/12 * (2*(v^3)*w + v^4);
      N(11) = 1/12 * (2*u*w^3 + w^4 + 6*u*v*w^2 + 6*v*w^3 + 6*u*(v^2)*w + 12*(v^2)*w^2 + 2*u*v^3 + 6*(v^3)*w + v^4);
      N(12) = 1/12 * (w^4 + 2*v*w^3);

      n_val = N(node);
    end

    %------------------------------------------------------------------
    function nd_val = getDerivedShape(obj,v,w,node,d_direction,d_order)
      % getDerivedShape(v,w,node,d_direction,d_order) Return value of
      %   derived shape functions
      % v           ... v coordinate
      % w           ... w coordinate
      % node        ... local node number [1..12]
      % d_direction ... 1 <- v direction
      %                 2 <- w direction
      %                 3 <- vw direction (only if d_order = 2)
      % d_order     ... order of derivative

      %Nder(node,d_direction,d_order);
      Nder = zeros(12,3,2);

      Nder(1,1,1) = -1/6+1/2*w+1/2*v^2-1/2*w^2-1/3*v^3-1/2*v^2*w+1/6*w^3;
      Nder(1,2,1) = -1/3+1/2*v+w-v*w-w^2-1/6*v^3+1/2*v*w^2+1/3*w^3;
      Nder(1,1,2) = -(-1+v+w)*v;
      Nder(1,2,2) = 1-v-2*w+v*w+w^2;
      Nder(1,3,2) = 1/2-w-1/2*v^2+1/2*w^2;
      Nder(2,1,1) = -1/3+v+1/2*w-v^2-v*w+1/3*v^3+1/2*v^2*w-1/6*w^3;
      Nder(2,2,1) = -1/6+1/2*v-1/2*v^2+1/2*w^2+1/6*v^3-1/2*v*w^2-1/3*w^3;
      Nder(2,1,2) = 1-2*v-w+v^2+v*w;
      Nder(2,2,2) = -(-1+v+w)*w;
      Nder(2,3,2) = 1/2-v+1/2*v^2-1/2*w^2;
      Nder(3,1,1) = 1/2*w^2-1/2*w+v^2*w+1/6-v^2+2/3*v^3-1/6*w^3;
      Nder(3,2,1) = -1/2*v+1/2*w^2-1/2*v*w^2+v*w-1/6+1/3*v^3-1/3*w^3;
      Nder(3,1,2) = -2*v+2*v^2+2*v*w;
      Nder(3,2,2) = w-v*w-w^2+v;
      Nder(3,3,2) = -1/2+w+v^2-1/2*w^2;
      Nder(4,1,1) = -2*v+w^2+2*v*w-w-1/2*v^2*w+2*v^2-1/3*v^3-1/6*w^3;
      Nder(4,2,1) = -v+2*w^2-1/2*v*w^2+2*v*w-2*w+v^2-1/6*v^3-1/3*w^3;
      Nder(4,1,2) = 2*w-v*w+4*v-v^2-2;
      Nder(4,2,2) = 4*w-v*w-w^2-2+2*v;
      Nder(4,3,2) = 2*w-1/2*w^2-1+2*v-1/2*v^2;
      Nder(5,1,1) = v*w-1/2*w-1/2*v^2*w-1/6+1/2*v^2-1/3*v^3+1/3*w^3;
      Nder(5,2,1) = -1/2*v-w^2+v*w^2+1/6+1/2*v^2-1/6*v^3+2/3*w^3;
      Nder(5,1,2) = w-v*w+v-v^2;
      Nder(5,2,2) = -2*w+2*v*w+2*w^2;
      Nder(5,3,2) = -1/2+v-1/2*v^2+w^2;
      Nder(6,1,1) = -1/3*v^3+1/2*v^2-1/2*v^2*w;
      Nder(6,2,1) = -1/6*v^3;
      Nder(6,1,2) = -(-1+v+w)*v;
      Nder(6,2,2) = 0;
      Nder(6,3,2) = -1/2*v^2;
      Nder(7,1,1) = v-w^2-v*w+1/2*w-1/2*v^2*w+1/3-v^2-1/3*v^3+1/3*w^3;
      Nder(7,2,1) = 1/2*v-w^2+v*w^2-2*v*w+1/6-1/2*v^2-1/6*v^3+2/3*w^3;
      Nder(7,1,2) = 1-2*v-w-v^2-v*w;
      Nder(7,2,2) = -2*w+2*v*w+2*w^2-2*v;
      Nder(7,3,2) = 1/2-v-2*w-1/2*v^2+w^2;
      Nder(8,1,1) = -1/2*w^2-2*v*w+1/2*w+v^2*w+1/6-v^2+2/3*v^3-1/6*w^3;
      Nder(8,2,1) = 1/2*v-w^2-1/2*v*w^2-v*w+w+1/3-v^2+1/3*v^3-1/3*w^3;
      Nder(8,1,2) = -2*w+2*v*w-2*v+2*v^2;
      Nder(8,2,2) = 1-v-2*w-v*w-w^2;
      Nder(8,3,2) = 1/2-2*v-w+v^2-1/2*w^2;
      Nder(9,1,1) = -1/6*w^3;
      Nder(9,2,1) = -1/3*w^3+1/2*w^2-1/2*v*w^2;
      Nder(9,1,2) = 0;
      Nder(9,2,2) = -(-1+v+w)*w;
      Nder(9,3,2) = -1/2*w^2;
      Nder(10,1,1) = 1/2*v^2*w+1/3*v^3;
      Nder(10,2,1) = 1/6*v^3;
      Nder(10,1,2) = v*w+v^2;
      Nder(10,2,2) = 0;
      Nder(10,3,2) = 1/2*v^2;
      Nder(11,1,1) = -1/6*w^3+1/2*w^2-1/2*v^2*w+v*w-1/3*v^3+1/2*v^2;
      Nder(11,2,1) = -1/3*w^3+1/2*w^2-1/2*v*w^2+v*w+1/2*v^2-1/6*v^3;
      Nder(11,1,2) = w-v*w+v-v^2;
      Nder(11,2,2) = w-v*w-w^2+v;
      Nder(11,3,2) = -1/2*w^2+w-1/2*v^2+v;
      Nder(12,1,1) = 1/6*w^3;
      Nder(12,2,1) = 1/3*w^3+1/2*v*w^2;
      Nder(12,1,2) = 0;
      Nder(12,2,2) = w^2+v*w;
      Nder(12,3,2) = 1/2*w^2;

      nd_val = Nder(node,d_direction,d_order);
    end

    %------------------------------------------------------------------
    function n_matrix = getShapeMatrix(obj,v,w)
      n_matrix = zeros(12,1);

      for node = 1:1:12
        n_matrix(node) = obj.getShape(1-v-w,v,w,node);
      end
    end

    %------------------------------------------------------------------
    function nd_matrix = getDerivedShapeMatrix(obj,v,w)
      % getDerivedShape(v,w,d_direction) Return value of first
      %   derivative of shape functions returned in a Matrix
      % v           ... v coordinate
      % w           ... w coordinate

      nd_matrix = zeros(12,2);

      for node=1:1:12
        nd_matrix(node,1) = obj.getDerivedShape(v,w,node,1,1);
        nd_matrix(node,2) = obj.getDerivedShape(v,w,node,2,1);
      end
    end

    %------------------------------------------------------------------
    function ndd_matrix = getSecondDerivedShapeMatrix(obj,v,w)
      % getSecondDerivedShape(v,w,d_direction) Return value of second
      %   derivative of shape functions returned in a Matrix
      % v           ... v coordinate
      % w           ... w coordinate

      ndd_matrix = zeros(12,3);

      for node=1:1:12
        ndd_matrix(node,1) = obj.getDerivedShape(v,w,node,1,2);
        ndd_matrix(node,2) = obj.getDerivedShape(v,w,node,2,2);
        ndd_matrix(node,3) = obj.getDerivedShape(v,w,node,3,2);
      end
    end

  end
end
