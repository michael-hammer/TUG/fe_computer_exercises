% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef Lagrange < handle
  properties( SetAccess = protected )
    order;
    coordinates;
  end

  methods
    function self = Lagrange( coordinates )
      %
      self.order = length(coordinates)-1;
      self.coordinates = coordinates;
    end
    
    function L = get( self, xi , index, highest_index )
      %
      if nargin <= 3
        hi = self.order+1;
      else
        hi = highest_index;
      end
      L = 1;
      for count=1:(hi)
        if (count~=index)
          L = L*(xi - self.coordinates(count)) / ...
            (self.coordinates(index) - self.coordinates(count));
        end
      end
    end
    
    function derivative = der( self, xi, index, highest_index )
      if nargin <= 3
        hi = self.order+1;
      else
        hi = highest_index;
      end
      constant_part = 1;
      % calc constant part
      for prod_counter=1:hi
        if( prod_counter ~= index )
          constant_part = constant_part / ...
            ( self.coordinates(index) - self.coordinates(prod_counter) );
        end
      end
      dependent_part = 0;
      % calc dependent part
      for sum_counter=1:hi
        if( sum_counter ~= index )
          temp = 1;
          for prod_counter=1:hi
            if( ( prod_counter ~= sum_counter ) && ( prod_counter ~=index ) )
              temp = temp * (xi - self.coordinates(prod_counter));
            end
          end
          dependent_part = dependent_part + temp;
        end
      end
      derivative = dependent_part * constant_part;
    end
    
    function plot(self)
      width = 0.01;
      x_vector=self.coordinates(1):width:self.coordinates(end);
      y_values=ones((self.order+1),length(x_vector));
            
      for xi=1:length(x_vector)
        for curve_counter=1:(self.order+1)
          y_values(curve_counter,xi)=self.get(x_vector(xi), curve_counter);               
        end
      end

      Knoten=1;
      color_vector = ['r'; 'g'; 'b'; 'y'; 'c'; 'm'; 'k'];
      for curve=1:self.order+1
        if curve<=7         
          plot(x_vector,y_values(curve,:), color_vector(curve))
          hold on
        else
          plot(x_vector,y_values(curve,:), color_vector(1))
          hold on
        end
                
        X_Koordinate=self.coordinates(curve);
        Y_Koordinate=self.get(self.coordinates(curve),curve);
                
        strX = num2str(X_Koordinate, 3);
        strY = num2str(Y_Koordinate, 3);
        
        text(X_Koordinate,Y_Koordinate,['\leftarrow N: '...
          num2str(Knoten, 15) ' ' strX '/' strY ],...
          'HorizontalAlignment','left')
            
        Knoten=Knoten+1;       
      end
      hold off
    end
  end
end