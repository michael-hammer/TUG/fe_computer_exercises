% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef TriangleShape < model.shape.Shape

  methods
    function self = TriangleShape( order )
      self@model.shape.Shape( 2, order )
    end
    
    function value = get( self, coord , node_index )
      K = self.order - (node_index(1)-1 + node_index(2)-1) + 1;
      value = self.lagrange.get( coord(1) , node_index(1), node_index(1) )*...
      self.lagrange.get( coord(2), node_index(2), node_index(2) ) *...
      self.lagrange.get( 1-coord(1)-coord(2) , K, K );
    end
    
    function derivative = der( self, coord , node_index, derivative_index )
      K = self.order - (node_index(1)-1 + node_index(2)-1) + 1;
      switch derivative_index
        case 1
          derivative_index = 1;
          non_derivative_index = 2;
        case 2
          derivative_index = 2;
          non_derivative_index = 1;
      end
      derivative = self.lagrange.get( coord(non_derivative_index) , node_index(non_derivative_index) , node_index(non_derivative_index) ) * ...
        self.lagrange.der( coord(derivative_index) , node_index(derivative_index) , node_index(derivative_index) )*...
        self.lagrange.get( 1-coord(1)-coord(2) , K , K ) - ...
        self.lagrange.get( coord(non_derivative_index) , node_index(non_derivative_index) , node_index(non_derivative_index) ) * ...
        self.lagrange.get( coord(derivative_index) , node_index(derivative_index) , node_index(derivative_index) ) * ...
        self.lagrange.der( 1-coord(1)-coord(2) , K , K );
    end
    
    function node_index = getNodeIndex( self, local_number )
      switch( local_number )
        case 1
          node_index = [1,1];
        case 2
          node_index = [self.order+1,1];
        case 3
          node_index = [1,self.order+1];
        case 4
          node_index = [2,1];
        case 5
          node_index = [2,2];
        case 6
          node_index = [1,2];
      end
    end
    
    function node_nr_array = getBoundary( self )
      switch( self.order )
        case 1
          node_nr_array = [1,2,3];
        case 2
          node_nr_array = [1,4,2,5,3,6];
      end
    end
  end
  
  methods( Access = protected )
    function coordinates = calcLagrangeCoordinates( self )
       coordinates = linspace( 0 , 1 , self.order+1 );
    end
    
    function node_amount = calcNodeAmount( self )
       node_amount = (self.order+1)*(self.order+2)/2;
    end
  end
end
