% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef Shape < handle
  
  properties( SetAccess = protected )
    coordinates;
    dimension;
    order;
    lagrange;
    node_amount;
  end

  methods
    function self = Shape( dimension, order )
      self.dimension = dimension;
      self.order = order;
      self.lagrange = model.shape.Lagrange( self.calcLagrangeCoordinates );
      self.node_amount = self.calcNodeAmount();
    end
    
    function t = getTensor( self, coord )
      t = tensor(self.getMatrix(coord));
    end

    function t = getMatrix( self, coord )
      t = zeros(self.node_amount,1);
      for i=1:self.node_amount
        t(i) = self.get( coord , self.getNodeIndex(i) );
      end
    end
    
    function t = getDerivativeTensor( self, coord )
      t = tensor(self.getDerivativeMatrix(coord));
    end
    
    function t = getDerivativeMatrix( self, coord )
      t = zeros(self.dimension,self.node_amount);
      for dim_counter=1:self.dimension
        for i=1:self.node_amount
          t(dim_counter,i) = self.der( coord , self.getNodeIndex(i) , dim_counter );
        end
      end
    end
    
  end
  
  methods( Abstract = true )
    value = get( self, coord , node_index )
    derivative = der( self, coord , node_index, derivative )
    node_index = getNodeIndex( self, local_number )
    node_nr_array = getBoundary( self )
  end

  methods( Abstract = true, Access = protected )
    coordinates = calcLagrangeCoordinates( self )
    node_amount = calcNodeAmount( self )
  end
  
end
