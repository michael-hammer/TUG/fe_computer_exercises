% ------------------------------------------------------------------
% This file is part of SOOFEAM -
%         Software for Object Oriented Finite Element Analysis in Matlab.
%
% SOOFEAM is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% SOOFEAM is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with SOOFEAM.  If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------

classdef Node < model.NumberedObject

  properties( SetAccess = protected )
    lagrangian_coords;
    eularian_coords;
    dof_cell = {};
    load_cell = {};
  end

  methods
    function self = Node( number , lagrangian_coords )
      self@model.NumberedObject(number);
      
      self.lagrangian_coords = lagrangian_coords;
      self.eularian_coords = lagrangian_coords; % that's at the beginning without deformation

      self.addDisplacementDOF();
    end
       
    function addDisplacementDOF( self )
        global GlobalData;
        self.dof_cell{1} = model.dof.DisplacementDOF( GlobalData.dimension );
    end
    
    function disp_dof = getDisplacementDOF( self )
        disp_dof = self.dof_cell{ 1 };
    end
    
    function disp_dof_vector = getDisplacementDOFVector( self )
        global GlobalData;
        disp_dof_vector = zeros(GlobalData.dimension,1);
        for i=1:GlobalData.dimension
            disp_dof_vector(i) = self.getDisplacementDOF().getValue(i);
        end
    end
    
    function setForceLoad( self, force_load )
        self.load_cell{1} = model.load.ForceLoad(force_load);
    end

    function force_load = getForceLoad( self )
        if( size(self.load_cell , 2 ) >= 1 )
            force_load = self.load_cell{1};
        else
            force_load = [];
        end
    end

    function updatePoint( self, displacement, coord )
      self.getDisplacementDOF().setValue(coord,displacement);
      self.eularian_coords(coord) = self.eularian_coords(coord) + displacement;
    end
    
    function disp( self )
      global GlobalData;
      switch( GlobalData.dimension )
        case 2
          disp(['Node ',num2str(self.number),...
            ': x=',num2str(self.lagrangian_coords(1)),...
            ' (u=',num2str(self.getDisplacementDOF().getValue(1)),...
            ') y=',num2str(self.lagrangian_coords(2)),...
            ' (v=',num2str(self.getDisplacementDOF().getValue(2)),')']);
        case 3
          disp(['Node ',num2str(self.number),...
            ': x=',num2str(self.lagrangian_coords(1)),...
            ' (u=',num2str(self.getDisplacementDOF().getValue(1)),...
            ') y=',num2str(self.lagrangian_coords(2)),...
            ' (v=',num2str(self.getDisplacementDOF().getValue(2)),...
            ') z=',num2str(self.lagrangian_coords(3)),...
            ' (w=',num2str(self.getDisplacementDOF().getValue(3)),')']);
      end
    end
  end

end
