function soofea_post( disp_model )

config.scaling = 1000;
config.show.node_number = true;
config.show.deformed = true;
config.show.undeformed = true;

close;

dim = disp_model.global_data.dimension;
lagrangian = zeros( disp_model.nodeAmount() , dim );
scaled_eulerian = zeros( disp_model.nodeAmount() , dim );
for node_counter = 1:disp_model.nodeAmount()
  lagrangian(node_counter,:) = disp_model.getNode(node_counter).lagrangian_coords;
  if( config.show.deformed )
    scaled_eulerian(node_counter,:) = disp_model.getNode(node_counter).lagrangian_coords + ...
      (disp_model.getNode(node_counter).getDisplacementDOFVector() * config.scaling)';
  end
end

figure(1);
hold on;
axis equal;
axes_handle = gca;

for element_counter = 1:disp_model.elementAmount
  disp_model.getElement(element_counter).vis(axes_handle,config);
end

%for node_counter = 1:disp_model.nodeAmount()
%  scatter(lagrangian(node_counter,1),lagrangian(node_counter,2),'xb');
%end

if( config.show.node_number )
  for node_counter = 1:disp_model.nodeAmount()
    if( ~ config.show.deformed )
 %       scatter(lagrangian(node_counter,1),lagrangian(node_counter,2),'r');   
        text('Position',[lagrangian(node_counter,:),0],'String',num2str(node_counter));
    else
 %       scatter(scaled_eulerian(node_counter,1),scaled_eulerian(node_counter,2),'r');   
        text('Position',[scaled_eulerian(node_counter,:),0],'String',num2str(node_counter));
    end
  end
end

end

