bottom_cube_size = 100.;
div = 3;

//------------------------------------------------------------------

Point(1) = {0, 0, 0, 1.0};
Point(2) = {bottom_cube_size, 0, 0, 1.0};
Line(1) = {1, 2};
Transfinite Line {1} = div+1;
bottom_surface[] = Extrude {0, 100, 0} {
  Line{1}; Layers{div}; Recombine;
};
bottom_volume[] = Extrude {0, 0, 100} {
  Surface{bottom_surface[1]}; Layers{div}; Recombine;
};

Physical Surface("bottom_mesh") = {bottom_surface[1],bottom_volume[0],bottom_volume[2],bottom_volume[3],bottom_volume[4],bottom_volume[5]};
Physical Volume("bottom_mesh") = {bottom_volume[1]};
