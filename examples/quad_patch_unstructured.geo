length = 100.;
mesh_div = 4;

Point(1) = {0, 0, 0, length/mesh_div};
Point(2) = {length, 0, 0, length/mesh_div};
Point(3) = {length, length, 0, length/mesh_div};
Point(4) = {0, length, 0, length/mesh_div};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(6) = {1, 2, 3, 4};
Plane Surface(7) = {6};

Mesh.RecombineAll = 1;
Mesh.SubdivisionAlgorithm = 1;
