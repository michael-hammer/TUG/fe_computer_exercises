import numpy as np
import os

from soofea.io.input_handler import GmshInputHandler
from soofea.model.type import ElementType
from soofea.model.type import FaceType
from soofea.model import Model
from soofea.model.material import StVenantKirchhoffMaterial
from soofea.analyzer.implementation import ElementImpl
from soofea.analyzer.implementation import BoundaryImpl

#------------------------------------------------------------------
def read():
	mesh_file_name = 'brick_patch.msh'
	dimension = 3

	mesh_file = os.path.join( os.path.split(os.path.realpath(__file__))[0], mesh_file_name )
	print 'Loading input file <'+ os.path.abspath(mesh_file) +'> ...'
	input_handler = GmshInputHandler( mesh_file , dimension );
	model = Model( dimension )

	model.addType( ElementType(N = 1, shape_order = 1, shape_type = 'hex', number_of_int_points=[2,2,2] ) )
	model.getType(1).implementation = ElementImpl()
	model.addType( FaceType(N = 3, shape_order = 1, shape_type = 'quad', number_of_int_points=[2,2] ) )
	model.getType(3).implementation = BoundaryImpl()
	model.addMaterial( StVenantKirchhoffMaterial(N = 1, E = 2.1e5 , nu = 0.3 ) )
	input_handler.readMesh( model )

	# incorporate BCs
	for node in model.getBoundary( 5 ).getNodes():
		node.setBCDOF( z=0. )
	# top nodes
	#model.getNode(5).setBCDOF( x=0., y=0. )
	#model.getNode(8).setBCDOF( x=0. )
	# bottom nodes
	model.getNode(1).setBCDOF( x=0., y=0. )
	model.getNode(2).setBCDOF( y=0. )

	# ------------------------------------------------------------------
	# displacement controlled
	#for node in model.getBoundary( 27 ).getNodes():
	#	node.setBCDOF( x=-1., y=-1 )

	# ------------------------------------------------------------------
	# surface load on top
	# for face in model.getBoundary( 27 ).getTargetList():
	#     face.setPressure( 100. )
	# for node in model.getBoundary( 27 ).getNodes():
	#   node.setSurfaceForce( [0,0,-100.] )

	# ------------------------------------------------------------------
	# body force
	for node in model.getNodeIterator():
		node.setBodyForce( np.array([0,0,-10.]) )
	#for element in model.getElementIterator():
	#	element.setBodyForce( np.array([0,0,-10.]) )

	return( model )
