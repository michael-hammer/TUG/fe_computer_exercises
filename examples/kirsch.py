import os

from soofea.io.input_handler import GmshInputHandler
from soofea.model.type import ElementType
from soofea.model.type import EdgeType
from soofea.model import Model
from soofea.model.material import StVenantKirchhoffMaterial
from soofea.analyzer.implementation import ElementImpl
from soofea.analyzer.implementation import BoundaryImpl

#------------------------------------------------------------------
def read():
	mesh_file_name = 'kirsch.msh'
	dimension = 2

	mesh_file = os.path.join( os.path.split(os.path.realpath(__file__))[0], mesh_file_name )
	print 'Loading input file <'+ os.path.abspath(mesh_file) +'> ...'
	input_handler = GmshInputHandler( mesh_file , dimension );
	model = Model( dimension )

	model.addType( ElementType(N = 1, shape_order = 1, shape_type = 'quad', number_of_int_points=[2,2] ) )
	model.getType(1).height = 1
	model.getType(1).implementation = ElementImpl()
	model.addType( EdgeType(N = 2, shape_order = 1, number_of_int_points=[2] ) )
	model.getType(2).height = 1
	model.getType(2).implementation = BoundaryImpl()
	model.addMaterial( StVenantKirchhoffMaterial(N = 1, E = 100 , nu = 0. , twodim_type = 'plane_stress' ) )
	input_handler.readMesh( model )

	# incorporate BCs
	for node in model.getBoundary( 1 ).getNodes():
		node.setBCDOF( y=0. )
	for node in model.getBoundary( 4 ).getNodes():
		node.setBCDOF( x=0. )

	for edge in model.getBoundary( 3 ).getTargetList():
		edge.setPressure( 1.  )

	return( model )
