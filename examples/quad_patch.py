import os
import numpy as np

from soofea.io.input_handler import GmshInputHandler
from soofea.model.type import ElementType
from soofea.model import Model
from soofea.model.material import StVenantKirchhoffMaterial
from soofea.analyzer.implementation import ElementImpl

#------------------------------------------------------------------
def read():
	mesh_file_name = 'quad_patch.msh'
	dimension = 2

	mesh_width = 20
	pressure = 100

	mesh_file = os.path.join( os.path.split(os.path.realpath(__file__))[0], mesh_file_name )
	print 'Loading input file <'+ os.path.abspath(mesh_file) +'> ...'
	input_handler = GmshInputHandler( mesh_file , dimension );
	model = Model( dimension )

	model.addType( ElementType(N = 1, shape_order = 1, shape_type = 'quad', number_of_int_points=[2,2] ) )
	model.getType(1).height = 1
	model.getType(1).implementation = ElementImpl()
	model.addMaterial( StVenantKirchhoffMaterial(N = 1, E = 2.1e5 , nu = 0.3 , twodim_type = 'plane_stress' ) )
	input_handler.readMesh( model )

	# incorporate BCs
	model.getNode( 1 ).setBCDOF( x=0. )
	for node in model.getBoundary( 1 ).getNodes():
		node.setBCDOF( y=0. )
	for node in model.getBoundary( 2 ).getNodes():
		node.setBCLoad( y=-2*pressure*mesh_width )
	# ATTENTION: work conjugated loads!
	model.getNode(3).setBCLoad( y=-pressure*mesh_width )
	model.getNode(4).setBCLoad( y=-pressure*mesh_width )

	return( model )
