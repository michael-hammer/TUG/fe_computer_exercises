cl1 = 30;
cl2 = 0.5;

Point(1) = {0, 0, 0, cl1};
Point(2) = {5, 0, 0, cl2};
Point(3) = {0, 5, 0, cl2};
Point(4) = {100, 0, 0, cl1};
Point(5) = {100, 100, 0, cl1};
Point(6) = {0, 100, 0, cl1};
Line(1) = {2, 4};
Line(2) = {4, 5};
Line(3) = {5, 6};
Line(4) = {6, 3};
Circle(5) = {3, 1, 2};
Line Loop(6) = {3, 4, 5, 1, 2};
Plane Surface(7) = {6};

Physical Line("mesh") = {1,2,3,4};
Physical Surface("mesh") = {7};

Mesh.RecombineAll = 1;
Mesh.SubdivisionAlgorithm = 1;
