#! /usr/bin/env python
""" ------------------------------------------------------------------
This file is part of SOOFEA Python.

SOOFEA - Software for Object Oriented Finite Element Analysis
Copyright (C) 2012 Michael Hammer

SOOFEA is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
------------------------------------------------------------------ """

import sys, os, imp
if sys.version_info[1] >= 7:
	from argparse import ArgumentParser
else:
	from optparse import OptionParser
src_path = str(os.path.join( os.path.abspath(sys.path[0]), "src" ))
if src_path not in sys.path:
	sys.path.append( src_path )

from soofea.io.output_handler import VTKOutputHandler
from soofea.analyzer.analysis import LinearAnalysis

#------------------------------------------------------------------
def main():
	if sys.version_info[1] >= 7:
		parser = ArgumentParser(description="SOOFEA - Software for Object Oriented Finite Element Analysis")
		parser.add_argument('-i','--input_file', help="Open SOOFEA python INPUT_FILE as mesh input file", metavar="INPUT_FILE")
		parser.add_argument('-o','--output_file', help="Output VTK OUTPUT_FILE to write results", metavar="OUTPUT_FILE")
		args = parser.parse_args()
		input_file_name = args.input_file
		output_file_name = args.output_file
	else:
		parser = OptionParser(description="SOOFEA - Software for Object Oriented Finite Element Analysis")
		parser.add_option('-i','--input_file', help="Open SOOFEA python INPUT_FILE as mesh input file", metavar="INPUT_FILE")
		parser.add_option('-o','--output_file', help="Output VTK OUTPUT_FILE to write results", metavar="OUTPUT_FILE")
		options, args = parser.parse_args()
		input_file_name = options.input_file
		output_file_name = options.output_file

	sys.path.append( os.path.split(input_file_name)[0] )
	input_script_name = os.path.splitext( os.path.basename(input_file_name) )[0]
	fp, pathname, description = imp.find_module(input_script_name)
	module = imp.load_module( input_script_name, fp, pathname, description )

	print 'Creating Model ...'
	model = module.read()
	output_handler = VTKOutputHandler( output_file_name , model.dimension )

	print 'Start Analysis ...'
	analysis = LinearAnalysis( model )
	analysis.run()
	print '... done'

	output_handler.write( model )

#------------------------------------------------------------------
if __name__ == "__main__":
	main()
