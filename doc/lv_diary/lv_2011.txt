#------------------------------------------------------------------
22.03.2011: Einführung in Matlab

- Einführung in MATLAB
- OO Prinzipien (Konstruktor, einfache Methode, Student.m)
- leider keine Ableitungen

#------------------------------------------------------------------
29.03.2011: Interpolation

- Was ist ein handle? (sollte eigentlich noch in erste LV)
- Interpolation Theorie
- Implementierung beginnen -> mercurial repository erklären
- set_env.m
- model.shape.Lagrange Klasse implementiert incl. get() und der() aber OHNE "high_index" (wir machen keine Dreiecke mehr)!

#------------------------------------------------------------------
05.04.2011: Formfunktionen

- Abstrakte Shape Klasse geschrieben
- QuadShape Klasse
- getNodeIndex() : erklärt was bei uns der NodeIndex ist
- get() Matrix
- der() Matrix NICHT MEHR GESCHAFFT!!!!

#------------------------------------------------------------------
12.04.2011: Numerische Integration

- Numerische Integration (Theorie)
- +model/IntegrationPoint.m implement
- +math/NumInt.m (NumIntIO download, was heißt "static")

Download: etc/soofea/*
          +math/NumIntIO.m

>> num_int_file = 'etc/soofea/soofea_num_int.xml'
>> math.NumIntIO.getInstance( num_int_file );
>> int_points = math.NumIntIO.getInstance().getIntegrationPointsWithPoints('rectangular', 'gauss_legendre', 2 );
>> myfun = @(x) (1+3*x.^2+2*x.^3+2.5*x.^4)
>> quad( myfun, -1, 1)
>> math.NumInt.integrateMatrix( @(element,int_point)intFunction(element,int_point) , 0 , int_points )

Von wo kommt dieser Unterschied?

#------------------------------------------------------------------
10.05.2011: Finite Elemente Methode (im Hörsaal C)

- Theorie

#------------------------------------------------------------------
17.05.2011: SOOFEA Software Architektur

- Model as Container (download)
  * zeige Aufbau des Models
  * Model (download)
  * NumberedObject (download)
  * Material, StVenantKirchhoffMaterial (download)

- Struktur eine Elementes
  * Implementiere Element
  * ElementType (download)
  * using ElementType id to create IntPoints
  * Implementiere Node (without dof and load)

- disp( self )
  * Model (already due to download)
  * Element
  * Node

#------------------------------------------------------------------
24.05.2011: IO & ModelProgression

- IO
  * InputHandler, XMLInputHandler (download +io/*)
  * -> erkläre PREProcessing
  * modifiziere soofem.m
  * erzeuge Model und teste disp routinen

- DOF, Loads
  * Was sind DOFs, Loads
  * Zusammenhanng mit Unbekannten in der Analyse
  * einlesen der *Prescribed* in ModelProgression 
    (download PrescribedDOF.m, PrescribedLoad.m, NodePrescribedDOF.m, NodePrescribedLoad.m, ModelProgression.m)

#------------------------------------------------------------------
31.05.2011: DOF,Load

- model.load
- model.dof
 * Node::(get|addDisplacementDof, setExtForce , disp DOF )

- BCHandler (integrate DOF, Load)

==================================================================
=> Hier wäre die Theorie wohl am Besten aufgehoben!
==================================================================

#------------------------------------------------------------------
07.06.2011: Analysis, Element Stiffness

- Analysis
 * Analysis Frame
 * Analysis::run() -> LinearAnalysis
 * Analysis::assembleK (base structure - bis element_impl.calcElementStiffness( element ))
 * Analysis::assembleFExt (base structure)

 * Element::getNodeMatrix()
 * Shape::getDerivativeMatrix() (had to implement Shape::der() & QuadShape::der() first - was left from LV 3.0)
 * LagrangianJacobian (calc,getInv,getDet)

#------------------------------------------------------------------
21.06.2011: Element Stiffness, Assemble -> FIRST SOLUTION!

- ElementImpl (add implementation in ElementType)
- calcElementStiffness in QuadImpl
- assembleK (there is a mistake in the preparation - sadly - reexplain next time)
- assembleFExt
- solve System!

#------------------------------------------------------------------
28.06.2011: updateDOF, visualization, Examples

- strip analyze.Analysis.assembleK
- implement analyze.Analysis.integrateDirichletBC()
- correct analyze.Analysis.integrateNeumannBC()

=> TEST

- model.Node.updatePoint()
- analyze.Analysis.updateDOF()

=> Test

- POSTProcessing (soofea_post.m download)
- model.Element.vis()
- model.shape.QuadShape.getBoundary()

=> Visualize!
