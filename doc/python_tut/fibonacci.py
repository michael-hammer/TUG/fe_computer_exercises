class NumberCalculator:
	def __init__(self, n):
		self._n = n

	def fib(self):
		a, b = 0, 1
		while b < self._n:
			print b
			a, b = b, a+b

	def factorial(self):
		f = 1
		for x in range(1,self._n+1):
			f *= x
			print f
