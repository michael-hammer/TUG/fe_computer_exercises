\chapter{\python - An Introduction}
\label{chap:python}

{\it \python is a general-purpose, high-level programming language
  whose design philosophy emphasizes code readability. \python claims
  to combine ``remarkable power with very clear syntax'', and its
  standard library is large and comprehensive.} [Wikipedia]

\vspace{1cm}
To understand the philosophy of \python let us cite Tim Peters by
typing \code{import this} on a python shell
\vspace{1cm}

\begin{verbatim}
In [1]: import this
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
\end{verbatim} 
\vspace{1cm}

At the moment of writing this \python is number 8 at the TIOBE
programming language popularity index and is therefore a very popular
language behind the big three C, C++ and Java and the two big
commercially supported languages Objective-C and C\#.

\section{Getting the Software}

Beside the \python environment itself we will use \gmsh as mesher
and \paraview for visualization tasks.

\subsection{Running Linux}

If you have a Linux installation (e.g. Debian/Ubuntu) and if one would
like to use (and one should want to) Emacs as editor type the
following into a terminal

\begin{verbatim}
$ apt-get install emacs python python-numpy python-scipy\
python-vtk python-matplotlib gmsh paraview
\end{verbatim}

The packages should be named similar in other distributions. Always
use the package manager to install the software!

\subsection{Running Windows}

On Windows you can use the following links to download the software:

\begin{itemize}
\item \python distribution for Windows
  \url{http://enthought.com/products/epd.php}, including all needed
  \python libraries, a usable shell and IPython.
\item Gmsh \url{http://www.geuz.org/gmsh/}
\item Paraview \url{http://www.paraview.org/paraview/resources/software.php}
\end{itemize}

You will need an editor which is appropriate for \python
coding. Especially for the right indentation it is important to have
an editor which is supportive on that.

\begin{itemize}
\item Of course one can use Emacs on Windows too \url{http://ftp.gnu.org/pub/gnu/emacs/windows/}
\item Notepad++ can be used and found at \url{http://notepad-plus-plus.org/download/v6.1.1.html}
\end{itemize}

\subsection{Documentation}

\begin{itemize}
\item The official tutorial for \python
  \url{http://docs.python.org/tutorial/index.html} is really excellent
  to get started. The author suggests to use IPython as shell from the
  very first beginning (This is not part of the tutorial as IPython
  is third-party software).
\item Of course there also exists a tutorial for NumPy, which can be
  found at \url{http://www.scipy.org/Tentative_NumPy_Tutorial}. NumPy
  was not intended to replace \matlab but over the time the feature
  set grew. That is why
  \url{http://www.scipy.org/NumPy_for_Matlab_Users} describes the
  differences and how to migrate from \matlab to NumPy.
\item For scientific computing, visualization is an important task. To
  create plots in \python matplotlib
  \url{http://matplotlib.sourceforge.net/users/index.html} can be
  used.
\end{itemize}

\subsection{A Note on IPython}

If you use IPython as development environment it is advantageous if
the modules (see Section~\ref{sec:modules}) are reloaded whenever
source files are modified. This is a non trivial task and it might
happen (but in rather rare situations) that reloading does not work.

It is important that already existing objects are not replaced. If you
edit a class file you have to reconstruct all objects of this type
manually.

To get \code{autoreload} working IPython delivers an extension which
has to be loaded and activated. If you have an IPython version $<0.11$
do the following

\begin{lstlisting}
In [ ]: import ipy_autoreload
In [ ]: %autoreload ? # To get some documentation on the extension
In [ ]: %autoreload 2
\end{lstlisting}

else you can use the new magic functions for
loading/unloading/reloading extension

\begin{lstlisting}
In [ ]: %load_ext autoreload
In [ ]: %autoreload 2
\end{lstlisting}

Of course one can configure IPython to do all these things on
start-up. Simply ask google for an appropriate solution for your
situation.

\section{\python Tutorial}

\begin{itemize}
\item Defining and using variables:
  %
  \begin{lstlisting}
In [ ]: width = 20
In [ ]: height = 5*9
In [ ]: width * height
  \end{lstlisting}
  %
\item Assignment of values and some operations on variables. One
  should always be aware of the type.
  %
  \begin{lstlisting}
In [ ]: x = y = z = 0  # Zero x, y and z
In [ ]: 3 * 3.75 / 1.5
In [ ]: 3 / 2 # attention - types!
  \end{lstlisting}
  %
\item How to define strings
  % 
  \begin{lstlisting}
In [ ]: 'spam eggs'
In [ ]: 'doesn\'t'
In [ ]: "doesn't"
In [ ]: '"Yes," he said.'
In [ ]: word = 'Help' + 'A' # Strings can be concatenated
In [ ]: word[4]
In [ ]: word[0:2]
In [ ]: word[:2]
In [ ]: word[2:]
  \end{lstlisting}
\item One of the most important types in \python - lists
  % 
  \begin{lstlisting}
In [ ]: a = ['spam', 'eggs', 100, 1234]
In [ ]: a[3]
In [ ]: a[-2]
In [ ]: a[1:-1]
In [ ]: a[:2] + ['bacon', 2*2]
In [ ]: 3*a[:3] + ['Boo!']
In [ ]: a[0:2] = [1, 12]
In [ ]: a[0:2] = []
In [ ]: a[1:1] = ['bletch', 'xyzzy']
In [ ]: a[:0] = a
In [ ]: a[:] = [] # (clear list) or a = []
In [ ]: a
  \end{lstlisting}
\item How to determine the length, do concatenating. Also nesting of
  lists in lists is possible
  % 
  \begin{lstlisting}
In [ ]: q = [2, 3]
In [ ]: p = [1, q, 4]
In [ ]: len(p)
In [ ]: p[1].append('xtra')
  \end{lstlisting}
\item Are there any control flow tools?
  % 
  \begin{lstlisting}
In [ ]: a, b = 0, 1
In [ ]: while b < 10:
   ...:     print b
   ...:     a, b = b, a+b
  \end{lstlisting}
  % 
  {\bf \python uses indentation to identify blocks of code!} Either
  use 4 spaces or one tab - one should prefer the spaces version if
  the editor supports it. The comparison operators in \python are
  \code{<,>,==,<=,>=,!=}.
\end{itemize}

Obviously we can use \python in an interactive way. However, this is
not suitable for the implementation of bigger programs. So how to
write source code files which can be executed multiple times?

Create a file \code{fibonacci.py} and insert the \code{while ...}
example from above. Run IPython in the directory where you stored the
source file and enter

\begin{lstlisting}
In [ ]: %run fibonacci.py
\end{lstlisting}

This is equivalent to enter in the command line

\begin{verbatim}
$ python fibonacci.py
\end{verbatim}

\python takes the file and executes it. This means python interprets
all the statements in the file. The next step will be to collect code
statements into functions. Modify \code{fibonacci.py} to contain

\begin{lstlisting}
def fib(n):
	a, b = 0, 1
	while b < n:
		print b
		a, b = b, a+b
\end{lstlisting}

Now, if you now run the file nothing will happen. The code is
encapsulated in a function and we first have to run the function
itself. So the question is - How do we get the function \code{fib(n)}
into the interpreters scope to call it? This is where the built in
function \code{import} gets important which is explained in the next
section.

\subsection{Modules and Packages (\python Namespaces)}
\label{sec:modules}

A {\bf module} is a file containing Python definitions and
statements. The file name is the module name with the suffix
\code{.py} appended. We can load a module with \code{import}

\begin{lstlisting}
In [ ]: import fibonacci
In [ ]: fibonacci.fib(20)
\end{lstlisting}

The functions of a module always live in their namespace. Therefore it
is important for the \python interpreter to find the module.

\begin{lstlisting}
In [ ]: import sys
In [ ]: print sys.path  
\end{lstlisting}

The variable \code{sys.path} contains a list of all directories which
are searched for possible module paths. The current working directory
is always part of the path. With \linebreak
\code{sys.path.append(\$path)} one can append any arbitrary path to be
searched for modules.

It is also possible to collect multiple modules in a {\bf package}. A
package is a directory which contains a file
\code{\_\_init\_\_.py}. Inside such a directory multiple modules might
exist. 

To access the modules inside packages or functions (variables) inside
modules we use the dot \code{`.`}. As example the directory structure
of \soofea is given in the following

\begin{verbatim}
soofea/
|-- __init__.py # <- This makes a directory a package container
|-- analyzer/
|   |-- analysis.py
|   |-- implementation.py
|   |-- __init__.py # <- This makes a directory a package container
|   \-- jacobian.py
|-- base.py
|-- io/
|   |-- __init__.py # <- This makes a directory a package container
|   |-- input_handler.py
|   \-- output_handler.py
|-- math/
|   |-- __init__.py # <- This makes a directory a package container
|   |-- math.py
|   |-- num_int.py
\-- model/
    |-- dof.py
    |-- __init__.py # <- This makes a directory a package container
    |-- load.py
    |-- material.py
    |-- model.py
    |-- shape.py
    \-- type.py
\end{verbatim}

Here we have a main package \code{soofea}. Inside this package there
are four subpackages, \code{analyzer}, \code{io}, \code{math} and
\code{model}. If one would like to access the \code{shape.py} module
one would have to write \code{soofea.model.shape}.

\subsection{Running Module as Script}

There is one question left. What happens if we run our modified
\code{fibonacci.py} module file?

\begin{verbatim}
$ python fibonacci.py
\end{verbatim}

No output is generated which is no surprise. We have defined a function
\code{fib()} inside the module \code{fibonacci.py}. The interpreter
reads the module file as script and loads the function
\code{fib()}. But we do not have any statement that we want to start
the function \code{fib()}. \python provides an easy solution for
this. If a module is started as a script the property
\code{\_\_name\_\_} contains the string \code{"\_\_main\_\_"}. We can
use this with

\begin{lstlisting}
if __name__ == '__main__':
    fib( 5 )
\end{lstlisting}

This is a very common code snippet for python module files. Often the
function which is called inside this if block is called
\code{main()}. Normally it has no parameters.

\subsection{Classes}

As \python is an object oriented language (but also support other
software paradigms like functional programming) we can define
classes. Let us consider the following code snippet

\begin{lstlisting}
class NumberCalculator:
	def __init__(self, n):
		self._n = n

	def fib(self):
		a, b = 0, 1
		while b < self._n:
			print b
			a, b = b, a+b

	def factorial(self):
		f = 1
		for x in range(1,self._n+1):
			f *= x
			print f  
\end{lstlisting}

This is an extension of the \code{fibonacci.py} module. We
encapsulated the function \code{fib()} in the class
\code{NumberCalculator}. So the class definition keyword is
\code{class}. Once again the block of code defining the class is given
by indentation. The method \code{\_\_init\_\_} is the constructor of
the class. The first argument of each method inside the class is the
object itself. We call it \code{self} (which is a suitable
convention). For further notes on classes visit the tutorial
\url{http://docs.python.org/tutorial/classes.html}.

\section{And Now?}

In this short introduction we only gave a starting point to get
running with the tool chain and give a short idea what \python is
about. To get more familiar with this language we suggest to have a
look at the following tutorials:

\begin{itemize}
\item The official \python tutorial can be found at
  \url{http://docs.python.org/tutorial/index.html}. A lot of snippets
  in this introduction are lent from there.
\item We will use the NumPy \python package for all the linear algebra
  we are doing. This can be seen (but is not intended to be) as a
  replacement for \matlab. Start with the tutorial
  \url{http://www.scipy.org/Tentative_NumPy_Tutorial}. If you are
  already familiar with \matlab the tutorial
  \url{http://www.scipy.org/NumPy_for_Matlab_Users} might be
  interesting too.
\item To create plots we will use matplotlib. You can find a good
  beginners tutorial at
  \url{http://matplotlib.sourceforge.net/users/index.html}. Once again
  one can find a lot of similarities to \matlab. Nevertheless the
  package is still pythonic.
\end{itemize}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
