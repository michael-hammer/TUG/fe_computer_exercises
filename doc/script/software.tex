\chapter{Software Framework}
\label{chap:software}

In this chapter the software design is presented. The code developed
during the first two chapters already follows these ideas and can be
integrated unchanged.

\section{Code Structure and Design Rules}
\label{sec:design_rules}

We will use (like already done) object oriented software
paradigms. Therefore we will group the code in modules and
classes. Although some terms and parts of the concept are \python
related it could be applied to other languages as well; e.g. modules
in \python could be namespaces in C++.

The structure of the main project directory is shown in
Figure~\ref{fig:project_dir}.

\begin{figure}[!ht]
  \centering \scriptsize
  \begin{minipage}{0.80\linewidth}
\begin{verbatim}
|-- src/
|   \-- soofea/
|-- examples/
\-- pySoofea.py
\end{verbatim}    
  \end{minipage}
  \caption{Project directory structure}
  \label{fig:project_dir}
\end{figure}

Inside the examples directory the different input files can be
collected. This will be discussed further in
Section~\ref{sec:input_handling}. The content of the \code{src/soofea}
directory (containing all the \python sources) is shown in
Figure~\ref{fig:src_dir}.

\begin{figure}[!ht]
  \centering \scriptsize
  \begin{minipage}{0.80\linewidth}
\begin{verbatim}
soofea/
|-- analyzer/
|   |-- analysis.py
|   |-- implementation.py
|   |-- __init__.py
|   \-- jacobian.py
|-- base.py
|-- __init__.py
|-- io/
|   |-- __init__.py
|   |-- input_handler.py
|   \-- output_handler.py
|-- math/
|   |-- __init__.py
|   |-- math.py
|   |-- num_int.py
\-- model/
    |-- dof.py
    |-- __init__.py
    |-- load.py
    |-- material.py
    |-- model.py
    |-- shape.py
    \-- type.py
\end{verbatim}
  \end{minipage}
  \caption{Source directory structure}
  \label{fig:src_dir}
\end{figure}

\subsection{Naming Standard}
\label{sec:naming_standard}

This is a very comprehensive and incomplete collection of rules for
the naming (and coding) standard used in this code project. To see a
list of some common coding standards in use by different projects
visit \citet{wikiProgrammierstil}.

\begin{itemize}
\item Module directories are named by lower case letters. As word
  delimiter we use {\tt `\_`}, e.g. \code{sample\_module}.
\item Multiple classes and functions might be collected in a module
  file. The naming standard is the same as for module directories.
\item Classes are written in CamelCase starting with an upper case
  letter (first letter of a word written in upper case, else lower
  case, no white spaces), e.g. \code{IAmAClass}
\item Methods are written in CamelCase starting with a lower case
  letter, e.g. \code{iAmAMethod}
\item Variables are written in lower case letters and words are
  seperated by underscores {\tt `\_`} to separate words,
  e.g. \code{i\_am\_a\_variable}. In \python a special treatment of
  private variables is not necessary. It can be seen as good practice
  to prefix a member method or variable with one \code{`\_`} if it
  should be seen as excluded from the public interface. (we will not
  use spam name mangling \code{`\_\_`})
\end{itemize}

\section{The Model}
\label{sec:model}

All classes related to the model (those defining the mesh, like
\code{Node}, \code{Edge}, \code{Element}, \code{Model}) are stored in
\code{soofea/model/model.py}. They are imported in
\code{soofea/model/\_\_init\_\_.py} to bring them into the namespace
of the \code{soofea.model} module. In the following the abbreviation
\code{s.m} stands for the \code{soofea.model} module and \code{s.a}
for the \code{soofea.analyzer} module. One of the main data classes is
the class \code{s.m.Model}. It contains all the mesh data. An
example for a mesh is sketched in Figure~\ref{fig:mesh}. It contains
the main items and defines the terms we will use in the following.

\begin{figure}[!ht]
  \hspace{2.5cm} \input{src_images/mesh.pdf_tex}
  \caption{Example of a quadrilateral mesh containing the main terms
    of a finite element mesh.}
  \label{fig:mesh}
\end{figure}

 A mesh consists of \code{Node}s (see
Section~\ref{sec:s.m.Node} for implementation details) with
each \code{Node} having its unique global node number. A unique global
number is not only defined for \code{Node}s bot for every object in
the mesh. These globally numbered objects have therefore the base
class \code{NumberedObject}. A \code{Node} also stores its degrees of
freedom \code{s.m.dof.DOF}
(Section~\ref{sec:s.m.dof.DOF}) and loads
\code{s.m.load.Load} (Section~\ref{sec:load}). The degrees of
freedom might be free or constraint (locked) due to Dirichlet boundary
conditions. The loads on each node might be zero or given through a
Neumann boundary condition.

One patch of \code{Node}s is collected to an \code{Element}
(Section~\ref{sec:node_container}). The \code{Shape} of this
\code{Element} is given by the \code{ElementType}. Multiple
\code{Element}s can have one \code{ElementType}. For instance all
quadrilateraly shaped \code{Element}s with the same number of
\code{IntegrationPoint}s, the same height aso. refer to one and the
same \code{ElementType}. It is often the case that for the whole mesh
only one \code{ElementType} exists. The property of collecting
\code{Node}s and having a \code{Shape} function is also true for
\code{Edge}s or \code{Face}s. That is why we collect this properties
and interfaces into the base class \code{NodeContainer}. In the
\code{NodeContainer} the corresponding \code{Node}s have also a local
node numbering as it is required to construct the \code{Shape}
functions (see Section~\ref{sec:shape_functions}). \code{Edge}s or
\code{Face}s have a further specialty. They are
\code{BoundaryComponent}s. This is getting import when it comes to the
incorporation of the boundary conditions.

We have to evaluate an integral over the \code{Element} volume (see
Eq.~\eqref{eq:element_stiffness}), the \code{Face} surface or the
\code{Edge} length. So we need \code{IntegrationPoint}s which are part
of the \linebreak \code{NodeContainer} too. In
Section~\ref{sec:impl_num_int} we already mentioned that we will
differ between mathematical integration points (with natural
coordinates) and model integration points. The first are aggregated by
the \code{Type}s and the latter by the \code{NodeContainer}.

This verbal design description is illustrated as UML diagram in
Figure~\ref{fig:model_design}.

\begin{figure}[!ht]
  \centering
  \includegraphics{images/uml}
  \caption{UML design diagram of the \soofea model. This diagram
    represents the main classes and their relations for a two
    dimensional mesh.}
  \label{fig:model_design}
\end{figure}

The \code{s.m.Model} aggregates all mesh objects, it is
therefore responsible for holding and maintaining the data. The
\code{s.m.Element}, as a subclass of \code{NodeContainer},
holds its \code{s.m.Node} objects,
\code{s.m.type.ElementType}, \linebreak
\code{s.m.material.StVenantKirchhoffMaterial} objects
aso.. This means it would be sufficient to let only the
\code{s.m.Element} aggregate those object. But to realize the
possibility to iterate over all nodes of a model, the
\code{s.m.Model} is the main aggregator. The main objects
held by \code{s.m.Model} are of type \linebreak
\code{s.m.Element}, \code{s.m.Edge} (or
\code{s.m.Face} in the case of a three dimensional analysis),
\code{s.m.Node}, \code{s.m.ElementType}, \linebreak
\code{s.m.EdgeType} (or \code{s.m.FaceType} in the
case of a three dimensional analysis) and
\code{s.m.Material}.

\subsection{\code{soofea.base.NumberedObject}}
\label{sec:soofea.base.NumberedObject}

All of the classes mentioned above are subclasses of
\code{s.m.NumberedObject} (signature is given in
Figure~\ref{fig:sig:model.NumberedObject}). This class adds a property
\code{number} which represents a unique number for each object of this
type in the model. Each node, element aso. has its unique ``global''
number. It is important for the assembling algorithm that the nodes
have strictly increasing global numbers. This is not necessary for the
other objects.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=8cm]{NumberedObject}{0,0}
      \attribute{number}
      \operation{NumberedObject( self, number )}
    \end{class}
  \end{tikzpicture}
  \caption{Signature of \code{model.NumberedObject}}
  \label{fig:sig:model.NumberedObject}
\end{figure}

\subsection{\code{soofea.model.Node}}
\label{sec:s.m.Node}

We defined the points where the interpolation conditions are fulfilled
as nodes (see Section~\ref{chap:interpolation}). The node was the
start point for doing the finite element discretization in
Section~\ref{sec:discretization}. This means a node holds at least the
nodal displacement values which are used for interpolation. This will
lead us to the definition of degrees of freedom
\code{s.m.dof.DOF} in Section~\ref{sec:s.m.dof.DOF}.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=6cm]{NumberedObject}{0,0}
    \end{class}
    \begin{class}[text width=12cm]{Node}{0,-2}
      \inherit{NumberedObject}
      \attribute{lagrangian\_coordinates \iref{impl:s.m.Node.lagrange_coordinates}}
      \attribute{dof \iref{impl:s.m.Node.dof}}
      \attribute{load \iref{impl:s.m.Node.load}}
      \attribute{f\_S \iref{impl:s.m.Node.force_densities}}
      \attribute{f\_B \iref{impl:s.m.Node.force_densities}}
      \operation{Node( self, number , lagrangian\_coordinates ) \iref{impl:s.m.Node}}
      \operation{setBCDOF(self, x=None, y=None, z=None) \iref{impl:s.m.Node.bc}}
      \operation{setBCLoad(self, x=None, y=None, z=None) \iref{impl:s.m.Node.bc}}
      \operation{setBodyForce( self, load ) \iref{impl:s.m.Node.force_densities}}
      \operation{setSurfaceForce( self, load ) \iref{impl:s.m.Node.force_densities}}
    \end{class}
  \end{tikzpicture}
  \caption{Signature of \code{s.m.Node}}
  \label{fig:sig:s.m.Node}
\end{figure}

\begin{impl}[\code{s.m.Node}]
  \label{impl:s.m.Node}
  The constructor needs the unique global node \code{number} and the
  \code{lagrangian\_coordinates} as defined in the input file.
\end{impl}

\begin{impl}[\code{lagrangian\_coordinates}]
  \label{impl:s.m.Node.lagrange_coordinates}
  A node has a position in global coordinates. We have to distinguish
  between the position in initial configuration, the
  \code{lagrangian\_coordinates}, and a position in the current
  configuration. The difference between the two positions is stored
  inside the displacement degree of freedom (see
  Section~\ref{sec:s.m.dof.DOF}).
\end{impl}

\begin{impl}[\code{dof}]
  \label{impl:s.m.Node.dof}
  The degrees of freedom are implemented in a special class (see
  Section~\ref{sec:s.m.dof.DOF}). The \code{dof} attribute
  stores a \code{s.m.DisplacementDOF} object. If one would
  combine a structural analysis with a thermical analysis one would
  have to store a \code{s.m.TemperatureDOF} in addition and
  extend the \code{dof} attribute to a \code{dofs} array.
\end{impl}

\begin{impl}[\code{load}]
  \label{impl:s.m.Node.load}
  \code{s.m.load.ForceLoad} (see Section~\ref{sec:load}) is
  the kinetic counterpart of \linebreak
  \code{s.m.dof.DisplacementDOF}. In \code{load}, the
  \code{s.m.load.ForceLoad} object is stored. If no nodal
  force is defined it is set to a zero array.
\end{impl}

\begin{impl}[Set boundary conditions]
  \label{impl:s.m.Node.bc}
  With the two methods \code{setBCDOF} and \code{setBCLoad} the
  boundary conditions are incorporated into the model. One can set the
  conditions on each node in each coordinate direction separately.
\end{impl}

\begin{impl}[Set force densities]
  \label{impl:s.m.Node.force_densities}
  If we want to define either surface loads $\bff^S$ or body forces
  $\bff^B$ it might be needed to define discrete values in the
  nodes. These values are then interpolated over the boundary or the
  element to get the load at a certain integration point. Then, we are
  able to calculate the work conjugated nodal forces.
\end{impl}

\subsection{\code{soofea.model.type.Type}}
\label{sec:type}

Each \code{Element} (but also \code{Edge} or \code{Face}) has its own
type, the \code{ElementType}, \code{EdgeType}, or \code{FaceType}
respectively. A type collects all properties, which are unique to
multiple elements, edges or faces of a mesh. The signature of
\code{s.m.type.Type} is given in
Figure~\ref{fig:sig:s.m.type.Type}. It further holds a
\code{s.a.implementation} which provides the interface to
calculate the stiffness and load arrays for our system of finite
element equations.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=6cm]{NumberedObject}{0,0}
    \end{class}
    \begin{class}[text width=14cm]{Type}{0,-2}
      \inherit{NumberedObject}
      \attribute{\_math\_ips \iref{impl:s.m.type.Type._math_ips}}
      \attribute{shape \iref{impl:s.m.type.Type.shape}}
      \attribute{implementation \iref{impl:s.m.type.Type.implementation}}
      \operation{Type( self, number, shape\_order, shape\_type, number\_of\_int\_points ) \iref{impl:s.m.type.Type}}
    \end{class}
  \end{tikzpicture}
  \caption{Signature of \code{s.m.type.Type}}
  \label{fig:sig:s.m.type.Type}
\end{figure}

\begin{impl}[\code{s.m.type.Type}]
  \label{impl:s.m.type.Type}
  The constructor needs a unique global node \code{number} for the type
  to be created. Per default we use the global number
  \begin{itemize}
  \item '1' for \code{ElementType},
  \item '2' for \code{EdgeType} and 
  \item '3' for \code{FaceType}.
  \end{itemize}
  The \code{shape\_order} and \code{shape\_type} are used to construct
  the actual shape
  (Implementation~remark~\iref{impl:s.m.type.Type.shape}). Finally
  we need the \code{number\_of\_int\_points} array which stores the
  number of Gaussian quadrature points in each direction of the shape
  dimension
  (Implementation~remark~\iref{impl:s.m.type.Type._math_ips}).
\end{impl}

\begin{impl}[\code{\_math\_ips}]
  \label{impl:s.m.type.Type._math_ips}
  This property holds the mathematical integration points -
  \code{soofea.math.IntegrationPoint}. It is constructed out of the
  \code{int\_point\_amounts} which is an array. For the 2D case this
  array holds \code{int\_point\_amounts}~$=[m,n]$ from
  Eq.~\eqref{eq:num_int_2D}. For further notes on numerical
  integration see Chapter~\ref{chap:num_int}.
\end{impl}

\begin{impl}[\code{shape}]
  \label{impl:s.m.type.Type.shape}
  Holds an object of \code{model.shape.Shape}. See
  Section~\ref{sec:impl_shape} for further details on this class.
\end{impl}

\begin{impl}[\code{implementation}]
  \label{impl:s.m.type.Type.implementation}
  Holds the actual implementation, which provides the calculation of
  the stiffness and load arrays. It is an object of class
  \code{analyzer.implementation.ElementImpl}, \code{BoundaryImpl}
  or similar. As explained in
  Section~\ref{sec:s.a.implementation.Impl}, this is the
  interface between model and analysis.
\end{impl}

\subsection{\code{soofea.model.NodeContainer} (Element, Edge, Face)}
\label{sec:node_container}

An \code{Element}, \code{Edge}, \code{Face} is a collection (a
geometrical patch) gathering a certain connected amount of nodes. As
these classes have similar properties and interfaces they are derived
from the base class \code{s.m.NodeContainer}.

There is a speciality for the \code{Edge} and \code{Face} class as
they are both components which may lie on the boundary. Therefore they
are derived from the base class \linebreak
\code{s.m.BoundaryComponent}, which can hold \code{pressure}
and \code{surface\_load\_density} values. In order to distribute
pressure loads, the classes \code{s.m.Edge} and \linebreak
\code{s.m.Face} must be able to provide a normal vector by
the method \linebreak \code{getNormal(self, int\_point)}.

The \code{NodeContainer} class holds an appropriate \code{Type} and
therefore, holds the \code{Shape} and the implementation
(\code{Impl}). In addition an \code{Element} also contains a
\code{s.m.material} object which is needed to calculate the
elasticity matrid $\mathbb{C}$.

The signature of the \code{NodeContainer} class and its subclasses is
given in Figure~\ref{fig:sig:s.m.NodeContainer}.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=5cm]{NumberedObject}{0,0}
    \end{class}
    \begin{class}[text width=11cm]{NodeContainer}{0,-2}
      \inherit{NumberedObject}
      \attribute{\_node\_number\_list \iref{impl:s.m.NodeContainer.node_list}}
      \attribute{\_node\_list \iref{impl:s.m.NodeContainer.node_list}}
      \attribute{type}
      \attribute{int\_points}
      \operation{NodeContainer(self, number, node\_number\_list) \iref{impl:s.m.NodeContainer}}
      \operation{setNodes(self, node\_list) \iref{impl:s.m.NodeContainer.node_list}}
      \operation{setNode(self, local\_node\_number) \iref{impl:s.m.NodeContainer.node_list}}
      \operation{getNodes(self) \iref{impl:s.m.NodeContainer.node_list}}
      \operation{getCoordinateArray(self) \iref{impl:s.m.NodeContainer.node_values}}
      \operation{getDisplacementArray(self) \iref{impl:s.m.NodeContainer.node_values}}
      \operation{setType(self, the\_type)}
    \end{class}
    \begin{class}[text width=7cm]{Element}{-4,-9.5}
      \inherit{NodeContainer}
      \attribute{material}
      \attribute{f\_B \iref{impl:s.m.Element.body_force}}
      \operation{setMaterial(self, material)}
      \operation{setBodyForce(self, load) \iref{impl:s.m.Element.body_force}}
      \operation{getLoad(self, int\_point) \iref{impl:s.m.Element.body_force}}
    \end{class}
    \begin{class}[text width=7cm]{BoundaryComponent}{4,-9.5}
      \inherit{NodeContainer}
      \attribute{p}
      \attribute{f\_S}
      \operation{setPressure(self, p) \iref{impl:s.m.Edge.surface_load}}
      \operation{setSurfaceLoad(self, load) \iref{impl:s.m.Edge.surface_load}}
      \operation{getLoad(self, int\_point)}
    \end{class}
    \begin{class}[text width=7cm]{Edge}{4,-14}
      \inherit{BoundaryComponent}
      \operation{getNormal( self, int\_point ) \iref{impl:s.m.getNormal}}
    \end{class}
    \begin{class}[text width=7cm]{Face}{-4,-14}
      \inherit{BoundaryComponent}
      \operation{getNormal( self, int\_point ) \iref{impl:s.m.getNormal}}
    \end{class}
  \end{tikzpicture}
  \caption{Signature of \code{s.m.NodeContainer} and its subclasses}
  \label{fig:sig:s.m.NodeContainer}
\end{figure}

\begin{impl}[\code{s.m.NodeContainer}]
  \label{impl:s.m.NodeContainer}
  The constructor needs a unique \code{number} and the list of node
  numbers. This last has to be resolved to real node references inside
  \code{s.m.Model.resolveNodes()} (see
  Implementation~remark~\iref{impl:s.m.NodeContainer.node_list}).
\end{impl}

\begin{impl}[Node referencing]
  \label{impl:s.m.NodeContainer.node_list}
  Inside a \code{NodeContainer} the list of node numbers and a list
  containing the node references is stored. This is of course
  redundant. But the problem is, that at the moment of construction
  (during reading the input file) the node references do not
  exist. Therefore we first provide the node number list and then
  resolve this list to real node references
  (\code{s.m.Model.resolveNodes()}). It is important to note
  that the nodes inside the \code{\_node\_list} are sorted regarding
  to their local node numbers.
\end{impl}

\begin{impl}[Getting the nodal coordinates]
  \label{impl:s.m.NodeContainer.node_values}
  To interpolate the geometry one needs the array of nodal
  coordinates, see Eq.~\eqref{eq:isogeometric_interpolation}. This
  array $\node{\bfx}^{(m)}$ is returned by
  \code{getCoordinateArray()}. To interpolate the displacement
  (e.g. for calculating strains or stresses) we need the displacement
  array, see Eq.~(\ref{eq:disp_interpolation}). This array
  $\node{\bfu}^{(m)}$ is provided by the method
  \code{getDisplacementArray()}.
\end{impl}

\begin{impl}[Set and get body forces of an \code{Element}]
  \label{impl:s.m.Element.body_force}
  There are two possibilities. Either the body force is constant over
  the whole \code{element} or it is given by discrete values inside
  the \code{Node}s (see implementation remark
  \ref{impl:s.m.Node.force_densities}). The method
  \code{getLoad(self, int\_point)} should return the load density
  vector for a given integration point \code{int\_point}. This means
  we have to handle the two different cases inside this method.
\end{impl}

\begin{impl}[Set and get surface loads on \code{BoundaryComponent}]
  \label{impl:s.m.Edge.surface_load}
  For a \code{BoundaryComponent} it is possible to define a
  \code{pressure} or surface load \code{f\_S}. This means we have 3
  possiblities to define a surface load, either for each node, as
  pressure or as vectorial surface load. Inside the method
  \code{getLoad(self, int\_point)} we have to handle these cases and
  return the appropriate load vector at an integration point. This
  means we have to use the normal vector from \code{getNormal()}
  (Implementation~remark~\iref{impl:s.m.getNormal}) for a
  pressure load or interpolate the nodal force densities over the
  boundary.
\end{impl}

\begin{impl}[\code{getNormal()}]
  \label{impl:s.m.getNormal}
  If a \code{pressure} is defined for a \code{BoundaryComponent} we
  need the normal vector to distribute the pressure to a surface load
  (see Figure~\ref{fig:work_conj_forces}). In the case of an
  \code{Edge}, we construct the normal vector via the cross product of
  the tangential vector with the third elementary vector $\bfe_3$. We
  have to be very careful that the node numbering inside the edge is
  correct so that the normal vector points outwards the material! In
  the case of a \code{Face}, we can construct the normal vector with
  the cross product of the two tangential vectors. See
  Section~\ref{sec:surface_loads} for further treatment.
\end{impl}

\subsection{\code{soofea.model.dof.DOF} (Degrees of Freedom)}
\label{sec:s.m.dof.DOF}

As shown in Figure~\ref{fig:mesh} each node holds its degrees of
freedom (\code{DOF}). In case of structural analysis this \code{DOF}
is a \code{DisplacementDOF}. If one solves the heat conduction problem
the \code{DOF} might be a \code{TemperatureDOF}. As we are going to
solve the structural problem only, we limit the implementation to
\code{s.m.dof.DisplacementDOF}.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=4cm]{DOF}{0,0}
    \end{class}
    \begin{class}[text width=12cm]{DisplacementDOF}{0,-2}
      \inherit{DOF}
      \attribute{displacement \iref{impl:model.dof.DisplacementDOF}}
      \attribute{constraint  \iref{impl:model.dof.DisplacementDOF}}
      \operation{DisplacementDOF(self, dimension)}
      \operation{getValue(self, coord\_id) \iref{impl:model.dof.DisplacementDOF}}
      \operation{setValue(self, coord\_id, value) \iref{impl:model.dof.DisplacementDOF}}
      \operation{setConstraintValue(self, coord\_id, value) \iref{impl:model.dof.DisplacementDOF}}
    \end{class}
  \end{tikzpicture}
  \caption{Signature of \code{s.m.dof.DOF} and its subclass
    \newline \code{s.m.dof.DisplacementDOF}}
  \label{fig:sig:s.m.dof.DOF}
\end{figure}

\begin{impl}
  \label{impl:model.dof.DisplacementDOF}
  The \code{displacement} property of the
  \code{s.m.dof.DisplacementDOF} is nothing else then the
  displacement vector per node. The \code{coord\_id} selects the
  coordinate direction after which the \code{displacement} vector is
  sorted.
  \begin{itemize}
  \item '0' - $x$-coord (or $u$ displacement)
  \item '1' - $y$-coord (or $v$ displacement)
  \item '2' - $z$-coord (or $w$ displacement)
  \end{itemize}

  We save in \code{constraint[coord\_id]} whether the
  \code{displacement[coord\_id]} is free, which means it is going to
  be an unknown in the analysis, or prescribed. This is a vector of
  Boolean variables, one for each coordinate direction. The value is
  true if the \code{DisplacementDOF} in this \code{coord\_id} is
  locked (constraint) by a boundary condition. We will use this
  information during integration of Dirichlet boundary conditions (see
  Implementation~remark~\iref{impl:s.a.analysis.Analysis.integrateDirichletBC}). For
  setting the \code{displacement} we have therefore two methods. One
  might be used to store the result of the analysis -
  \code{setValue()} - and one to set {\bf and} lock the value -
  \code{setConstraintValue()}. Each of them needs the \code{coord\_id}
  (position index in the \code{displacement} and \code{constraint}
  vector).
\end{impl}

\subsection{\code{soofea.model.load.Load}}
\label{sec:load}

\code{Load}s are the ``kinetic'' counterpart to the \code{DOF}s (of
Section~\ref{sec:s.m.dof.DOF}). In the case of structural
analysis the work conjugated load to the displacement is a
\code{s.m.load.ForceLoad}. The conjugated load to a
temperature would be a heat flux. You can see the signature in
Figure~\ref{fig:sig:s.m.load.Load}.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=4cm]{Load}{0,0}
    \end{class}
    \begin{class}[text width=10cm]{ForceLoad}{0,-2}
      \inherit{Load}
      \attribute{force}
      \operation{ForceLoad(self, dimension)}
      \operation{getValue(self)}
      \operation{setValue(self, value)}
    \end{class}
  \end{tikzpicture}
  \caption{Signature of \code{s.m.load.Load} and its subclass \newline \code{s.m.load.ForceLoad}}
  \label{fig:sig:s.m.load.Load}
\end{figure}

The implementation remark is similar to the one in
Section~\ref{sec:s.m.dof.DOF}. It is even easier as we do not
have to differ between constraint and non constraint \code{ForceLoad}
values. We simply set the force load to zero for the initial state. If
there is a non zero force load value we will incorporate it into the
system of equations (see
Implementation~remark~\iref{impl:s.a.analysis.Analysis.integrateNeumannBC}).

\subsection{\code{soofea.model.model.Material}}
\label{sec:material}

The main goal of the module \code{s.m.model} is to provide
the elasticity tensor - more general - the constitutive law (see also
Section~\ref{sec:strain_stress_voigt}).

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=6cm]{NumberedObject}{0,0}
    \end{class}
    \begin{class}[text width=8cm]{Material}{0,-2}
      \inherit{NumberedObject}
    \end{class}
    \begin{class}[text width=16cm]{StVenantKirchhoffMaterial}{0,-4}
      \inherit{Material}
      \attribute{\_E}
      \attribute{\_nu}
      \attribute{\_twodim\_type \iref{impl:s.m.material.StVenantKirchhoff.getElasticityMatrix}}
      \operation{StVenantKirchhoffMaterial(self, N, E, nu, twodim\_type='plane\_strain') \iref{impl:s.m.material.StVenantKirchhoffMaterial}}
      \operation{getElasticityMatrix(self)\iref{impl:s.m.material.StVenantKirchhoff.getElasticityMatrix}}
    \end{class}    
  \end{tikzpicture}
  \caption{Signature of \code{s.m.material.StVenantKirchhoffMaterial}}
  \label{fig:sig:model.Material}
\end{figure}

\begin{impl}[\code{s.m.material.StVenantKirchhoffMaterial}]
  \label{impl:s.m.material.StVenantKirchhoffMaterial}

  The constructor needs in addition to the unique global number the material parameters, namely

  \begin{itemize}
  \item youngs modulus (\code{E}),
  \item poisson ratio (\code{nu}) and
  \item \code{twodim\_type}, which should be a string containing either
    ``\code{plane\_strain}'' or \linebreak ``\code{plane\_stress}'' in
    the case of a two dimensional analysis.
  \end{itemize}
\end{impl}

\begin{impl}[\code{getElasticityMatrix()}]
  \label{impl:s.m.material.StVenantKirchhoff.getElasticityMatrix}
  Returns the matrix $\mathbb{C}$ given in Eq.~\eqref{eq:C_3D} in the
  case of a three dimensional volume element. If we have a two
  dimensional analysis one distinguishs based on \code{\_twodim\_type}
  between ``\code{plane\_strain}'', where we return the result of
  Eq.~\eqref{eq:C_plane_strain} and ``\code{plane\_stress}'', where we
  return Eq.~\eqref{eq:C_plane_stress}.
\end{impl}

\section{Preprocessing}
\label{sec:preprocessing}

It is a very important part of the finite element method to create the
mesh. This meshing procedure, where one splits the continuous domain
into elements and nodes, is a field of interest on its own. It is not
part of this lecture to explain different algorithms for
e.g. triangulation. There are various tools for mesh generation. For
our lecture we are using \gmsh (pronounced 'geemesh' -
\citet{gmsh}). This is a very sophisticated open source mesh generator.

\subsection{Input Handling}
\label{sec:input_handling}

There are of course multiple formats for input files. Nearly each code
basis uses its own input format. As we are using \gmsh as mesher, we
wrote a parser for the \gmsh ASCII file format. In addition to the
mesh file itself, we need information about the element types and
boundary conditions. For this we have to write a short input script
analogous to the following:

\begin{lstlisting}
import os
import numpy as np

from soofea.io.input_handler import GmshInputHandler
from soofea.model.type import EdgeType
from soofea.model.type import ElementType
from soofea.model import Model
from soofea.model.material import StVenantKirchhoffMaterial
from soofea.analyzer.implementation import BoundaryImpl
from soofea.analyzer.implementation import ElementImpl

def read():
	mesh_file_name = 'quad_patch_unstructured.msh'
	dimension = 2

	mesh_file = os.path.join( os.path.split(os.path.realpath(__file__))[0],\
                                  mesh_file_name )
	print 'Loading input file <'+ os.path.abspath(mesh_file) +'> ...'
	input_handler = GmshInputHandler( mesh_file , dimension );
	model = Model( dimension )

	model.addType( ElementType(N = 1, shape_order = 1, \
                       shape_type = 'quad', int_point_amounts=[2,2] ) )
	model.getType(1).height = 1
	model.getType(1).implementation = ElementImpl()
	model.addType( EdgeType(N = 2, shape_order = 1, int_point_amounts=[2] ) )
	model.getType(2).implementation = BoundaryImpl()
	model.addMaterial( StVenantKirchhoffMaterial(N = 1, youngs_modulus = 2.1e5,\
                                                     poisson_ratio = 0.3,\
                                                     twodim_type = 'plane_stress' ) )
	input_handler.readMesh( model )

	# incorporate BCs
	for node in model.getBoundary( 1 ).getNodes():
		node.setBCDOF( x=0., y=0. )
	# force controlled
	for node in model.getBoundary( 3 ).getNodes():
	 	node.setBCLoad( y=-100. )
	model.getNode( 3 ).setBCLoad(y=-50)
	model.getNode( 4 ).setBCLoad(y=-50)

	return( model )
\end{lstlisting}

The function \code{read()} has to return a valid
\code{s.m.Model} object for which the analysis is done. So
everything one needs to incorporate into the model has to be done
here.

We will not discuss the \code{soofea.io.InputHandler} in depth as this
is not related to the finite element method itself. Inside the
\code{io} module one might implement any kind of input file
format. The class \code{io.InputHandler} serves as base class. The
signature is given in Figure~\ref{fig:sig:io.InputHandler}. The method
\code{io.InputHandler.readMesh( model )} reads the input file and add
the entries into the model. This is more or less a dummy method, which
calls itself the \code{\_read()} method. This method is implemented in
the specialization, e.g. \code{io.InputHandler.GmshInputHandler}.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=6cm]{InputHandler}{0,0}
      \operation{readMesh( self, model )}
      \operation{$\cdots$}
    \end{class}
    \begin{class}[text width=6cm]{GmshInputHandler}{0,-3}
      \inherit{InputHandler}
      \operation{\_read( self, model )}
      \operation{$\cdots$}
    \end{class}    
  \end{tikzpicture}
  \caption{Signature of \code{io.InputHandler}}
  \label{fig:sig:io.InputHandler}
\end{figure}

\section{Postprocessing}
\label{sec:postprocessing}

Postprocessing is similar to preprocessing
(Section~\ref{sec:preprocessing}) a really challenging task which can
not be covered by this lecture. We are writing \vtk files
(\citet{vtk}) which can be used inside \paraview to be
visualized. \vtk as well as \paraview are powerful tools in the field
of visualization.

The code for writing the \vtk output is collected in the module
\code{soofea.io.output\_handler}.

\section{Examples}
\label{sec:software_examples}

\begin{ex}
  \label{ex:software_displacement_array}
  Implement the method
  \code{s.m.NodeContainer.getDisplacementArray(self)} which
  should return the displacement array $\bfu^{(m)}$ from
  Eq.~\eqref{eq:nodal_displacement_array}.
\end{ex}

\begin{ex}
  \label{ex:software_normal_vectors}
  Implement the methods
  \code{s.m.Edge.getNormal(self,int\_point)} and \linebreak
  \code{s.m.Face.getNormal(self,int\_point)} as preparation
  for pressure distributions on \code{BoundaryComponent}s. In
  Eq.~\eqref{eq:normal_2D} and Eq.~\eqref{eq:normal_3D} one can find
  the corresponding mathematical formulas. Write some unit tests to
  check that the methods are working correctly.
\end{ex}

\begin{ex}
  \label{ex:software_print_model}
  Implement the \code{\_\_str\_\_(self)} method for the main classes
  of the model. They should be called revursivly so that the
  \code{Model} prints the return of
  \code{Element.\_\_str\_\_()}. \code{Element.\_\_str\_\_()} again should
  call \code{Node.\_\_str\_\_()}. In the end all main properties of the
  model should be printed by a single \code{print model} statement
  like given in the following code snippet.
\begin{lstlisting}
model = Model( dimension )

# Here read the model from input, solve system and do all the interesting stuff

print model # The results should be printed
\end{lstlisting}
\end{ex}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
