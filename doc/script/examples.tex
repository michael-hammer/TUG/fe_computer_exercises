\chapter{Engineering Examples}
\label{chap:examples}

\begin{ex}[\ref{ex:fe_boundary_component_load},\ref{ex:fe_stress_projection}]
  Analyze the example from Figure~\ref{fig:kirsch_problem}, the so
  called ``Kirsch problem''. Take advantage of the symmetries.

  \begin{figure}[!ht]
    \centering
    %\includegraphics[width=0.5\linewidth]{images/kirsch}
    \input{src_images/kirsch.pdf_tex}
    \caption{Kirsch problem - An infinite plate with a circular
      hole. The height of plate $h$ is free to choosen. As the finite
      element mesh is finite, the radius $a$ of the whole should be
      small in comparison to the actual dimensions of the finite
      element mesh.}
    \label{fig:kirsch_problem}
  \end{figure}

  Plot the stresses along the $x$-axis and the $y$-axis for the finite
  element and the algebraic solution (see
  \citet{Girkmann1956}). Discuss the differences between the finite
  element solution (difference between the nodal stresses and the
  exact stresses at node points) and the exact algebraic solution. The
  algebraic solution is given with

  \begin{equation*}
    \begin{array}{lll@{}lllllllllll}
      \sigma_{rr}(r,\varphi) &=& \displaystyle\frac{p}{2}\left[1-\displaystyle\frac{a^2}{r^2}+\left(1-\displaystyle\frac{4a^2}{r^2}+
          \displaystyle\frac{3a^4}{r^4}\right)\cdot\cos(2\varphi)\right]\\[5mm]
      \sigma_{\varphi\varphi}(r,\varphi) &=& \displaystyle\frac{p}{2}\left[ 1 +
        \displaystyle\frac{a^2}{r^2}-\left(1+\displaystyle\frac{3a^4}{r^4}\right)\cdot\cos(2\varphi)\right]\\[5mm]
      \sigma_{r\varphi}(r,\varphi) &=& \displaystyle\frac{p}{2}\left(-1-\displaystyle\frac{2a^2}
        {r^2}+\displaystyle\frac{3a^4}{r^4}\right)\cdot\sin(2\varphi)\\[5mm]
    \end{array} 
  \end{equation*}

  Compare some different values for the radius $a$. How big is the
  influence of the finite element mesh being finite and not infinite?
\end{ex}
 
\begin{ex}[\ref{ex:fe_boundary_component_load},\ref{ex:fe_stress_projection}]
  Analyze the example illustrated in
  Figure~\ref{fig:thick_cyl_pressure}. Take advantage of symmetries.

  \begin{figure}[!ht]
    \centering
    \input{src_images/thick_cyl_pressure.pdf_tex}
    \caption{Thick cylinder under pressure, the given dimension and
      the height $h$ of the plate are free to choose.}
    \label{fig:thick_cyl_pressure}
  \end{figure}

  Plot the stresses along the radius. Discuss the differences between
  the finite element solution (difference between the nodal stresses
  and the exact stresses at node points) and the exact algebraic
  solution. The algebraic solution is given with

  \begin{align*}
    \sigma_{rr}(r) &= p \cdot \frac{r_i^2}{r_a^2-r_i^2} \cdot \left( 1 - \frac{r_a^2}{r^2} \right)\\
    \sigma_{\varphi\varphi}(r) &= p \cdot \frac{r_i^2}{r_a^2-r_i^2} \cdot \left( 1 + \frac{r_a^2}{r^2} \right)
  \end{align*}

  Compare some different values for $\frac{r_a}{r_i}$. What happens if
  $r_i$ gets zero and $r_i = r_a$ as limit state?
\end{ex}

\begin{ex}[\ref{ex:fe_boundary_component_load},\ref{ex:fe_stress_projection}]
  Analyze the example illustrated in
  Figure~\ref{fig:short_bending_plate}. Take advantage of the symmetry.

  \begin{figure}[H]
    \centering
    \input{src_images/short_bending.pdf_tex}
    \caption{A short plate under bending load. On top of the ``beam''
      the pressure $p(x)$ is applied. The plate is supported on the
      left $x=0$ and the right side $x=l$. This is not a local
      concentrated support in a point but distributed over the
      boundary. It is important, that the displacement and stress in
      $x$-direction is not constrained in the algebraic
      solution. Further no special stress distribution was prescribed
      to yield a solution of the Airy's stress function (see
      \citet{Girkmann1956}). Choose an appropriate support boundary
      condition.}
    \label{fig:short_bending_plate}
  \end{figure}

  The short plate is loaded by

  \begin{equation}
    p(x) = p_n \cdot \sin \left( \alpha_n x \right) \quad, \qquad \alpha_n = \frac{n \pi}{l}
  \end{equation}

  With the help of Airy's stress function $F$ it is possible to
  calculate the stress distribution analytically (see
  \citet{Girkmann1956}).

  \begin{align}
      \sigma_{xx}(x,y) = & [(A_n+2B_n)\cdot \cosh(\alpha_n\cdot y)+\alpha_n\cdot y\cdot B_n\cdot\sinh(\alpha_n\cdot y)+\\
      &(C_n+2D_n)\cdot \sinh(\alpha_n\cdot y)+\alpha_n\cdot y\cdot D_n\cdot\cosh(\alpha_n\cdot y)]\cdot \sin(\alpha_n \cdot x)\\
      \sigma_{xy}(x,y) = &-[(A_n+B_n)\cdot \sinh(\alpha_n\cdot y)+\alpha_n\cdot y\cdot B_n\cdot\cosh(\alpha_n\cdot y)+\\
      &(C_n+D_n)\cdot \cosh(\alpha_n\cdot y)+\alpha_n\cdot y\cdot D_n\cdot\sinh(\alpha_n\cdot y)]\cdot \cos(\alpha_n \cdot x)
  \end{align}
  % 
  \begin{align}
    A_n=&0 &  B_n=&p_n/h \cdot \left[ \alpha_n c\cdot\sinh(\alpha_n c) \right] / \left[ \cdot \sinh^2(\alpha_n c)-\alpha_n^2 c^2 \right] \\
    C_n=&-D_n &  D_n=&- p_n/h \cdot \left[ \sinh(\alpha_n c)+\alpha_n c\cdot\cosh(\alpha_n c) \right]/\left[\cdot \sinh^2(\alpha_n c)-\alpha_n^2 c^2 \right]
  \end{align}

  Compare the stresses (create a plot of the stress distribution)
  results along the symmetry line and the boundaries. Discuss the
  differences between the finite element solution (difference between
  the nodal stresses and the exact stresses at node points) and the
  exact algebraic solution. Further compare this stress distributions
  with the classical Navier solution for the bending stress and shear
  stress due to shear forces (at the boundaries)

  \begin{align}
    \sigma_{xx}(x_{,1},y_{,2}) &= - \frac{M_{bz_{,2}}(x_{,1})}{I_{z_{,2}z_{,2}}} \cdot y_{,2} \\
    \tau_{xy}(x_{,1},y_{,2})   &=   \frac{3}{2} \frac{Q_y(x_{,1})}{c\,h} \cdot \left[ 1 - \left( \frac{2 \, y_{,2}}{c} \right)^2 \right]
  \end{align}

  Do the analysis for different ratios $\frac{l}{c} = 1$, $\frac{l}{c}
  = 2$ and $\frac{l}{c} = 3$.
\end{ex}

\begin{ex}
  Calculate the example shown in Figure~\ref{fig:rotating_ring}. Take
  advantage of the symmetries.

  \begin{figure}[!ht]
    \centering
    \includegraphics{images/rotating_ring}
    \caption{A massive ring rotating around its own axis}
    \label{fig:rotating_ring}
  \end{figure}

  Create an input file and discuss the results. Analyze the error of
  the stress distribution calculated by the finite element method in
  comparison to the exact solution. (Missing parameters are free to
  choose) The analytical solution is given for with

  \begin{align*}
    \sigma_{rr}(r) &= \displaystyle\frac{\varrho\omega^2}{8}\left(3+\nu\right)\left(r_a^2+r_i^2-r^2-\displaystyle\frac{r_a^2r_i^2}{r^2}\right)\\
    \sigma_{\varphi\varphi}(r) &= \displaystyle\frac{\varrho\omega^2}{8}\left[\left(3+\nu\right)\left(r_a^2+r_i^2\right)-\left(1+3\nu\right)r^2+\left(3+\nu\right)\displaystyle\frac{r^2_ar_i^2}{r^2}\right]
  \end{align*}
\end{ex}

\begin{ex}
  Calculate the ``Cook's membrane'' shown in
  Figure~\ref{fig:cooks_membrane} (a very common benchmark example in
  the field of finite elements). Create an input file and use the code
  from Example~\ref{ex:fe_equivalent_stress} to do a stress and strain
  analysis.

  \begin{figure}[!ht]
    \centering
    \includegraphics[width=0.7\linewidth]{images/cooks_membrane}
    \caption{Cook's membrane}
    \label{fig:cooks_membrane}
  \end{figure}
\end{ex}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
