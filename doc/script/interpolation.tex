\chapter{Interpolation}
\label{chap:interpolation}

Interpolation is a method to create continuous functions which fit a
given set of data points. At least this is the way we are going to
apply and therefore can understand the term ``interpolation''. For a
general introduction into interpolation we would like to refer to
\citet{Schwarz2006}.

Given a sequence of $(n+1)$ distinct points $r_i$ called nodes and for
each $r_i$ a value $\beta_i$, we are looking for a function $f_n$
so that

\begin{equation}
  f_n(r_i) = \beta_i \quad\mbox{for}\quad i=0,\ldots,n  
\end{equation}

We assume a function

\begin{equation}
  f_n(r) = \sum_{k=0}^{n} a_k \, \varphi_k(r)
\end{equation}

where $\varphi_k(x)$ are linear independent basis functions and $a_k$
are unknown weighting values. The function $f_n(r)$ fulfills the
interpolation equations in the nodes

\begin{equation}
  \label{eq:interpol_cond}
  f_n(r_i) = \beta_i = \sum_{k=0}^{n} a_k \varphi_k(r_i) \quad\mbox{for}\quad i=0,\ldots,n
\end{equation}

With the help of \eqref{eq:interpol_cond} we are able to calculate the
weights $a_k$. For the special case of 

\begin{equation}
  \label{eq:interpol_one}
  \varphi_k(r_i) = \delta_{ki} = \left\{ \begin{aligned}
      1, &\quad \text{if} \quad k=i \\
      0, &\quad \text{if} \quad k \neq i
    \end{aligned} \right. 
\end{equation}

the evaluation is very simple, $a_k = \beta_k$. In other words: If the
value of the base function is 1 in its own node and zero in all the
other nodes the weighting values $a_k$ are identically with the
interpolation values $\beta_k$. This situation is visualized for
linear basis functions in Figure~\ref{fig:interpolation}.

\begin{figure}[!ht]
  \centering
  % \includegraphics{images/interpol_cond}
  \input{src_images/interpolation.pdf_tex}
  \caption{We want to interpolate the function $f(r)$. Therefore we
    have to know the function values $f(r_k)$ at the nodal points
    $r_k$. For each of the nodes $k$ a corresponding interpolation
    basis $\varphi_k$ is given. For our special case of using Lagrange
    polynomials these functions are $1$ in the corresponding node $k$ and
    $0$ on all others. This means that the weights $a_k$ are for our
    polynomials rather easy to determine as the nodal function values
    $f(r_k)=\beta_k=a_k$.}
  \label{fig:interpolation}
\end{figure}

\section{Lagrange Polynomials}
\label{sec:lagr_poly}

One possible choice for such basis functions can be polynomials. We
are focusing here on special polynomial ansatz functions. The Lagrange
polynomials, named after Joseph-Louis Lagrange, fulfill the condition
in \eqref{eq:interpol_one} and are easy to construct.

\begin{eqnarray}
  l_k^n(r) &=& \prod\limits_{\genfrac{}{}{0pt}{}{i=0}{i \ne k}}^{n} \frac{r - r_i}{r_k - r_i} \label{eq:lagrange_poly} \\
  l_k^n(r) &=& \frac{(r - r_0)(r - r_1)\cdots(r - r_{k-1})(r - r_{k+1})\cdots(r - r_n)}{(r_k - r_0)(r_k - r_1)\cdots(r_k - r_{k-1})(r_k - r_{k+1})\cdots(r_k - r_n)}
\end{eqnarray}

The value $r_k$ is the coordinate of the node $k$. The values $r_i$
are the position values of the other nodes i, respectively,
$i \ne k$. $l_k^n(r)$ is now the Lagrange polynomial of the node $k$
with the order of $n$ dependent on $r$. For $n=1$ (linear case) they
look like ``hat'' functions shown in Figure~\ref{fig:interpolation}.

For our special purpose of application in the finite element method it
is sufficient to define equidistant nodal positions. This means we
split the interval $[a,b]$ ($b > a$) into arbitrary equidistant
points $r_i$ so that $r_0 = a$ and $r_n = b$.

\begin{equation}
  r_i = a + \frac{(b-a)}{n} \, i
\end{equation}

We can further assume that the interval used is $[-1,1]$ (or rarely
$[0,1]$). Let us have a brief look on a Lagrange polynomial of second
order with equidistant nodes in the interval $[-1,1]$.

\begin{align*}
  l_0^2 &= \frac{r-r_1}{r_0-r_1} \cdot \frac{r-r_2}{r_0-r_2} \\
  l_1^2 &= \frac{r-r_0}{r_1-r_0} \cdot \frac{r-r_2}{r_1-r_2} \\
  l_2^2 &= \frac{r-r_0}{r_2-r_0} \cdot \frac{r-r_1}{r_2-r_1}
\end{align*}

On this example it is easy to see the property of
\eqref{eq:interpol_one}. A visualization of this second order Lagrange
polynomial is given in Figure~\ref{fig:lagr_second_order}.

\begin{figure}[!ht]
  \centering
  \input{src_images/lagr_second_order.pdf_tex}
  \caption{The three second order Lagrange polynomials are shown. They
    are $1$ in the corresponding node and $0$ in all others.}
  \label{fig:lagr_second_order}
\end{figure}

\subsection{First Derivative}

For the finite element method at least the first derivation of the
basis functions are needed. To develop a general formula for
arbitrary order we once again have a look on Lagrange polynomials of
second order.

\begin{align*}
  \partiel{l_0^2(r)}{r} &= \frac{1}{r_0-r_1} \cdot \frac{r-r_2}{r_0-r_2} + \frac{r-r_1}{r_0-r_1} \cdot \frac{1}{r_0-r_2} \\
  \partiel{l_1^2(r)}{r} &= \frac{1}{r_1-r_0} \cdot \frac{r-r_2}{r_1-r_2} + \frac{r-r_0}{r_1-r_0} \cdot \frac{1}{r_1-r_2} \\
  \partiel{l_2^2(r)}{r} &= \frac{1}{r_2-r_0} \cdot \frac{r-r_1}{r_2-r_1} + \frac{r-r_0}{r_2-r_0} \cdot \frac{1}{r_2-r_1}
\end{align*}

We can see that due to the product rule of derivation we get a
summation over a reduced product. This can be written for arbitrary
order as

\begin{equation}
  \label{eq:lagr_poly_first_der}
  \frac{\partial l_k^n(r)}{\partial r} = \left[ \prod\limits_{\genfrac{}{}{0pt}{}{i=0}{i \ne k}}^n \frac{1}{(r_k - r_i)} \right] \cdot 
  \left[ \sum\limits_{\genfrac{}{}{0pt}{}{i=0}{i \ne k}}^n \prod\limits_{\genfrac{}{}{0pt}{}{j=0}{j \ne k , j \ne i}}^n (r-r_j) \right]
\end{equation}

These first derivatives are shown in
Figure~\ref{fig:lagr_second_order_deriv} for quadratic Lagrange
polynomials.

\begin{figure}[!ht]
  \centering
  \input{src_images/lagr_second_order_derivative.pdf_tex}
  \caption{First derivative of the Lagrange polynomials}
  \label{fig:lagr_second_order_deriv}
\end{figure}

\section{Multidimensional Interpolation - Shape Functions}
\label{sec:shape_functions}

For the finite element method shape functions are necessary. A one
dimensional Lagrange polynomial might already be seen as a suitable
shape function for truss elements but not for plane 2D or volume 3D
elements. For further hints on construction of shape functions see
\citet{ZienTayl2000} or \citet{CelFE1998}.

We can construct multidimensional shape functions in a very simple way
by applying an ``outer tensor product'' to our Lagrange
polynomials. This procedure can be used to construct different types
of shape functions. The shape function constructed in this way can be
used to realize finite elements of the so called Lagrange type.

\subsection{Quadrilateral Lagrange Shape}
\label{sec:quad_shape}

The application of the tensor product for Lagrange polynomial can be
written for a two dimensional quadrilateral shape function with

\begin{equation}
  \label{eq:quad_shape}
  h_{[i,j]}^n(r,s) = l_i^n(r) \cdot l_j^n(s)
\end{equation}

In Figure~\ref{fig:bilinear_shape_2D} the shape function for the node
$[1,1]$ is shown in red. This shape function is once again $1$ in its
corresponding node and zero in all the others.

\begin{figure}[!ht]
  \centering
  \input{src_images/quad_shape_iso.pdf_tex}  
  \caption{The bilinear shape function for two dimensional
    quadrilateral at node $[0,0]$ is shown. The shape ``hat'' function
    is $h_{[0,0]}^1(r,s)$.}
  \label{fig:bilinear_shape_2D}
\end{figure}

We will discuss the node index $[i,j]$ in the following. Consider a
set of 4 points in ${r,s} \in \mathbb{R}^2$ as shown in
Figure~\ref{fig:bilinear_shape_2D_adjacency}.

\begin{figure}[H]
  \centering
  \hspace{3.5cm}\input{src_images/quad_shape_node_index_bilinear.pdf_tex}
  \caption{Adjacency between local node numbers and node indices for
    bilinear quadrilateral shape functions.}
  \label{fig:bilinear_shape_2D_adjacency}
\end{figure}

The correspondence between a local node number (like used in most
finite element codes) and the node index can be noted in the adjacency
Table~\ref{tab:adj_table_bilinear_quad}.

\begin{table}[!ht]
  \centering
  \begin{tabular}{c|c}
    local number & node index \\ \hline
    0 & [0,0] \\
    1 & [1,0] \\
    2 & [1,1] \\
    3 & [0,1]
  \end{tabular}
  \caption{Adjacency table for bilinear quadrilaterals}
  \label{tab:adj_table_bilinear_quad}
\end{table}

One big advantage of Lagrange type shape functions are that the order
can be easily raised as one can see in
Figure~\ref{fig:second_order_shape_2D_adjacency} with the
corresponding adjacency table, see Table~\ref{tab:adj_table_biquadratic_quad}.

\begin{figure}[!ht]
  \centering
  \hspace{2.5cm}\input{src_images/quad_shape_node_index_biquadratic.pdf_tex}
  \caption{Second order Lagrange polynomials are used to construct a
    Lagrange type shape function. One can see the relation between the
    local node number and the node index.}
  \label{fig:second_order_shape_2D_adjacency}
\end{figure}

\begin{table}[!ht]
  \centering
  \begin{tabular}{c|c}
    local number & node index \\ \hline
    0 & [0,0] \\
    1 & [2,0] \\
    2 & [2,2] \\
    3 & [0,2] \\
    4 & [1,0] \\
    5 & [2,1] \\
    6 & [1,2] \\
    7 & [0,1] \\
    8 & [1,1]
  \end{tabular}
  \caption{Adjacency table for biquadratic quadrilaterals}
  \label{tab:adj_table_biquadratic_quad}
\end{table}

To write the interpolation in matrix notation we define an
interpolation matrix ${\cal H}(r,s)$. We use the adjacency table (see
Table~\ref{tab:adj_table_bilinear_quad}) and yield for the linear case

\begin{equation}
  \begin{array}{l@{\,=\big[}cccc@{\big]}}
    {\cal H}(r,s) & h_{[0,0]} & h_{[1,0]} & h_{[1,1]} & h_{[0,1]} \\[3mm]
    & h_0 & h_1 & h_2 & h_3
  \end{array}
  \label{eq:quad_shape_matrix}
\end{equation}

We have defined the ${\cal H}(r,s)$ matrix as a row matrix. The length is
equal to the number of nodes. The ordering of the values in the matrix
correspond to the local number which is strongly related to the node
index with the adjacency table.

Thus we can write the interpolation in a very compact
way. $\node{\beta_i}$ being the nodal values and $\node{\bfbeta} =
\mat{cccc}{\beta_0 & \beta_1 & \beta_2 & \beta_3}^T$ the corresponding
vector of these nodal values ($m$ being the number of nodes for this
shape function).

\begin{equation}
  \label{eq:scalar_interpolation}
  \beta(r,s) = \sum \limits_{i=0}^{m-1} \node{\beta_i} h_i(r,s) =  {\cal H}(r,s) \cdot \node{\bfbeta}
\end{equation}

\begin{figure}[!ht]
  \centering
  \input{src_images/quad_shape_iso_interpol.pdf_tex}
  \caption{Visualization of a bilinear quadrilateral
    interpolation. Although the Lagrange polynomials are linear
    functions the tensor product gives an interpolated surface which
    is in general not plane.}
  \label{fig:lin_interpol_quad}
\end{figure}

This procedure of constructing shape functions can be extended to
arbitrary orders. It is even possible to have different orders in the
two coordinate directions. In
Figure~\ref{fig:quad_shape_arbitrary_order} a visualization of this
construction via tensor product of ansatz functions is given. This
general method allows the development of $p$-adaptive finite element
procedures.

\begin{figure}[!ht]
  \centering
  \input{src_images/quad_shape_arbitrary_order.pdf_tex}
  \caption{The construction of the quadrilateral shape function of Lagrange class is visualized.}
  \label{fig:quad_shape_arbitrary_order}
\end{figure}

\subsubsection{First Derivative}
\label{sec:quad_shape_first_der}

We need (at least) the first derivative of the shape functions during
the application of the finite element method.

\begin{align}
  \partiel{h_{[i,j]}^n(r,s)}{r} &= \partiel{l_i^n(r)}{r} \cdot l_j^n(s) \nonumber \\
  \partiel{h_{[i,j]}^n(r,s)}{s} &= l_i^n(r) \cdot \partiel{l_j^n(s)}{s} \label{eq:quad_shape_first_derivation}
\end{align}

The first derivation of the Lagrange polynomial itself (which is
needed here) is already given in Eq.~\eqref{eq:lagr_poly_first_der}.

We define the shape matrix ${\cal H}(r,s)$ (see
Eq.~\eqref{eq:quad_shape_matrix}) and yield

\begin{equation}
  \label{eq:quad_shape_matrix_first_der}
  {\cal H}'(r,s) = \mat{c}{\partiel{{\cal H}}{r} \\[2mm] \partiel{{\cal H}}{s}} = 
  \mat{cccc}{\partiel{h_0}{r} & \partiel{h_1}{r} & \partiel{h_2}{r} & \partiel{h_3}{r} \\[2mm]
    \partiel{h_0}{s} & \partiel{h_1}{s} & \partiel{h_2}{s} & \partiel{h_3}{s} }
\end{equation}

This allows us now to write the gradient in a very efficient way

\begin{equation}
  \nabla\left(\beta(r,s)\right) = \partiel{\beta(r,s)}{\bfr}= 
  \mat{c}{
    \partiel{\beta(r,s)}{r} \\[2mm] 
    \partiel{\beta(r,s)}{s}
  } = {\cal H}'(r,s) \cdot \hat{\bfbeta}
\end{equation}

\begin{figure}[!ht]
  \centering
  \input{src_images/quad_shape_iso_divergence.pdf_tex}
  \caption{Visualization of the first derivatives on a bilinear
    quadrilateral interpolation. The interpolated function
    $\beta(r,s)$ is derived with respect to the two natural
    coordinates $r,s$. The gradient $\nabla\left(\beta(r,s)\right)$
    for this two dimensional scalar field has two components.}
  \label{fig:lin_interpol_quad_deriv}
\end{figure}

\subsection{Triangular Lagrange Shape}
\label{sec:triangular_shape}

For the triangular shape the polynomial tensor product has to be
modified (see \citet{ZienTayl2000}).

\begin{equation}
  \label{eq:triangular_shape}
  h_{[i,j,k]}^n(L_1,L_2,L_3) = l_i^i(L_1) \cdot l_j^j(L_2) \cdot l_k^k(L_3)
\end{equation}

where $L_i$ are the area coordinates with $L_1+L_2+L_3 \equiv 1$. They
can be transferred into natural coordinates $(r,s)$ with the following
relation.

\begin{equation}
  \label{eq:triange_area_natural}
  \begin{array}{lccrr}
    L_1 &=&   &  r &   \\
    L_2 &=&   &    &  s\\
    L_3 &=& 1 & -r & -s
  \end{array}
\end{equation}

\begin{figure}[!ht]
  \centering
  \input{src_images/triangular_shape.pdf_tex}
  \caption{For the triangular shape we need a node index consisting of
    3 values $[i,j,k]$. They have to fulfill the constraint $i+j+j =
    n$, where $n$ is the order of the triangular shape.}
  \label{fig:triangular_shape}
\end{figure}

The scheme for the node index can be seen in
Figure~\ref{fig:triangular_shape}. It is also notable, that the Lagrange
polynomials $l_\alpha^\alpha$ have different orders for each node. For
node $[1,2,0]$ we multiply the linear $l_1^1(r)$ with the quadratic
$l_2^2(s)$. But regardless of the order of the Lagrange polynomials
the natural node coordinates do not change. This means the node
coordinates for $l_1^1(r)$ are $\{r_0 = 0, r_1 = 1/3\}$ and for
$l_2^2(s)$, $\{s_o=0,s_1=1/3,s_2=2/3\}$. This has to be reflected in
the implementation of the Lagrangian polynomials.

Through an appropriate adjacency table (see
Table~\ref{tab:adj_table_linear_triangular} and
Table~\ref{tab:adj_table_quadratic_triangular}) one can collect the
shape functions to a shape matrix ${\cal H}(L_1,L_2,L_3)$ similar to
Eq.~\eqref{eq:quad_shape_matrix}.

\begin{table}[!ht]
  \begin{minipage}{0.45\linewidth}
  \centering
  \begin{tabular}{c|c}
    local number & node index \\ \hline
    0 & [0,0,1] \\
    1 & [1,0,0] \\
    2 & [0,1,0]
  \end{tabular}
  \caption{Adjacency table for triangles of order $n=1$.}
  \label{tab:adj_table_linear_triangular}    
  \end{minipage}
  \begin{minipage}{0.45\linewidth}
  \centering
  \begin{tabular}{c|c}
    local number & node index \\ \hline
    0 & [0,0,2] \\
    1 & [2,0,0] \\
    2 & [0,2,0] \\
    3 & [1,0,1] \\
    4 & [1,1,0] \\
    5 & [0,1,1]
  \end{tabular}
  \caption{Adjacency table for triangles of order $n=2$.}
  \label{tab:adj_table_quadratic_triangular}    
  \end{minipage}
\end{table}

\subsubsection{First Derivative}
\label{sec:triangular_shape_first_der}

For the finite element method we will need the first derivative with
respect to the natural coordinates $r$ and $s$. Those can be
calculated by inserting the map from
Eq.~\eqref{eq:triange_area_natural} and yield

\begin{align}
  \partiel{h_{[i,j,k]}^n(L_1,L_2,L_3)}{r} &= 
  \partiel{l_i^i(L_1)}{L_1} \cdot l_j^j(L_2) \cdot l_k^k(L_3) - 
  l_i^i(L_1) \cdot l_j^j(L_2) \cdot \partiel{l_k^k(L_3)}{L_3} \nonumber \\
  \partiel{h_{[i,j,k]}^n(L_1,L_2,L_3)}{s} &= 
  l_i^i(L_1) \cdot \partiel{l_j^j(L_2)}{L_2} \cdot l_k^k(L_3) - 
  l_i^i(L_1) \cdot l_j^j(L_2) \cdot \partiel{l_k^k(L_3)}{L_3} \label{eq:triangular_shape_first_derivation}
\end{align}

These terms can be collected to a derivative shape matrix
${\cal H}'(L_1,L_2,L_3)$ similar to
Eq.~\eqref{eq:quad_shape_matrix_first_der}.

\subsection{Hexahedral Lagrange Shape}
\label{sec:hex_shape}

Hexahedral shapes are the logic expansion of the two dimensional
quadrilateral shape from Section~\ref{sec:quad_shape} to three
dimensions.

\begin{equation}
  \label{eq:hex_shape}
  h_{[i,j,k]}^n(r,s,t) = l_i^n(r) \cdot l_j^n(s) \cdot l_k^n(t)
\end{equation}

A suitable adjacency table for linear basis functions can be found in
Table~\ref{tab:adj_table_linear_hex}.

\begin{table}[!ht]
  \centering
  \begin{tabular}{c|c}
    local number & node index \\ \hline
    0 & [0,0,0] \\
    1 & [1,0,0] \\
    2 & [1,1,0] \\
    3 & [0,1,0] \\
    4 & [0,0,1] \\
    5 & [1,0,1] \\
    6 & [1,1,1] \\
    7 & [0,1,1] \\
  \end{tabular}
  \caption{Adjacency table for hexahedrals of order $n=1$.}
  \label{tab:adj_table_linear_hex}
\end{table}

With this table the shape functions can be collected to a shape matrix
${\cal H}(r,s,t)$ similar to Eq.~\eqref{eq:quad_shape_matrix}.

\subsubsection{First Derivative}

\begin{align}
  \partiel{h_{[i,j,k]}^n(r,s,t)}{r} &= \partiel{l_i^n(r)}{r} \cdot l_j^n(s) \cdot l_k^n(t) \nonumber \\
  \partiel{h_{[i,j,k]}^n(r,s,t)}{s} &= l_i^n(r) \cdot \partiel{l_j^n(s)}{s} \cdot l_k^n(t) \nonumber \\
  \partiel{h_{[i,j,k]}^n(r,s,t)}{t} &= l_i^n(r) \cdot l_j^n(s) \cdot \partiel{l_k^n(t)}{t} \label{eq:hex_shape_first_derivation}
\end{align}

With Table~\ref{tab:adj_table_linear_hex} we can collect these
derivatives into the derivative shape matrix ${\cal H}'(r,s,t)$ similar to
Eq.~\eqref{eq:quad_shape_matrix_first_der}.

\subsection{Tetrahedral Lagrange Shape}
\label{sec:tetra_shape}

Like hexahedral for quadrilaterals are tetrahedral shapes the logic
expansion of the two dimensional triangular shape from
Section~\ref{sec:triangular_shape} to three dimensions.

\begin{equation}
  \label{eq:tetra_shape}
  h_{[i,j,k,l]}^n(L_1,L_2,L_3,L_4) = l_i^i(L_1) \cdot l_j^j(L_2) \cdot l_k^k(L_3) \cdot l_l^l(L_4)
\end{equation}

where $L_i$ are the volume coordinates with $L_1+L_2+L_3+L_4 \equiv 1$. They
can be transferred into natural coordinates $(r,s,t)$ with the following
relations

\begin{equation}
  \label{eq:tetra_area_natural}
  \begin{array}{lcrrrr}
    L_1 &=&   & r  &    &    \\
    L_2 &=&   &    &  s &    \\
    L_3 &=&   &    &    &  t \\
    L_4 &=& 1 & -r & -s & -t
  \end{array}
\end{equation}

For the indices the condition $i+j+k+l=n$ where $n$ is the shape
order, has to be fulfilled. A suitable adjacency table for linear
tetrahedrals can be found in
Table~\ref{tab:adj_table_linear_tetra}.

\begin{table}[!ht]
  \centering
  \begin{tabular}{c|c}
    local number & node index \\ \hline
    0 & [0,0,0,1] \\
    1 & [1,0,0,0] \\
    2 & [0,1,0,0] \\
    3 & [0,0,1,0]
  \end{tabular}
  \caption{Adjacency table for tetrahedrals of order $n=1$.}
  \label{tab:adj_table_linear_tetra}
\end{table}

\subsubsection{First Derivative}
\label{sec:tetra_shape_first_der}

For the finite element method we will need the first derivative with
respect to the natural coordinates $r$ and $s$. These can be
calculated by inserting the mapping from
Eq.~\eqref{eq:tetra_area_natural} and yield

\begin{align}
  \partiel{h_{[i,j,k,l]}^n(L_1,L_2,L_3,L_4)}{r} &= 
  \partiel{l_i^i(L_1)}{L_1} \cdot l_j^j(L_2) \cdot l_k^k(L_3) \cdot l_l^l(L_4) - 
  l_i^i(L_1) \cdot l_j^j(L_2) \cdot l_k^k(L_3) \cdot \partiel{l_l^l(L_4)}{L_4} \nonumber \\
  \partiel{h_{[i,j,k,l]}^n(L_1,L_2,L_3,L_4)}{s} &= 
  l_i^i(L_1) \cdot \partiel{l_j^j(L_2)}{L_2} \cdot l_k^k(L_3) \cdot l_l^l(L_4) - 
  l_i^i(L_1) \cdot l_j^j(L_2) \cdot l_k^k(L_3) \cdot \partiel{l_l^l(L_4)}{L_4} \nonumber \\
  \partiel{h_{[i,j,k,l]}^n(L_1,L_2,L_3,L_4)}{s} &= 
  l_i^i(L_1) \cdot l_j^j(L_2) \cdot \partiel{l_k^k(L_3)}{L_3} \cdot l_l^l(L_4) - 
  l_i^i(L_1) \cdot l_j^j(L_2) \cdot l_k^k(L_3) \cdot \partiel{l_l^l(L_4)}{L_4} \nonumber \\
\end{align}

They can be collected to a derivative shape matrix ${\cal H}'(L_1,L_2,L_3)$
similar to Eq.~\eqref{eq:quad_shape_matrix_first_der}.

\section{Implementation}

\subsection{\tt soofea.model.shape.Lagrange}

The interface is shown in Figure~\ref{fig:sig:model.shape.Lagrange}
and described in the following.

\begin{impl}[{\tt \_node\_positions}]
  \label{impl:lagr_node_positions}
  {\tt \_node\_positions} is an array containing all the coordinates
  of the nodes $r_i$ from Eq.~\eqref{eq:lagrange_poly}. For
  quadrilateral or hexahedral shape functions we can directly compute
  the order of the polynomial out of the number of node points, {\tt
    order = len(self.\_node\_positions)-1}.
\end{impl}

\begin{impl}[{\tt index}]
  \label{impl:lagr_index}
  The {\tt index} identifies the Lagrange polynomial. It is identical
  to the value of $k$ in Eq.~\eqref{eq:lagrange_poly}.
\end{impl}

\begin{impl}[{\tt order}]
  \label{impl:lagr_order}
  For triangular and tetrahedral shape functions we might have defined
  more {\tt \_node\_positions} then the order we want to retrieve
  requires. Then we can use the argument {\tt order} to overwrite the
  top index $n$ of the Lagrange polynomial $l_i^n(r)$.
\end{impl}

\begin{figure}[h!]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=9cm]{Lagrange}{0,0}
      \attribute{\_node\_positions \iref{impl:lagr_node_positions}}
      \operation{Lagrange( self, node\_positions ) \iref{impl:lagr_node_positions}}
      \operation{get( self, r , index , order = None ) \iref{impl:lagr_index} \iref{impl:lagr_order}}
      \operation{der( self, r , index , order = None ) \iref{impl:lagr_index} \iref{impl:lagr_order}}
    \end{class}
  \end{tikzpicture}    
  \caption{Signature of class {\tt model.shape.Lagrange}}
  \label{fig:sig:model.shape.Lagrange}
\end{figure}

\subsection{{\tt soofea.model.shape.Shape}}
\label{sec:impl_shape}

The interface is shown in
Figure~\ref{fig:sig:model.shape.Shape_model.shape.QuadShape}. Everything
which is dependent on the shape type has to be implemented in the
specialization. Here we explain the specialization of \linebreak {\tt
  soofea.model.shape.QuadShape} but the situation is equivalent for
other shape types like {\tt soofea.model.shape.LinearShape}, {\tt
  soofea.model.shape.BrickShape} aso.

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=14cm]{Shape}{0,0}
      \attribute{\_shape\_dimension \iref{impl:shape_dimension}}
      \attribute{\_order \iref{impl:shape_order}}
      \attribute{\_number\_of\_nodes \iref{impl:shape_number_of_nodes}}
      \attribute{\_lagrange}
      \operation[0]{Shape( self, order, shape\_dimension ) \iref{impl:shape_order} \iref{impl:shape_dimension}}
      \operation[0]{getMatrix( self, natural\_coordinates, array\_length ) \iref{impl:model.shape.Shape.getMatrix}}
      \operation[0]{getDerivativeMatrix( self, natural\_coordinates, array\_length ) \iref{impl:model.shape.Shape.getDerivativeMatrix}}
      \operation[0]{getNumberOfNodes( self ) \iref{impl:shape_number_of_nodes}}
    \end{class}
    \begin{class}[text width=15.5cm]{QuadShape}{0,-6}
      \inherit{Shape}
      \operation[0]{QuadShape( self, order ) \iref{impl:shape_order}}
      \operation[0]{getNodeIndex( self, local\_number ) \iref{impl:shape_node_index}}
      \operation[0]{get( self, natural\_coordinates, node\_index ) \iref{impl:shape_get} \iref{impl:shape_node_index}}
      \operation[0]{der( self, natural\_coordinates, node\_index , derivative\_direction ) \iref{impl:shape_der} \iref{impl:shape_node_index}}
      \operation[1]{\_calcNodePositions( self ) \iref{impl:model.shape.Shape._calcNodePositions}}
      \operation[1]{\_calcNumberOfNodes( self ) \iref{impl:shape_number_of_nodes}}
    \end{class}
  \end{tikzpicture}    
  \caption{Signature of class {\tt soofea.model.shape.Shape} and its
    subclass \newline {\tt soofea.model.shape.QuadShape}}
  \label{fig:sig:model.shape.Shape_model.shape.QuadShape}
\end{figure}

\begin{impl}[{\tt \_shape\_dimension}]
  \label{impl:shape_dimension}
  The {\tt \_shape\_dimension} represents the dimension of the shape
  in the reference configuration. It is possible that the {\tt
    \_shape\_dimension} differs from the dimension of the global
  coordinate systems, e.g. a shell element embedded in a
  three dimensional space.
\end{impl}

\begin{impl}[{\tt \_order}]
  \label{impl:shape_order}
  {\tt order} is the order of the polynomials used to construct the
  shape function.
\end{impl}

\begin{impl}[{\tt \_number\_of\_nodes}]
  \label{impl:shape_number_of_nodes}
  {\tt Shape.getNumberOfNode()} should return the number of nodes for
  shape type and order. Of course a bilinear triangular shape or brick
  shape returns a different number in comparison to a bilinear
  quadrilateral. Therefore a method {\tt QuadShape.\_calcNumberOfNodes()}
  has to be implemented in the specialization.
\end{impl}

\begin{impl}
  \label{impl:model.shape.Shape._calcNodePositions}
  Calculates the coordinates for the Lagrange polynomial in one
  direction (for this implementation the coordinates $r_i$, $s_i$ and
  $t_i$ are assumed to be equal) depending on the shape type. They
  have to be calculated in the specializations, here in {\tt
    QuadShape.\_calcNodePositions()}. Normally those are equidistant
  points in the range $[-1,1]$.
\end{impl}

\begin{impl}
  \label{impl:shape_node_index}
  In {\tt QuadShape.getNodeIndex()} the adjacency table should be
  implemented as described and shown in
  Table~\ref{tab:adj_table_bilinear_quad},
  Table~\ref{tab:adj_table_biquadratic_quad} and in the
  Figure~\ref{fig:bilinear_shape_2D_adjacency},
  Figure~\ref{fig:second_order_shape_2D_adjacency} and corresponding
  explanations.
\end{impl}

\begin{impl}
  \label{impl:shape_get}
  {\tt QuadShape.get()} returns the value of the shape function at the
  given coordinates {\tt coord} (implementation of
  Eq.~\eqref{eq:quad_shape}). One must of course provide the node
  index $[i,j]$ for which the shape function should be evaluated (see
  Implementatino~remark~\iref{impl:shape_node_index}).
\end{impl}

\begin{impl}
  \label{impl:shape_der}
  {\tt QuadShape.der()} returns the value of the first derivation
  (implementation of Eq.~\eqref{eq:quad_shape_first_derivation} and
  similar). Therefore one has to provide the direction in which the
  derivation should be calculated ({\tt der\_dir}=1 for $r$ and {\tt
    der\_dir}=2 for $s$) in addition to the parameters {\tt
    QuadShape.get()} needs. \ref{impl:shape_order}
\end{impl}

\begin{impl}
  \label{impl:model.shape.Shape.getMatrix}
  This method returns the matrix ${\cal H}(r,s)$ from
  Eq.~\eqref{eq:quad_shape_matrix}. With the parameter {\tt
    array\_length} a shape matrix which is able to interpolate a
  vector field is constructed. We return the shape matrix (see
  Eq.~\eqref{eq:fe_shape_matrix} from
  Section~\ref{sec:discretization}). For a two dimensional vector
  field {\tt array\_length} = 2 and for quadrilateral shape
  $\bfH(r,s)$ would yield

  \begin{equation}
    \bfH(r,s) = \mat{ccccccccc}{
      h_0 & 0 & h_1 & 0 & h_2 & 0 & h_3 & 0 \\
      0 & h_0 & 0 & h_1 & 0 & h_2 & 0 & h_3
    }
  \end{equation}

  Which allows to interpolate a nodal vector

  \begin{equation}
    \label{eq:gamma_field}
    \node{\bfgamma} =
    \mat{cccccccc}{\node{\gamma}_{x0} & \node{\gamma}_{y0} &
      \node{\gamma}_{x1} & \node{\gamma}_{y1} & \node{\gamma}_{x2} &
      \node{\gamma}_{y2} & \node{\gamma}_{x3} & \node{\gamma}_{y3}}   
  \end{equation}

  to retrieve $\bfgamma(r,s) = \mat{cc}{\gamma_x(r,s) &
    \gamma_y(r,s)}^T = \bfH(r,s) \cdot \node{\bfgamma}$
\end{impl}

\begin{impl}
  \label{impl:model.shape.Shape.getDerivativeMatrix}
  This method returns the matrix ${\cal H}'(r,s)$ from
  Eq.~\eqref{eq:quad_shape_matrix_first_der}. With the parameter {\tt
    array\_length} a derivative shape matrix applicable on a vector
  field is constructed.  For a two dimensional vector
  field {\tt array\_length} = 2 and for quadrilateral shape
  $\bfH'(r,s)$ would yield

  \begin{equation}
    \bfH'(r,s) = \mat{cccccccc}{
      \partiel{h_0}{r} & 0 & \partiel{h_1}{r} & 0 & \partiel{h_2}{r} & 0 & \partiel{h_3}{r} & 0 \\
      \partiel{h_0}{s} & 0 & \partiel{h_1}{s} & 0 & \partiel{h_2}{s} & 0 & \partiel{h_3}{s} & 0 \\
      0 & \partiel{h_0}{r} & 0 & \partiel{h_1}{r} & 0 & \partiel{h_2}{r} & 0 & \partiel{h_3}{r} \\
      0 & \partiel{h_0}{s} & 0 & \partiel{h_1}{s} & 0 & \partiel{h_2}{s} & 0 & \partiel{h_3}{s}
    }
  \end{equation}

  Which would allow to calculate all derivatives in one step for the
  nodal vector from Eq.~(\ref{eq:gamma_field}) and retrieve
  $\partiel{\bfgamma(r,s)}{\bfr} =
  \mat{cccc}{\partiel{\gamma_x(r,s)}{r} & \partiel{\gamma_x(r,s)}{s}
    & \partiel{\gamma_y(r,s)}{r} & \partiel{\gamma_y(r,s)}{s}}^T =
  \bfH'(r,s) \cdot \node{\bfgamma}$
\end{impl}

\section{Examples}
\label{sec:num_math_examples}

\begin{ex}
  \label{ex:test_lagrange}
  Use the {\tt soofea.model.shape.Lagrange} class and implement unit
  tests.

  \begin{enumerate}[a)]
  \item The values of the Lagrange polynomials ({\tt
      soofea.model.shape.Lagrange.get()}) on all nodes has to be $1$
    for the corresponding and $0$ on all the other nodes.
  \item The sum of the Lagrange polynomials of all nodes has to $1$. Check
    this on various randomized points inside the domain.
  \item Precalculate some selected points inside and check against the
    calculated ones.
  \item Precalculate the value of the first derivative ({\tt
      soofea.model.shape.Lagrange.der()}) on different points inside
    $[-1,+1]$ and check them.
  \end{enumerate}
\end{ex}

\begin{ex}
  \label{ex:plot_lagrange}
  Write a method inside the {\tt soofea.model.shape.Lagrange} class
  which displays a given Lagrange polynomial by its node coordinates
  as 2D plot with the help of {\tt matplotlib}. Label each node with its
  number and coordinate. The function call should be implemented like
  in the following example and should work for polynomials up to forth
  order.

  \begin{lstlisting}
from soofea.model.shape import Lagrange
node_positions = [1,2,3]
l = Lagrange(node_positions)
l.plot()
  \end{lstlisting}
\end{ex}

\begin{ex}
  \label{ex:test_quad_shape}
  Use the {\tt soofea.model.shape.QuadShape} class and implement unit
  tests (for order $n=1,2$).

  \begin{enumerate}[a)]
  \item The values of the shape functions ({\tt
      soofea.model.shape.QuadShape.get()}) on all nodes has to be $1$
    for the corresponding and $0$ on all the other nodes.
  \item The sum of the shape functions of all nodes has to $1$. Check
    this on various randomized points inside the domain.
  \item Precalculate some selected points inside and check against the
    calculated ones.
  \item Precalculate the value of the first derivative ({\tt
      soofea.model.shape.QuadShape.der()}) on different points inside the
    reference quadrilateral and check them
  \end{enumerate}
\end{ex}

\begin{ex}
  \label{ex:visualize_quad_shape}
  Use the {\tt soofea.model.shape.QuadShape} class and visualize the
  interpolation. Therefore use arbitrary coordinates for the nodes in
  the $[r,s]$ plane and for each node arbitrary values $f(r,s)$. Now
  it should be possible to draw a surface in 3D space similar to the
  following example in Figure~\ref{fig:visualize-quad-shape-1} for
  bilinear shape functions and in
  Figure~\ref{fig:visualize-quad-shape-2} for biquadratic shape
  functions.

  \begin{figure}[!ht]
    \begin{minipage}[b]{0.45\textwidth}
      \centering
      \includegraphics[width=\textwidth]{images/shape_quad_1.jpg}
      \caption{Visualization of an interpolation with bilinear {\tt QuadShape} shape functions.}
      \label{fig:visualize-quad-shape-1}
    \end{minipage}\hfill
    \begin{minipage}[b]{0.45\textwidth}
      \centering
      \includegraphics[width=\textwidth]{images/shape_quad_2.jpg}
      \caption{Visualization of an interpolation with biquadratic {\tt QuadShape} shape functions.}
      \label{fig:visualize-quad-shape-2}
    \end{minipage}
  \end{figure}
\end{ex}

\begin{ex}
  \label{ex:interpol_linear_shape}
  Implement the class {\tt soofea.model.shape.LinearShape}. This is a
  rather simple wrapper for the {\tt soofea.model.shape.Lagrange}
  class to provide a shape like interface for linear elements. This
  class can be used for {\tt Edge}s or truss elements.
\end{ex}

\begin{ex}
  \label{ex:interpol_triangle_shape}
  Implement the class {\tt soofea.model.shape.TriangleShape}. Use the
  relations given in Section~\ref{sec:triangular_shape}. Then implement
  the same tests as for {\tt soofea.model.shape.QuadShape} from
  Example~\ref{ex:test_quad_shape} for the orders $n=1,2$.
\end{ex}

\begin{ex}[\ref{ex:interpol_triangle_shape}]
  Write a test function in the same way as for
  Example~\ref{ex:test_quad_shape} for the \linebreak {\tt
    soofea.model.shape.TriangleShape} class.
\end{ex}

\begin{ex}[\ref{ex:interpol_triangle_shape}]
  Do a visualization of the {\tt soofea.model.shape.TriangleShape} class in a
  similar way as for Example~\ref{ex:visualize_quad_shape}.
\end{ex}

\begin{ex}
  \label{ex:interpol_hex_shape}
  Implement the class {\tt soofea.model.shape.HexShape}. Use the
  relations given in Section~\ref{sec:hex_shape}. Then implement the
  same tests as for {\tt soofea.model.shape.HexShape} from
  Example~\ref{ex:test_quad_shape} for the order $n=1,2$. (see the
  \gmsh documentation for an appropriate adjacency table for the
  second order polynomials)
\end{ex}

\begin{ex}[\ref{ex:interpol_hex_shape}]
  Write a test function in the same way as for
  Example~\ref{ex:test_quad_shape} for the \linebreak {\tt
    soofea.model.shape.HexShape} class.
\end{ex}

\begin{ex}[\ref{ex:interpol_hex_shape}]
  Visualize (plot) a single hexahedral element with (more or less)
  arbitrary node coordinates and $n=1,2$ in a global coordinate system
  by using the {\tt soofea.model.shape.HexShape} class to interpolate
  the geometry.
\end{ex}

\begin{ex}
  \label{ex:interpol_tetra_shape}
  Implement the class {\tt soofea.model.shape.TetraShape}. Use the
  relations given in Section~\ref{sec:hex_shape}. Then implement the
  same tests as for {\tt soofea.model.shape.TetraShape} from
  Example~\ref{ex:test_quad_shape} for the order $n=1,2$. (see the
  \gmsh documentation for an appropriate adjacency table for the
  second order polynomials)
\end{ex}

\begin{ex}[\ref{ex:interpol_tetra_shape}]
  Write a test function in the same way as for
  Example~\ref{ex:test_quad_shape} for the \linebreak {\tt
    soofea.model.shape.TetraShape} class.
\end{ex}

\begin{ex}[\ref{ex:interpol_tetra_shape}]
  Visualize (plot) a single tetrahedral element with (more or less)
  arbitrary node coordinates and $n=1,2$ in a global coordinate system
  by using the {\tt soofea.model.shape.TetraShape} class to interpolate
  the geometry.
\end{ex}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% eval: (setenv "TEXINPUTS" "./src_images//:")
%%% End: 
