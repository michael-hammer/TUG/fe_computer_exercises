\chapter{Numerical Integration}
\label{chap:num_int}

Numerical integration (also quadrature) is an approximative method to
calculate a definite integral. The exact integration is replaced by
the summation over weighted function values which can be written as

\begin{equation}
  \label{eq:num_int}
  {\cal I} = \int\limits_a^b F(x) \de x = \sum \limits_{i=0}^{n} \alpha_i F(x_i) + R_n
\end{equation}

$\alpha_i$ are the integration weights and $x_i$ the sampling
points. Weights and sampling points are given for a special
range $[a,b]$. $R_n$ represents the remainder term.

\begin{figure}[!ht]
  % \centering
  \hspace{3cm}\input{src_images/num_int_midpoint.pdf_tex}
  \caption{We assume a linear function $f(r)$ for which we would like
    to calculate the definite integral ${\cal I} = \int_a^b F(x) \de
    x$. It is quite obvious that we can calculate this integral by
    evaluating the function at the midpoint of the interval
    $f\left(\frac{a+b}{2}\right)$ and multiply this with the length of
    the interval $b-a$. This midpoint rule is the easiest numerical
    integration method but the main character of quadrature is
    observable: Evaluation of the function at given integration points
    $x_i$ and multiplying those with the integration weights
    $\alpha_i$. For linear functions the midpoint rules evaluates to
    the exact value of the integral. If the function $f(x)$ is a
    polynomial of higher order the remainder is unequal to zero. The
    accuracy of the quadrature depends on the order of the polynomial
    to integrate.}
  \label{fig:int_midpoint}
\end{figure}

The situation can be easily illustrated
(Figure~\ref{fig:int_midpoint}) for the mid point rule (see
\citet{Schwarz2006}) as the simplest case of the Newton-Cotes formulas
$n=0$ (see also \citet{CelFE1998}).

\section{Gauss-Legendre Quadrature}
\label{sec:gauss-legendre}

As we can see in Eq.~\eqref{eq:num_int} we have $2 \, (n+1)$ unknowns
($(n+1)$ weights $\alpha_i$ and $(n+1)$ coordinates $x_i$). If we
assume $(n+1)$ sampling points a polynomial of degree $(2\,n+1)$ can
be constructed and therefore exactly integrated in the best case. Such
an integration method is the Gauss-Legendre quadrature (see
\citet{Schwarz2006}).

Some important properties of this integration method are:

\begin{itemize}
\item The calculation of position and weight of sampling points is a
  non linear and non trivial task. Therefore it is advisable to store
  position and weight a priory.
\item The positions are symmetric with respect to
  $\frac{a+b}{2}$. That means for an odd amount of integration points,
  one integration point lies on $\frac{a+b}{2}$.
\item There are no integration points on the interval boundary.
\item The positions of the integration points are of course dependent
  on the integration interval.
\end{itemize}

\section{Mapping of Integration Interval}
\label{sec:int_mapping}

It is the primary case that you have to integrate a function $f(x)$ in
an interval $x \in [a,b]$ but the integration points positions are
given for a natural interval, e.g. $r \in [-1,1]$. Therefore we have
to find a suitable mapping of $r \rightarrow x$. The situation is
shown in Figure~\ref{fig:int_mapping}.

\begin{figure}[!ht]
  \centering
  \input{src_images/num_int_mapping.pdf_tex}
  \caption{We map the integral of $f(x)$ over the interval $[a,b]$
    onto the integral of $f(x(r))=\bar{f}(r)$ over the integral
    $[-1,1]$.}
  \label{fig:int_mapping}
\end{figure}

This mapping should be realized as linear mapping with

\begin{equation}
  x = k \, r + d
\end{equation}

Now we can insert the boundary and get the conditions

\begin{align}
  x(r=-1) &= -k + d \\
  x(r=+1) &= k + d
\end{align}

which yield

\begin{align}
  k &= \frac{b-a}{2} \\
  d &= \frac{a+b}{2}
\end{align}

so we know the mapping function as

\begin{equation}
  x =  \frac{b-a}{2} \, r + \frac{a+b}{2}
\end{equation}

This is nothing else then one would get if using the Lagrange
polynomials for interpolation.

\begin{equation}
  x = \sum \limits_{i=1}^2 x_i l_i^1(r) = a \, l_1^1(r) + b \, l_2^1(r) = a \, \frac{r-1}{-2} + b \, \frac{r+1}{2} = \frac{b-a}{2} \, r + \frac{a+b}{2}
\end{equation}

It is easy to calculate the linear tangent mapping

\begin{equation}
  \frac{\de x}{\de r} = \frac{b-a}{2}
\end{equation}

With this results we can finally do the integral transformation

\begin{equation}
  \int \limits_a^b f(x) \de x = 
  \int \limits_{-1}^{+1} \underbrace{f(x(r))}_{\displaystyle \bar{f}(r)} \underbrace{\frac{\de x}{\de r}}_{\displaystyle  \det(\bfJ)} \de r = 
  \int \limits_{-1}^{+1} \bar{f}(r) \frac{b-a}{2} \de r
\end{equation}

This is a well known result from analysis that the integral
transformation can be realized with $\det(\bfJ)$ which simplifies to
$\frac{\de x}{\de r}$ for the one dimensional case.

We insert Eq.~\eqref{eq:num_int} for the numerical integration and get

\begin{equation}
  \label{eq:num_int_1D}
  \int \limits_a^b f(x) \de x = \int \limits_{-1}^{+1} \bar{f}(r) \det(\bfJ) \de r = \sum \limits_{i=0}^n \alpha_i \bar{f}(r_i) \det(\bfJ)
\end{equation}

\section{Multidimensional Integration}

\begin{figure}[!ht]
  \centering
  \input{src_images/num_int_2D.pdf_tex}
  \caption{For two dimension quadrature (here for quadrilateral shaped
    domains) we first map the domain in natural coordinates $r,s$ onto
    a domain in global coordinates $x,y$. All possible domains (later
    we will call them elements) in the global coordinate system share
    the same natural domain. The integration points are distributed in
    the natural domain via a tensor product of the one dimensional
    locations. This means we introduce two position indices $[i,j]$ to
    identify each integration point uniquely.}
  \label{fig:int_2D}
\end{figure}

In the finite element method we have to integrate over
multidimensional domains. As example for the quadrilateral shape
functions we have to integrate over a two dimensional domain $\{r,s\}
\in \mathbb{R}^2$. The second issue we have to face is, that we have
to integrate over arbitrary shaped domains. Both issues are
illustrated in Figure~\ref{fig:int_2D}.

First we do the linear mapping similar to the one prescribed in
Section~\ref{sec:int_mapping}. The formulation of the integration
limits in the $x,y$ coordinates system is not easy

\begin{equation}
  {\cal I} = \int \limits_A f(x,y) \, \de A = \int \limits_x \int \limits_y f(x,y) \de x \de y
\end{equation}

whereas the limits in $r,s$ coordinates are rather simple.

\begin{equation}
  {\cal I} = \int \limits_{-1}^1 \int \limits_{-1}^1 f(x(r),y(s)) \, {\cal J}(r,s) \, \de r \de s
  = \int \limits_{-1}^1 \int \limits_{-1}^1 \bar{f}(r,s) \, \det(\bfJ(r,s)) \, \de r \de s
\end{equation}

The transformation factor ${\cal J}(r,s)$ is given by the determinant
of the Jacobian matrix of the corresponding domain at the given
integration point, ${\cal J}(r,s) = \det(\bfJ(r,s))$ and the Jacobian
matrix is given via $\bfJ(r,s) = \partiel{\bfx(r,s)}{\bfr}$ (see
Eq.~\eqref{eq:jacobian} for the definition of the Jacobian
matrix). For further information on this concept of isoparametric
finite elements and Jacobian matrices see Section~\ref{isoparametric}.

Second we have to introduce an integration ``grid''. Which means we
define in the natural coordinates a certain amount of integration
points in each coordinate direction (see Figure~\ref{fig:int_2D},
$i,j=0\dots1$). It is not necessary that the index in $r$ direction
$i$ and the index in $s$ direction $j$ does have the same limits but
it is appropriate to do so most of the time.

\begin{equation}
  {\cal I} = \int \limits_{-1}^1 \int \limits_{-1}^1 \bar{f}(r,s) \, {\cal J}(r,s) \, \de r \de s
  = \sum \limits_{i=0}^m \sum \limits_{j=0}^n \alpha_i \alpha_j \, \bar{f}(r_i,s_j) \, {\cal J}(r_i,s_j) \label{eq:num_int_2D}
\end{equation}

With this formulation we can reuse the integration point positions and
weights from the one dimensional case. It is straight forward to
extend this approach to three dimensions

\begin{equation}
  {\cal I} = \int \limits_{-1}^1 \int \limits_{-1}^1 \int \limits_{-1}^1 \bar{f}(r,s,t) \, {\cal J}(r,s,t) \, \de r \de s \de t
  = \sum \limits_{i=0}^m \sum \limits_{j=0}^n \sum \limits_{k=0}^o \alpha_i \alpha_j \alpha_k \, \bar{f}(r_i,s_j,t_k) \, {\cal J}(r_i,s_j,t_k) \label{eq:num_int_3D}
\end{equation}

\section{Implementation}
\label{sec:impl_num_int}

For the implementation we will differ between the integration point in
the natural coordinate system and the global coordinate system. This
situation has been already visualized in Figure~\ref{fig:int_mapping}.

The mathematical integration points in the natural coordinates are
then referenced by the integration points in the global coordinate
system. For integration domains of same type (later we will call those
{\tt ElementType}, {\tt EdgeType} aso.) the mathematical integration
points exist only once. Multiple model integration points (namely
those with identical natural coordinates) share one mathematical
integration point. The integration points in the global coordinate
system exist for each domain separately. There are much more model
integration points with their corresponding global coordinates then
mathematical ones.

\subsection{\code{soofea.math.IntegrationPoint}}

The weights and natural coordinates can be retrieved from the {\tt
  numpy} function \linebreak {\tt scipy.special.orthogonal.p\_roots(order)} (see
\citet{numpyGaussLegendre}).

\begin{impl}
  \label{impl:num_int_weight}
  {\tt weight} represents the value $\alpha_i$ for one integration
  point from Eq.~\eqref{eq:num_int_1D}. In the case of an
  multidimensional integration point (e.g. 2D) {\tt weight} already
  represents the product of $\alpha_i \alpha_j$ from this integration
  point at index position $i,j$ from Eq.~\eqref{eq:num_int_2D}.
\end{impl}

\begin{impl}
  \label{impl:num_int_nat_coord}
  {\tt natural\_coordinates} is an array containing all the
  coordinates of the integration point. It consists of one value for
  the one dimensional case and two values for the two dimension case
  aso.
\end{impl}

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=13cm]{IntegrationPoint}{0,0}
      \attribute{weight \iref{impl:num_int_weight}}
      \attribute{natural\_coordinates \iref{impl:num_int_nat_coord}}
      \operation[0]{IntegrationPoint( self, weight, natural\_coordinates ) \iref{impl:num_int_weight} \iref{impl:num_int_nat_coord}}
    \end{class}
  \end{tikzpicture}    
  \caption{Signature of class {\tt soofea.math.IntegrationPoint}}
  \label{fig:sig:soofea.math.IntegrationPoint}
\end{figure}

\subsection{\tt soofea.model.IntegrationPoint}

\begin{impl}
  \label{impl:model_integration_point}
  The {\tt soofea.model.IntegrationPoint} consists of a mathematical
  integration point \linebreak ({\tt soofea.math.IntegrationPoint} in
  {\tt math\_ip}) and the coordinates in the Lagrangian configuration
  ({\tt lagrange\_coordinates}).
\end{impl}

\begin{figure}[!ht]
  \centering
  \begin{tikzpicture}
    \begin{class}[text width=13cm]{IntegrationPoint}{0,0}
      \attribute{math\_ip \iref{impl:model_integration_point}}
      \attribute{lagrange\_coordinates \iref{impl:model_integration_point}}
      \operation[0]{IntegrationPoint( self, math\_ip, lagrange\_coordinates ) \iref{impl:model_integration_point}}
    \end{class}
  \end{tikzpicture}    
  \caption{Signature of class {\tt soofea.model.IntegrationPoint}}
  \label{fig:sig:soofea.model.IntegrationPoint}
\end{figure}

\subsection{\tt soofea.math.num\_int.integrate( ... )}

This function {\tt integrate(function, integration\_points,
  parameters)} integrates a \linebreak given {\tt function} which is
evaluated and the result is weighted and summed up. Therefore it needs
the {\tt integration\_points} which should be a list of \linebreak
{\tt soofea.model.IntegrationPoint}. The {\tt function} is called with
the actual integration point and the {\tt parameters} as
arguments. {\tt parameters} can be an array or dictionary which is
handed through to the {\tt function} to serve as argument list there.
The implementation mainly consists of a for loop realizing the
summation shown in Eqs.~\eqref{eq:num_int_1D} and \eqref{eq:num_int_2D}.

\subsection{\tt soofea.math.num\_int.methodIntegrate( ... )}
\label{sec:soofea.math.num_int.methodIntegrate}

To integrate a method function of a class you have to provide the {\tt
  object} from which the method should be called. For the other
arguments of \linebreak {\tt num\_int.methodIntegrate( function,
  object, integration\_points, parameters )} \newline the situation is
equal to {\tt math.num\_int.integrate()}.

\section{Examples}
\label{sec:num_int_examples}

\begin{ex}
  \label{ex:num_int_volume_test}
  Integrate the function array $f(r) = [1. 1. 1.]$, $f(r,s) =
  [1.,1.,1.]$ and $f(r,s,t) = [1.,1.,1.]$ for the natural 1D, 2D and
  3D domains (1D interval $\{ r = [-1,1] \}$, 2D Square $\{ r =
  [-1,1], s = [-1,1] \}$, 3D Cube $\{ r = [-1,1], s = [-1,1], t =
  [-1,1] \}$) respectively. The result should be the length, area and
  volume for the given domains respectively. The integration should be
  done with varying amount of integration points. This means that this
  implementation represents an elementary unit test if the numerical
  integration is correct.
\end{ex}

\begin{ex}
  \label{ex:num_int_functions}
  Integrate the functions $f(\xi) = \sin(\xi)$, $f(\xi) =
  \frac{1}{\xi}$ and $f(\xi) = \xi + 2 \xi^2 - 5 \xi^3$ with a varying
  number of integration points in the interval $\xi \in [-1,1]$. Use
  therefore the \linebreak {\tt soofea.math.mum\_int.integrate(...)}
  function and compare the numerical results with the solution of {\tt
    scipy.integrate.quad()}. How is the influence of different numbers
  of integration points on the accuracy?
\end{ex}

\begin{ex}
  \label{ex:num_int_newton_cotes}
  Calculate the weights for the Newton-C\^otes quadrature. The weights
  are defined via

  \begin{equation}
    \alpha_i = \int \limits_{-1}^1 l_i^n(r) \de r
  \end{equation}

  (see \citet{CelFE1998} for further treatment of Newton-C\^otes
  quadrature) for $n=1,2,3,4,5$. Use the Gauss-Legendre quadrature and
  the Lagrange polynomials from {\tt soofeam.model.shape.Lagrange} to
  do so. Finally evaluate the integrals for the functions from
  Example~\ref{ex:num_int_functions} with Newton-C\^otes and analyze
  the accuracy (compare it with the results from the Gauss-Legendre
  integration with the same amount of integration points).
\end{ex}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
