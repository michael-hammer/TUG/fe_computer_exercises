function y = avarage(x)
% AVERAGE Mean of vector elements.
%
% AVERAGE(X), where X is a vector, is the mean of vector
% elements. Nonvector input results in an error.
if nargin ~= 1
    error('You have to provide a vector!')
end
[m,n] = size(x);
if (~((m == 1) || (n == 1)) || (m == 1 && n == 1))
    error('Input must be a vector')
end
y = sum(x)/length(x);      % Actual computation