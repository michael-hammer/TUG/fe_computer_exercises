classdef AlumniStudent < Student
  properties
    retirement;
  end
  methods
    function obj = AlumniStudent( retirement, name, mat_nr )
      obj@Student(name,mat_nr);
      obj.retirement = retirement;
    end
    
    function disp(obj)
      disp@Student(obj);
      disp(['Pension : ',obj.retirement]);
    end
  end
end