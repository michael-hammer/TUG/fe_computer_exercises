classdef Student < handle
  properties( SetAccess = protected )
    name;
    mat_nr;
  end
  
  methods   
    function obj = Student( name, mat_nr )
      if nargin == 0
        obj.name = 'Max Mustermann';
        obj.mat_nr = 0130000;
      else
        obj.name = name;
        obj.mat_nr = mat_nr;
      end
    end
    
    function setName( obj , name )
      obj.name = name;
    end
    
    function disp( obj )
      disp(['Name  : "',obj.name,'"']);
      disp(['Mat-Nr: ',num2str(obj.mat_nr)]);
    end
  end
end
