# characters and strings
a = 'c'
a = 'cde'
a(2)
a = [ a , 'fg' ]
num2str(334)
a = [ a , num2str(345) ]
# cell Array
rand('state', 0)
umArray = rand(3,5)*20
chArray = ['Michael'; 'Hammer']
cellArray = {1 2 3; 4 5 6; 7 8 9}
logArray = numArray > 10
A = {numArray, pi, stArray; chArray, cellArray, logArray}
A
cellplot(A)
A(1,1)
A{1,1}
# structures
struct.test = 'hammer'
struct.v = 123.3
struct.test
struct.h = @sqrt
struct.h(3)